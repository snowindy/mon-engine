# Welcome to Freelance Job Monitor backend project

## Resources
Maven multi-module project 
www.avajava.com/tutorials/lessons/how-do-i-create-a-multi-module-project-in-eclipse.html?page=1

## Deployment
### Mon engine core
./build-engine.sh
rm tomcat-work/webapps/mon-engine-core.war; rm -r tomcat-work/webapps/mon-engine-core;   cp mon-engine-src/mon-engine-core/target/mon-engine-core.war tomcat-work/webapps/
rm tomcat-work/webapps/mon-engine-ws.war; rm -r tomcat-work/webapps/mon-engine-ws;   cp mon-engine-src/mon-engine-ws/target/mon-engine-ws.war tomcat-work/webapps/


cd mon-engine-parent/
~/apache-maven-3.0.4/bin/mvn -Dmaven.test.skip=true clean package
mkdir ~/mon-engine-work; 
rm ~/mon-engine-work/monitor.log

nohup ./eg-start-tomcat-here.sh > /dev/null &
nohup ./run-tomcat-loop.sh >> tomcat-work/logs/tomcat-out.log &

ps aux | less
http://www.cyberciti.biz/faq/bash-infinite-loop/

### Mon engine WS
~/eg-stop-tomcat.sh
cd mon-engine-src/
git pull
cd mon-engine-parent
~/apache-maven-3.0.4/bin/mvn -Dmaven.test.skip=true clean package
rm ~/apache-tomcat-7.0.37/webapps/mon-engine-ws.war
rm -r ~/apache-tomcat-7.0.37/webapps/mon-engine-ws
mv ../mon-engine-ws/target/mon-engine-ws.war ~/apache-tomcat-7.0.37/webapps/
~/eg-start-tomcat.sh


tail -n 1000 monitor.log

## Cryptography
http://stackoverflow.com/questions/2860943/suggestions-for-library-to-hash-passwords-in-java
http://www.javacodegeeks.com/2012/05/secure-password-storage-donts-dos-and.html

## Nginx
http://wiki.nognu.de/nginx_as_forward_proxy
 
## Jetty plugin
http://wiki.eclipse.org/Jetty/Feature/Jetty_Maven_Plugin#Quick_Start:_Get_Up_and_Running
mvn -Djetty.port=8093 -Dlog4j.configuration=file:src/log4j.properties jetty:run

## Cargo tomcat
mvn clean package; mvn org.codehaus.cargo:cargo-maven2-plugin:redeploy
Also:
http://mojo.codehaus.org/tomcat-maven-plugin/

### Setup tomcat
1) as usual,
2) http://according2albert.wordpress.com/2012/05/15/reading-properties-from-tomcat-classpath/
conf/catalina.properties: common.loader=SAME_AS_BEFORE,${catalina.home}/app-conf/
http://ralf.schaeftlein.de/2012/01/03/using-cargo-for-maven-war-deployments-to-tomcat-6-x/

## Testing RESTful services
http://stackoverflow.com/questions/2198435/is-there-a-rest-service-test-client-app-somewhere
http://code.google.com/p/chrome-rest-client/

## REST Auth
https://github.com/eugenp/REST#readme

## Storage
Spring Data JPA official documentation: http://static.springsource.org/spring-data/data-jpa/docs/current/reference/html/

## Email "from field" idea to try
Map headers = new HashMap();
map.put("To", "Claus Ibsen <davsclaus@apache.org>");
map.put("From", "James Strachan <jstrachan@apache.org>");
map.put("Subject", "Camel is cool");

# Updates from 3.1 to 3.2
1. prod.db.* was replaced with db.*
2. Updated liquibase updates to path independent
3. Password should be specified during registration
4. Added UUID field for entities.



-----------------------------------------------------

cd ~/mon-engine-src/mon-engine-parent/scripts/
~/apache-ant-1.9.3/bin/ant

cd ~/mon-engine-src/
git pull origin master
~/apache-maven-3.0.4/bin/mvn clean package -DskipTests -P prod
rm -r ~/tomcat-work/webapps/* ; ls ~/tomcat-work/webapps/
rm -r ~/tomcat-work/logs/* ; ls ~/tomcat-work/logs/
cp ~/mon-engine-src/target/mon-engine-core.war ~/tomcat-work/webapps
cp ~/mon-engine-front/target/mon-engine-front.war ~/tomcat-work/webapps/ROOT.war
cd ~ ; ./init-server-mon-loop.sh

rm -r ~/tomcat-work/webapps/* ; ls ~/tomcat-work/webapps/;rm -r ~/tomcat-work/logs/* ; ls ~/tomcat-work/logs/;cp ~/mon-engine-src/target/mon-engine-core.war ~/tomcat-work/webapps;cp ~/mon-engine-front/target/mon-engine-front.war ~/tomcat-work/webapps/ROOT.war

tail -n 1000 ~/tomcat-work/logs/mon-engine-core.log
tail -n 1000 ~/tomcat-work/logs/tomcat-out.log

cd ~
wget http://mirrors.digipower.vn/apache/tomcat/tomcat-7/v7.0.53/bin/apache-tomcat-7.0.53.zip
unzip apache-tomcat-7.0.53.zip
rm apache-tomcat-7.0.53.zip


rm ~/tomcat-work/webapps/*
cp ~/mon-engine-front/target/mon-engine-front.war ~/tomcat-work/webapps/ROOT.war

naming_strategy = com.github.snowindy.hibernate.UppercaseImprovedNamingStrategy