package com.github.snowindy.util.text;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class WordMatchUtilsTest extends Assert {
    @Test
    public void test_containsAny() {        
        assertTrue(WordMatchUtils.containsAnyKeywords(Arrays.asList("jAvA"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertTrue(WordMatchUtils.containsAnyKeywords(Arrays.asList("java"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertTrue(WordMatchUtils.containsAnyKeywords(Arrays.asList("gwt"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAnyKeywords(Arrays.asList("php"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAnyKeywords(Arrays.asList("java"), "javascript developer needed.\n Need a php developer, urgent."));
        assertTrue(WordMatchUtils.containsAnyKeywords(Arrays.asList("javaScript"), "javascript developer needed.\n Need a php developer, urgent."));
        assertFalse(WordMatchUtils.containsAnyKeywords(Arrays.asList(new String[]{}), "javascript developer needed.\n Need a php developer, urgent."));
        assertFalse(WordMatchUtils.containsAnyKeywords(Arrays.asList("gwt"), ""));
        
        assertTrue(WordMatchUtils.containsAnyKeywords(Arrays.asList("java developer"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAnyKeywords(Arrays.asList("gwt needed"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAnyKeywords(Arrays.asList("gwt needed"), ""));
    }

    @Test
    public void test_containsAll() {
        assertTrue(WordMatchUtils.containsAllKeywords(Arrays.asList("jAvA"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertTrue(WordMatchUtils.containsAllKeywords(Arrays.asList("jAvA","GWT"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAllKeywords(Arrays.asList("jAvA","php"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAllKeywords(Arrays.asList(new String[]{}), "javascript developer needed.\n Need a php developer, urgent."));
        
        assertTrue(WordMatchUtils.containsAllKeywords(Arrays.asList("gwt developer", "java"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAllKeywords(Arrays.asList("gwt needed", "java"), "GWT developer needed.\n Need a Java developer, urgent."));
    }

}
