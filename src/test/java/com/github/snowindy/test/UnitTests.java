package com.github.snowindy.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.github.snowindy.util.text.StringUtilTest;
import com.github.snowindy.util.text.WordMatchUtilsTest;

@RunWith(Suite.class)
@SuiteClasses({ StringUtilTest.class, WordMatchUtilsTest.class })
public class UnitTests {

}
