package com.github.snowindy.mon.beans;

import org.junit.Assert;
import org.junit.Test;


public class BeanNameUtilsTest {
    @Test
    public void test_getBeanNamePrefixFromDomainName() {
        Assert.assertEquals("odeskCom", BeanNameUtils.getBeanNamePrefixFromDomainName("odesk.com"));
        Assert.assertEquals("free_lanceRu", BeanNameUtils.getBeanNamePrefixFromDomainName("free-lance.ru"));
        Assert.assertEquals("flRu", BeanNameUtils.getBeanNamePrefixFromDomainName("fl.ru"));
        Assert.assertEquals("freelanceRu", BeanNameUtils.getBeanNamePrefixFromDomainName("freeLance.ru"));
        Assert.assertEquals("_1clancerRu", BeanNameUtils.getBeanNamePrefixFromDomainName("1clancer.ru"));
        Assert.assertEquals("foo_lanceCom", BeanNameUtils.getBeanNamePrefixFromDomainName("foo-lance.com"));
        Assert.assertEquals("foo_lan__ceCom", BeanNameUtils.getBeanNamePrefixFromDomainName("foo-lan_ce.com"));
        Assert.assertEquals("foo__lanceCom", BeanNameUtils.getBeanNamePrefixFromDomainName("foo_lance.com"));
        Assert.assertEquals("foo__lan__ceCom", BeanNameUtils.getBeanNamePrefixFromDomainName("foo_lan_ce.com"));
    }

    @Test
    public void test_getBeanNameFromDomainName() {
        Assert.assertEquals("odeskComSource", BeanNameUtils.getBeanNameFromDomainName("odesk.com", "Source"));
    }
}
