package com.github.snowindy.mon.beans;

import java.util.ArrayList;
import java.util.List;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Tested;
import mockit.VerificationsInOrder;

import org.apache.camel.ProducerTemplate;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.MonitoringRouteBuilder;
import com.github.snowindy.mon.entity.MonItemNotification;
import com.github.snowindy.mon.entity.MultiMonItemNotification;
import com.github.snowindy.mon.entity.User;
import com.github.snowindy.mon.entity.enums.EAggregationPolicy;
import com.github.snowindy.mon.i.INotificationsThresholdPeriodHandler;
import com.github.snowindy.spring.util.IPropertiesHolder;

public class NotificationFilterHandlerTest extends Assert {

    @Mocked
    private ProducerTemplate producerTemplate;

    @Mocked
    @Injectable
    private IPropertiesHolder propertiesHolder;

    @Mocked
    private PropertiesConfiguration propertiesConfiguration;

    @Tested
    NotificationsThresholdPeriodHandler handler;

    @Test
    public void test_maxNotificationsFiltering() {

        new NonStrictExpectations(handler) {
            {
                propertiesHolder.getConfig();
                result = propertiesConfiguration;

                propertiesConfiguration.getInt("notify.immediate.maxInPeriod.number",
                        INotificationsThresholdPeriodHandler.DEFAULT_MAX_NUMBER_MIXED);
                result = 2;

                propertiesConfiguration.getInt("notify.immediate.aggregationAvoided.maxInPeriod.number",
                        INotificationsThresholdPeriodHandler.DEFAULT_MAX_NUMBER_AVOIDED);
                result = 3;

                handler.getProducerTemplate();
                result = producerTemplate;
            }
        };

        final User user1 = new User();
        user1.setEmail("test1@dot.com");
        user1.setAggregationPolicy(EAggregationPolicy.MIXED);

        final User user2 = new User();
        user2.setEmail("test2@dot.com");
        user2.setAggregationPolicy(EAggregationPolicy.AVOIDED);

        final User user3 = new User();
        user3.setEmail("test3@dot.com");
        user3.setAggregationPolicy(EAggregationPolicy.FULL);

        final MonItemNotification notificationU1 = new MonItemNotification();
        notificationU1.setUser(user1);

        final MonItemNotification notificationU2 = new MonItemNotification();
        notificationU2.setUser(user2);

        final MonItemNotification notificationU3 = new MonItemNotification();
        notificationU3.setUser(user3);

        new Expectations() {
            {
                producerTemplate.sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_SINGLE, notificationU1);
                producerTemplate.sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_SINGLE, notificationU1);
            }
        };

        {
            // Go to single
            handler.process(notificationU1);
            handler.process(notificationU1);
        }

        {
            // Go to aggregation
            handler.process(notificationU1);
            handler.process(notificationU1);
            handler.process(notificationU3);
        }

        new Expectations() {
            {
                producerTemplate.sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_SINGLE, notificationU3);
            }
        };

        new NonStrictExpectations() {
            {
                producerTemplate.sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_MULTI, any);
            }
        };

        handler.processAggregated(null);

        new VerificationsInOrder() {
            {
                List<MultiMonItemNotification> args = new ArrayList<MultiMonItemNotification>();
                producerTemplate.sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_MULTI, withCapture(args));
                times = 1;
                assertEquals(1, args.size());

                assertEquals(2, args.get(0).getNotifications().size());
                assertEquals(user1, args.get(0).getUser());
            }
        };

        handler.cleanup(null);

        new Expectations() {
            {
                producerTemplate.sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_SINGLE, notificationU1);
                producerTemplate.sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_SINGLE, notificationU1);
                producerTemplate.sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_SINGLE, notificationU2);
                producerTemplate.sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_SINGLE, notificationU2);
                producerTemplate.sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_SINGLE, notificationU2);
            }
        };

        {
            // Go to single
            handler.process(notificationU1);
            handler.process(notificationU1);
            handler.process(notificationU2);
            handler.process(notificationU2);
            handler.process(notificationU2);
        }

        {
            // Go to aggregation
            handler.process(notificationU1);
            handler.process(notificationU1);
            handler.process(notificationU2);
        }

        new Expectations() {
            {
                producerTemplate.sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_SINGLE, notificationU2);
            }
        };
        new NonStrictExpectations() {
            {
                producerTemplate.sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_MULTI, any);
            }
        };

        handler.processAggregated(null);
        new VerificationsInOrder() {
            {
                List<MultiMonItemNotification> args = new ArrayList<MultiMonItemNotification>();
                producerTemplate.sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_MULTI, withCapture(args));
                times = 1;
                assertNotNull(args.get(0));
            }
        };

    }
}
