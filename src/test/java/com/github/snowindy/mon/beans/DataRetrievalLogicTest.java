package com.github.snowindy.mon.beans;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.github.snowindy.mon.beans.DataRetrievalLogic.DataRetrievalLogicException;
import com.github.snowindy.mon.beans.DataRetrievalLogic.IncorrectEnrichmentItemContent;
import com.github.snowindy.mon.beans.DataRetrievalLogic.IncorrectTopicItemContent;
import com.github.snowindy.mon.beans.DataRetrievalLogic.NoTopicElementsException;
import com.github.snowindy.mon.entity.StoredMonItem;

public class DataRetrievalLogicTest {

    private DataRetrievalLogic logic = new DataRetrievalLogic();

    @Test(expected = IncorrectEnrichmentItemContent.class)
    public void test_dataRetrievalLogic1() throws DataRetrievalLogicException {
        StoredMonItem mi = new StoredMonItem();
        mi.setUrl("url");
        mi.setTitle("title");
        logic.checkEnrichmentResult(mi, "", "");
    }

    @Test(expected = IncorrectEnrichmentItemContent.class)
    public void test_dataRetrievalLogic2() throws DataRetrievalLogicException {
        StoredMonItem mi = new StoredMonItem();
        mi.setTitle("title");
        mi.setTextBody("body");
        logic.checkEnrichmentResult(mi, "", "");
    }

    @Test(expected = IncorrectEnrichmentItemContent.class)
    public void test_dataRetrievalLogic3() throws DataRetrievalLogicException {
        StoredMonItem mi = new StoredMonItem();
        mi.setUrl("url");
        mi.setTextBody("body");
        logic.checkEnrichmentResult(mi, "", "");
    }

    @Test(expected = NoTopicElementsException.class)
    public void test_dataRetrievalLogic4() throws DataRetrievalLogicException {
        logic.checkTopicResult(new ArrayList<StoredMonItem>(), "", "", true);
    }

    @Test
    public void test_dataRetrievalLogic5() throws DataRetrievalLogicException {
        StoredMonItem mi = new StoredMonItem();
        mi.setUrl("url");
        mi.setTitle("title");
        List<StoredMonItem> items = new ArrayList<StoredMonItem>();
        items.add(mi);

        logic.checkTopicResult(items, "", "", true);
        
        assertEquals(0, items.size());
    }
    
    @Test
    public void test_dataRetrievalLogic6() throws DataRetrievalLogicException {
        StoredMonItem mi = new StoredMonItem();
        mi.setUrl("url");
        mi.setTitle("title");
        List<StoredMonItem> items = new ArrayList<StoredMonItem>();
        items.add(mi);

        logic.checkTopicResult(items, "", "", false);
    }
}
