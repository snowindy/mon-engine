package com.github.snowindy.mon.beans;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.beans.WordMatcher;

public class WordMatcherTest extends Assert {

    List<String> EMPTY = Arrays.asList(new String[] {});

    @Test
    public void test_matches() {
        WordMatcher matcher = new WordMatcher();

        assertTrue(matcher.matches(Arrays.asList("java", "gwt"), EMPTY, EMPTY,
                "GWT developer needed.\n Need a Java developer, urgent."));
        
        assertFalse(matcher.matches(Arrays.asList("java", "gwt"), EMPTY, Arrays.asList("developer"),
                "GWT developer needed.\n Need a Java developer, urgent."));
        
        assertFalse(matcher.matches(EMPTY, Arrays.asList("java", "gwt"), Arrays.asList("developer"),
                "GWT developer needed.\n Need a Java developer, urgent."));
        
        assertTrue(matcher.matches(EMPTY, Arrays.asList("java", "php"), Arrays.asList("dev"),
                "GWT developer needed.\n Need a Java developer, urgent."));
        
        assertFalse(matcher.matches(Arrays.asList("java", "gwt", "php"), EMPTY, Arrays.asList("dev"),
                "GWT developer needed.\n Need a Java developer, urgent."));

    }
}
