package com.github.snowindy.mon.storage;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.snowindy.mon.entity.House;
import com.github.snowindy.mon.entity.HouseTopic;
import com.github.snowindy.mon.entity.Token;
import com.github.snowindy.mon.entity.User;
import com.github.snowindy.mon.entity.repository.HouseRepository;
import com.github.snowindy.mon.entity.repository.HouseTopicRepository;
import com.github.snowindy.mon.entity.repository.MonItemRepository;
import com.github.snowindy.mon.entity.repository.TokenRepository;
import com.github.snowindy.mon.entity.repository.UserRepository;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:storage/storage-context.xml", "classpath:storage/unit-test-beans.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup({ "liquibaseDataDeletion.xml", "houseData.xml" })
@Transactional
@SuppressWarnings("unchecked")
public class RepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HouseRepository houseRepository;

    @Autowired
    private HouseTopicRepository houseTopicRepository;

    @Autowired
    private MonItemRepository monItemRepository;

    @Autowired
    private TokenRepository tokenRepository;

    @Test
    @DatabaseSetup({ "userData.xml" })
    public void test_findUser() throws ParseException {
        {
            User user1 = userRepository.findOneByEmail("u1@test.com");

            assertEquals(2, user1.getAnyKeywords().size());
            assertThat(user1.getAnyKeywords(), contains(equalTo("perl"), equalTo("css")));
        }
        {
            Date deadline = new SimpleDateFormat("yyyy-MM-dd").parse("2013-12-15");

            {
                List<User> users = userRepository.findAllByHouseWithNoSearchDateBefore(
                        houseRepository.findOneByDomainName("foo1-lance.com"), deadline);

                assertThat(users.size(), equalTo(1));
                assertThat(users, contains(hasProperty("email", containsString("u1"))));
            }
            {
                List<User> users = userRepository.findAllByHouseWithNoSearchDateBefore(
                        houseRepository.findOneByDomainName("foo2-lance.com"), deadline);

                assertThat(users.size(), equalTo(2));
                assertThat(
                        users,
                        contains(hasProperty("email", containsString("u2")), hasProperty("email", containsString("u3"))));
            }
            {
                List<User> users = userRepository.findAllByHouseWithNoSearchDateBefore(
                        houseRepository.findOneByDomainName("foo3-lance.com"), deadline);

                assertThat(users.size(), equalTo(0));
            }
            {
                List<User> users = userRepository.findAllByHouseWithNoSearchDateBefore(
                        houseRepository.findOneByDomainName("foo4-lance.com"), deadline);

                assertThat(users.size(), equalTo(1));
                assertThat(users, contains(hasProperty("email", containsString("u5"))));
            }
        }
    }

    @Test
    public void test_findActiveHouses() {
        List<House> houses = houseRepository.findAllByActive(true);

        assertEquals(3, houses.size());
        assertThat(
                houses,
                contains(hasProperty("domainName", equalTo("foo1-lance.com")),
                        hasProperty("domainName", equalTo("foo2-lance.com")),
                        hasProperty("domainName", equalTo("foo3-lance.com"))));
    }

    @Test
    @DatabaseSetup({ "houseTopicData.xml" })
    public void test_findAllByHouseAndActive() {

        List<HouseTopic> topics = houseTopicRepository.findAllByHouseAndActive(
                houseRepository.findOneByDomainName("foo1-lance.com"), true);

        Assert.assertEquals(1, topics.size());
        assertThat(topics, contains(hasProperty("title", equalTo("t1"))));
    }

    @Test
    @DatabaseSetup({ "houseTopicData.xml", "monItemData.xml" })
    public void test_cleanupMonItem() throws ParseException {

        assertEquals(10, monItemRepository.count());

        monItemRepository.deleteAllBefore(new SimpleDateFormat("yyyy-MM-dd").parse("2013-12-15"));

        assertEquals(6, monItemRepository.count());
        assertThat(
                monItemRepository.findAll(),
                contains(hasProperty("url", equalTo("u5")), hasProperty("url", equalTo("u6")),
                        hasProperty("url", equalTo("u7")), hasProperty("url", equalTo("u8")),
                        hasProperty("url", equalTo("u9")), hasProperty("url", equalTo("u10"))));
    }

    @Test
    @DatabaseSetup({ "houseTopicData.xml", "monItemData.xml" })
    public void test_countMonItem() throws ParseException {

        assertEquals(2, monItemRepository.countForHouseAfterDate(houseRepository.findOneByDomainName("foo1-lance.com"),
                new SimpleDateFormat("yyyy-MM-dd").parse("2013-12-15")));
        assertEquals(3, monItemRepository.countForHouseAfterDate(houseRepository.findOneByDomainName("foo2-lance.com"),
                new SimpleDateFormat("yyyy-MM-dd").parse("2013-12-15")));

        assertEquals(0, monItemRepository.countForHouseAfterDate(houseRepository.findOneByDomainName("foo3-lance.com"),
                new SimpleDateFormat("yyyy-MM-dd").parse("2013-12-15")));

        assertEquals(0, monItemRepository.countByUrl("not-exists"));
        assertEquals(1, monItemRepository.countByUrl("u1"));
    }

    @Test
    @DatabaseSetup({ "tokenData.xml" })
    public void test_token() {
        Token token = tokenRepository.findOneByUuid("uuid1");

        assertThat(token, hasProperty("entityId", equalTo("1")));
    }

}
