package com.github.snowindy.mon.routes;

import org.apache.camel.test.junit4.CamelTestSupport;

public class MonitoringRouteBuilderTest extends CamelTestSupport {
    
//
//
//    @Override
//    protected RouteBuilder createRouteBuilder() throws Exception {
//        MonitoringRouteBuilder routeBuilder = new MonitoringRouteBuilder();
//
//        return routeBuilder;
//    }
//
//    IMonItemNotifier mockSingleNotifier = mock(IMonItemNotifier.class);
//    IMultiMonItemNotifier mockMultiNotifier = mock(IMultiMonItemNotifier.class);
//    IUserProvider mockUserSource = mock(IUserProvider.class);
//    IJobEnricher mockMonItemEnricher = mock(IJobEnricher.class);
//    IUserProvider mockUserProvider = mock(IUserProvider.class);
//    IMonItemSource mockMonItemSource = mock(IMonItemSource.class);
//    IMonItemHandler mockNewMonItemEnrichedHandler = mock(IMonItemHandler.class);
//    IPropertiesHolder propertiesHolder = mock(IPropertiesHolder.class);
//    PropertiesConfiguration propertiesConfiguration = mock(PropertiesConfiguration.class);
//
//    @Injectable
//    MockStorage storage = new MockStorage();
//    HouseSource houseSource = new HouseSource();
//    HouseTopicsSource houseTopicsSource = new HouseTopicsSource();
//    CountThresholdNewMonItemFilter filter = new CountThresholdNewMonItemFilter();
//    UserMonItemMatcher monItemUserMatcher = new UserMonItemMatcher();
//    NotificationsThresholdPeriodHandler notificationsThresholdPeriodHandler = new NotificationsThresholdPeriodHandler();
//    NotificationDestinationFilter notificationDestinationFilter = new NotificationDestinationFilter();
//
//    {
//        monItemUserMatcher.setUserProvider(mockUserProvider);
//        houseSource.setStorage(storage);
//        houseTopicsSource.setStorage(storage);
//        notificationsThresholdPeriodHandler.setMaxNotificationsInPeriod(10);
//    }
//
//    @Override
//    public String isMockEndpoints() {
//        return "*";
//    }
//
//    @Before
//    public void beforeEach() {
//        notificationsThresholdPeriodHandler.setProducerTemplate(template);
//        notificationDestinationFilter.setPropertiesHolder(propertiesHolder);
//
//        when(propertiesHolder.getConfig()).thenReturn(propertiesConfiguration);
//    }
//
//    @Override
//    protected JndiRegistry createRegistry() throws Exception {
//        JndiRegistry jndi = super.createRegistry();
//
//        for (House house : storage.getAllHouses()) {
//            jndi.bind(BeanNameUtils.getBeanNameFromDomainName(house.getDomainName(),
//                    MonitoringRouteBuilder.BEAN_NAME_SUFFIX_MON_ITEM_SOURCE), mockMonItemSource);
//            jndi.bind(BeanNameUtils.getBeanNameFromDomainName(house.getDomainName(),
//                    MonitoringRouteBuilder.BEAN_NAME_SUFFIX_MON_ITEM_ENRICHER), mockMonItemEnricher);
//        }
//
//        // more info on bean mocking:
//        // http://stackoverflow.com/questions/8919894/how-to-unit-test-a-route-with-a-bean-which-will-access-db
//
//        jndi.bind(MonitoringRouteBuilder.BEAN_HOUSE_SOURCE, houseSource);
//        jndi.bind(MonitoringRouteBuilder.BEAN_HOUSE_TOPICS_SOURCE, houseTopicsSource);
//        jndi.bind(MonitoringRouteBuilder.BEAN_NEW_MON_ITEM_FILTER, filter);
//        jndi.bind(MonitoringRouteBuilder.BEAN_MON_ITEM_USER_MATCHER, monItemUserMatcher);
//        jndi.bind(MonitoringRouteBuilder.BEAN_NOTIFIER_SINGLE, mockSingleNotifier);
//        jndi.bind(MonitoringRouteBuilder.BEAN_NOTIFIER_MULTI, mockMultiNotifier);
//        jndi.bind(MonitoringRouteBuilder.BEAN_NEW_MON_ITEM_ENRICHED_HANDLER, mockNewMonItemEnrichedHandler);
//        jndi.bind(MonitoringRouteBuilder.BEAN_NOTIFICATION_THRESHOLD_PERIOD_HANDLER,
//                notificationsThresholdPeriodHandler);
//        jndi.bind(MonitoringRouteBuilder.BEAN_NOTIFICATION_DESTINATION_FILTER, notificationDestinationFilter);
//
//        return jndi;
//    }
//
//    @Test
//    public void test_getHouses() throws Exception {
//
//        // Testing test itself
//        Assert.assertTrue(storage.getActiveHouses().size() > 0);
//
//        MockEndpoint queue = getMockEndpoint("mock:" + MonitoringRouteBuilder.QUEUE_HOUSES);
//        queue.expectedMessageCount(storage.getActiveHouses().size());
//        queue.expectedBodiesReceived(storage.getActiveHouses());
//
//        sendBody(SchedulerRouteBuilder.TOPIC_MAIN_TIMER_EVENT, "");
//    }
//
//    @Test
//    public void test_getHouseTopics() throws Exception {
//        // Testing test itself
//        List<HouseTopic> activeTopics = storage.getActiveTopicsByHouse(storage.house1.getDomainName());
//        Assert.assertTrue(activeTopics.size() > 0);
//
//        MockEndpoint queue = getMockEndpoint("mock:" + HouseTopicRouter.resolveHouseTopicChannel(storage.house1));
//        queue.expectedMessageCount(activeTopics.size());
//        queue.expectedBodiesReceived(activeTopics);
//
//        sendBody(MonitoringRouteBuilder.QUEUE_HOUSES, storage.house1);
//    }
//
//    @Test
//    public void test_getItems() throws Exception {
//
//        HouseTopic houseTopic = storage.house1Topic1;
//
//        MonItem monItem = createMonItemPreview("1", houseTopic);
//        ArrayList<MonItem> itemsFromSource = new ArrayList<MonItem>();
//        itemsFromSource.add(monItem);
//
//        when(mockMonItemSource.getMonItems(storage.house1Topic1)).thenReturn(itemsFromSource).thenReturn(
//                emptyMonItemList);
//
//        MockEndpoint queue = getMockEndpoint("mock:" + MonitoringRouteBuilder.QUEUE_ITEMS_UNFILTERED);
//        queue.expectedMessageCount(1);
//        queue.expectedBodiesReceived(monItem);
//
//        sendBody(HouseTopicRouter.resolveHouseTopicChannel(houseTopic.getHouse()), houseTopic);
//        // These tests: should not gen an item
//        sendBody(HouseTopicRouter.resolveHouseTopicChannel(houseTopic.getHouse()), storage.house1Topic2);
//        sendBody(HouseTopicRouter.resolveHouseTopicChannel(houseTopic.getHouse()), houseTopic);
//    }
//
//    @Test
//    public void test_filteringItems() throws Exception {
//
//        HouseTopic houseTopic = storage.house1Topic1;
//
//        MonItem monItem1 = createMonItemPreview("1", houseTopic);
//        MonItem monItem2 = createMonItemPreview("2", houseTopic);
//
//        filter.setInitialThreshold(1);
//
//        MockEndpoint queue = getMockEndpoint("mock:" + EnrichmentRouter.resolveEnrichmentChannel(houseTopic.getHouse()));
//        queue.expectedMessageCount(1);
//        queue.expectedBodiesReceived(monItem2);
//
//        sendBody(MonitoringRouteBuilder.QUEUE_ITEMS_UNFILTERED, monItem1);
//        sendBody(MonitoringRouteBuilder.QUEUE_ITEMS_UNFILTERED, monItem2);
//    }
//
//    @Test
//    public void test_enrichingItems() throws Exception {
//
//        HouseTopic houseTopic = storage.house1Topic1;
//
//        final MonItem monItem1 = createMonItemPreview("1", houseTopic);
//        final MonItem monItem1Enriched = createMonItemPreview("1-enriched", houseTopic);
//        monItem1Enriched.setTextBody("Body 1");
//
//        when(mockMonItemEnricher.enrich(monItem1)).thenReturn(monItem1Enriched);
//
//        final MockEndpoint queue = getMockEndpoint("mock:" + MonitoringRouteBuilder.QUEUE_NEW_ITEMS_ENRICHED);
//        queue.expectedMessageCount(1);
//        queue.expects(new Runnable() {
//            @Override
//            public void run() {
//                // Assure we don't race with queue handler
//                RouteTestUtils.sleepDefault();
//
//                Assert.assertEquals(monItem1Enriched.getTextBody(), queue.getReceivedExchanges().get(0).getIn()
//                        .getBody(MonItem.class).getTextBody());
//                verify(mockNewMonItemEnrichedHandler).handle(monItem1Enriched);
//            }
//        });
//
//        sendBody(MonitoringRouteBuilder.QUEUE_NEW_ITEMS_NOT_ENRICHED, monItem1);
//
//    }
//
//    @Test
//    public void test_itemMatchAndNotify() throws Exception {
//        HouseTopic houseTopic1 = storage.house1Topic1;
//        HouseTopic houseTopic2 = storage.house2Topic1;
//
//        final MonItem monItem1 = createMonItem("1", houseTopic1, "java developer needed body1");
//        final MonItem monItem2 = createMonItem("2", houseTopic2, "java developer needed body2");
//
//        final User user1 = User.getBuilder("testUser@gmail.com").anyKeywords(Arrays.asList("java"))
//                .housesOfInterest(Arrays.asList(houseTopic1.getHouse())).build();
//
//        final User user2 = User.getBuilder("testUser1@gmail.com").anyKeywords(Arrays.asList("java"))
//                .housesOfInterest(Arrays.asList(houseTopic1.getHouse(), houseTopic2.getHouse())).build();
//
//        final User user3 = User.getBuilder("testUser3@mail.ru").anyKeywords(Arrays.asList("java"))
//                .housesOfInterest(Arrays.asList(houseTopic1.getHouse())).build();
//
//        when(mockUserProvider.getUsersInterestedInHouse(houseTopic1.getHouse()))
//                .thenReturn(Arrays.asList(user1, user2, user3));
//        when(mockUserProvider.getUsersInterestedInHouse(houseTopic2.getHouse())).thenReturn(Arrays.asList(user2));
//        when(propertiesConfiguration.getStringArray(anyString())).thenReturn(new String[] { "@mail.ru" });
//
//        final MockEndpoint queue = getMockEndpoint("mock:" + MonitoringRouteBuilder.QUEUE_NOTIFICATIONS);
//        queue.expectedMessageCount(3);
//        queue.expects(new Runnable() {
//            @Override
//            public void run() {
//                // TODO Order here is not important. It's like this just because
//                // java is not so cool about collection traversal..
//                Assert.assertEquals(monItem1,
//                        queue.getReceivedExchanges().get(0).getIn().getBody(MonItemNotification.class).getJob());
//                Assert.assertEquals(user1,
//                        queue.getReceivedExchanges().get(0).getIn().getBody(MonItemNotification.class).getUser());
//
//                Assert.assertEquals(monItem1,
//                        queue.getReceivedExchanges().get(1).getIn().getBody(MonItemNotification.class).getJob());
//                Assert.assertEquals(user2,
//                        queue.getReceivedExchanges().get(1).getIn().getBody(MonItemNotification.class).getUser());
//
//                Assert.assertEquals(monItem2,
//                        queue.getReceivedExchanges().get(2).getIn().getBody(MonItemNotification.class).getJob());
//                Assert.assertEquals(user2,
//                        queue.getReceivedExchanges().get(2).getIn().getBody(MonItemNotification.class).getUser());
//            }
//        });
//
//        sendBody(MonitoringRouteBuilder.QUEUE_NEW_ITEMS_ENRICHED, monItem1);
//        sendBody(MonitoringRouteBuilder.QUEUE_NEW_ITEMS_ENRICHED, monItem2);
//    }
//
//    @Test
//    public void test_notifyMaxItemsHandler() throws Exception {
//
//        notificationsThresholdPeriodHandler.setMaxNotificationsInPeriod(1);
//
//        HouseTopic houseTopic1 = storage.house1Topic1;
//
//        final MonItem monItem1 = createMonItem("1", houseTopic1, "java developer needed body1");
//        final MonItem monItem2 = createMonItem("2", houseTopic1, "java developer needed body2");
//
//        final User user1 = User.getBuilder("testUser@gmail.com").anyKeywords(Arrays.asList("java"))
//                .housesOfInterest(Arrays.asList(houseTopic1.getHouse())).build();
//
//        MonItemNotification notification1 = new MonItemNotification();
//        notification1.setJob(monItem1);
//        notification1.setUser(user1);
//
//        MonItemNotification notification2 = new MonItemNotification();
//        notification2.setJob(monItem2);
//        notification2.setUser(user1);
//
//        final MockEndpoint queue = getMockEndpoint("mock:" + MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_SINGLE);
//        queue.expectedBodiesReceived(notification1);
//
//        final MockEndpoint queueMulti = getMockEndpoint("mock:" + MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_MULTI);
//        queueMulti.expectedMessageCount(1);
//
//        sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS, notification1);
//        sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS, notification2);
//
//        // Assure we don't race with queue handler
//        RouteTestUtils.sleep(2000);
//
//        notificationsThresholdPeriodHandler.processAggregated(null);
//
//        // Assure we don't race with queue handler
//        RouteTestUtils.sleepDefault();
//    }
//
//    private List<MonItem> emptyMonItemList = new ArrayList<MonItem>();
//
//    private MonItem createMonItemPreview(String id, HouseTopic houseTopic) {
//        MonItem monItem = new MonItem();
//        monItem.setHouseTopic(houseTopic);
//        monItem.setTitle("Item " + id);
//        monItem.setUrl(houseTopic.getUrl() + "/item" + id);
//
//        return monItem;
//    }
//
//    private MonItem createMonItem(String id, HouseTopic houseTopic, String body) {
//        MonItem monItem = createMonItemPreview(id, houseTopic);
//        monItem.setTextBody(body);
//        return monItem;
//    }
//
//    @After
//    @Override
//    public void tearDown() throws Exception {
//        assertMockEndpointsSatisfied();
//        super.tearDown();
//        filter.reInit();
//        notificationsThresholdPeriodHandler.cleanup(null);
//        notificationsThresholdPeriodHandler.setMaxNotificationsInPeriod(10);
//    }
}
