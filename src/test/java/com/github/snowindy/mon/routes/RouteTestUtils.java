package com.github.snowindy.mon.routes;

public class RouteTestUtils {
    
    public static void sleepDefault() {
        sleep(1000);
    }
    
    public static void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
