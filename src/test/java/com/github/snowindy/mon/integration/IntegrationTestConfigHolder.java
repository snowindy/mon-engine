package com.github.snowindy.mon.integration;

import java.io.File;

import org.apache.commons.configuration.PropertiesConfiguration;

import com.github.snowindy.spring.util.IPropertiesHolder;

public class IntegrationTestConfigHolder {

    public IntegrationTestConfigHolder() {
        try {
            config = new PropertiesConfiguration(new File("src/test/resources/conf/integration-test.properties"));
            config.setThrowExceptionOnMissing(true);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    protected IPropertiesHolder propertiesHolder = new IPropertiesHolder() {

        @Override
        public PropertiesConfiguration getConfig() {
            return config;

        }
    };

    protected PropertiesConfiguration config;
}
