package com.github.snowindy.mon.integration;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.github.snowindy.mon.parse.EtxtRuCookieLoginHelper;
import com.github.snowindy.mon.parse.LoginException;
import com.github.snowindy.util.net.PooledHttpGateway;
import com.github.snowindy.util.net.ex.IncorrectStatusCodeException;

public class EtxtRuCookieLoginHelperIT extends IntegrationTestConfigHolder {

    @Test
    public void test_logIn_searchPage() throws LoginException, IncorrectStatusCodeException {
        EtxtRuCookieLoginHelper helper = new EtxtRuCookieLoginHelper();

        PooledHttpGateway gw = new PooledHttpGateway();
        gw.init();

        helper.setPropertiesHolder(propertiesHolder);
        helper.setHttpGateway(gw);

        helper.setLogin(config.getString("parser.house.etxt.ru.login"));
        helper.setPassword(config.getString("parser.house.etxt.ru.password"));

        String queryUrl = "https://www.etxt.ru/admin.php?mod=tasks&lib=main";

        helper.logIn();

        String text = gw.readHtmlPageAsString(queryUrl, helper.getLoggedInCookieStore());

        assertTrue(helper.checkLoggedIn(text));
    }

    @Test
    public void test_logIn_mainPage() throws LoginException, IncorrectStatusCodeException {
        EtxtRuCookieLoginHelper helper = new EtxtRuCookieLoginHelper();

        PooledHttpGateway gw = new PooledHttpGateway();
        gw.init();

        helper.setPropertiesHolder(propertiesHolder);
        helper.setHttpGateway(gw);

        helper.setLogin(config.getString("parser.house.etxt.ru.login"));
        helper.setPassword(config.getString("parser.house.etxt.ru.password"));

        String queryUrl = "https://www.etxt.ru/";

        assertFalse(helper.isLoggedIn());

        helper.logIn();

        String text = gw.readHtmlPageAsString(queryUrl, helper.getLoggedInCookieStore());

        assertTrue(helper.checkLoggedIn(text));
        assertTrue(helper.isLoggedIn());
    }

    @Test
    public void test_logInWrongLogin() throws LoginException {
        EtxtRuCookieLoginHelper helper = new EtxtRuCookieLoginHelper();

        PooledHttpGateway gw = new PooledHttpGateway();
        gw.init();

        helper.setPropertiesHolder(propertiesHolder);
        helper.setHttpGateway(gw);

        helper.setLogin(config.getString("parser.house.etxt.ru.login"));
        helper.setPassword(config.getString("parser.house.etxt.ru.password") + 1);

        try {
            helper.logIn();
            fail();
        } catch (LoginException e) {
            // ok
        }

        assertFalse(helper.isLoggedIn());
    }

}
