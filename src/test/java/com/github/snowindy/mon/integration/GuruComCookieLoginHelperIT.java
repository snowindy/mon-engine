package com.github.snowindy.mon.integration;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.github.snowindy.mon.parse.GuruComCookieLoginHelper;
import com.github.snowindy.mon.parse.LoginException;
import com.github.snowindy.util.net.PooledHttpGateway;
import com.github.snowindy.util.net.ex.IncorrectStatusCodeException;

public class GuruComCookieLoginHelperIT extends IntegrationTestConfigHolder {

    @Test
    public void test_logIn() throws LoginException, IncorrectStatusCodeException {
        GuruComCookieLoginHelper helper = new GuruComCookieLoginHelper();

        PooledHttpGateway gw = new PooledHttpGateway();
        gw.init();

        helper.setPropertiesHolder(propertiesHolder);
        helper.setHttpGateway(gw);

        helper.setLogin(config.getString("parser.house.guru.com.login"));
        helper.setPassword(config.getString("parser.house.guru.com.password"));

        String queryUrl = "http://www.guru.com/d/jobs/";

        helper.logIn();

        String text = gw.readHtmlPageAsString(queryUrl, helper.getLoggedInCookieStore());

        assertTrue(helper.checkLoggedIn(text));
    }

    @Test
    public void test_logInWrongLogin() throws LoginException {
        GuruComCookieLoginHelper helper = new GuruComCookieLoginHelper();

        PooledHttpGateway gw = new PooledHttpGateway();
        gw.init();

        helper.setPropertiesHolder(propertiesHolder);
        helper.setHttpGateway(gw);

        helper.setLogin("foo");
        helper.setPassword("bar");

        try {
            helper.logIn();
            fail();
        } catch (LoginException e) {
            // ok
        }

        assertFalse(helper.isLoggedIn());
    }

}
