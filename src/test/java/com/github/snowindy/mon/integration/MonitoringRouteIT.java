package com.github.snowindy.mon.integration;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MonitoringRouteIT extends Assert {
    private AbstractApplicationContext applicationContext;
    private ProducerTemplate template;

    @Before
    public void setUp() throws Exception {
        applicationContext = new ClassPathXmlApplicationContext("WEB-INF/beans.xml",
                "WEB-INF/mock/mock-beans.xml");
        CamelContext camelContext = getCamelContext();
        template = camelContext.createProducerTemplate();
    }

    protected CamelContext getCamelContext() throws Exception {
        return applicationContext.getBean("camel", CamelContext.class);
    }

    @Test
    public void testMainRoute() throws Exception {
        // Order order = new Order(2);
        // order.addItem(DrinkType.ESPRESSO, 2, true);
        // order.addItem(DrinkType.CAPPUCCINO, 4, false);
        // order.addItem(DrinkType.LATTE, 4, false);
        // order.addItem(DrinkType.MOCHA, 2, false);
        //
        // template.sendBody("direct:cafe", order);

        Thread.sleep(15000);
    }

    @After
    public void tearDown() throws Exception {
        if (applicationContext != null) {
            applicationContext.stop();
        }
    }
}
