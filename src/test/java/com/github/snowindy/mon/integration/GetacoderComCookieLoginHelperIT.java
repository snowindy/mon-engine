package com.github.snowindy.mon.integration;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.github.snowindy.mon.parse.GetacoderComCookieLoginHelper;
import com.github.snowindy.mon.parse.LoginException;
import com.github.snowindy.util.net.PooledHttpGateway;
import com.github.snowindy.util.net.ex.IncorrectStatusCodeException;

public class GetacoderComCookieLoginHelperIT extends IntegrationTestConfigHolder {

    @Test
    public void test_logIn() throws LoginException, IncorrectStatusCodeException {
        GetacoderComCookieLoginHelper helper = new GetacoderComCookieLoginHelper();

        PooledHttpGateway gw = new PooledHttpGateway();
        gw.init();

        helper.setPropertiesHolder(propertiesHolder);
        helper.setHttpGateway(gw);

        helper.setLogin(config.getString("parser.house.getacoder.com.login"));
        helper.setPassword(config.getString("parser.house.getacoder.com.password"));

        String queryUrl = "http://www.getacoder.com/users/manage.php";

        helper.logIn();

        String text = gw.readHtmlPageAsString(queryUrl, helper.getLoggedInCookieStore());

        assertTrue(helper.checkLoggedIn(text));
    }

    @Test
    public void test_logInWrongLogin() throws LoginException {
        GetacoderComCookieLoginHelper helper = new GetacoderComCookieLoginHelper();

        PooledHttpGateway gw = new PooledHttpGateway();
        gw.init();

        helper.setPropertiesHolder(propertiesHolder);
        helper.setHttpGateway(gw);

        helper.setLogin(config.getString("parser.house.getacoder.com.login"));
        helper.setPassword(config.getString("parser.house.getacoder.com.password") + 1);

        try {
            helper.logIn();
            fail();
        } catch (LoginException e) {
            // ok
        }

        assertFalse(helper.isLoggedIn());
    }

}
