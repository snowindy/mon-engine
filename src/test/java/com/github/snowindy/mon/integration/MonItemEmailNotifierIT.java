package com.github.snowindy.mon.integration;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.github.snowindy.mon.beans.SingleMonItemEmailNotifier;
import com.github.snowindy.mon.entity.House;
import com.github.snowindy.mon.entity.HouseTopic;
import com.github.snowindy.mon.entity.StoredMonItem;
import com.github.snowindy.mon.entity.MonItemNotification;
import com.github.snowindy.mon.entity.User;
import com.github.snowindy.spring.util.IPropertiesHolder;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:webapp/WEB-INF/beans-def.xml",
        "file:webapp/WEB-INF/mock/integration-test-suport.xml" })
public class MonItemEmailNotifierIT {

    @Autowired
    private IPropertiesHolder propertiesHolder;

    @Autowired
    private SingleMonItemEmailNotifier emailNotifier;

    @Test
    public void testMessage() {
        Assert.assertNotNull(emailNotifier);

        MonItemNotification notification = new MonItemNotification();
        House house = House.getBuilder("odesk.com").urlPrefix("https://www.odesk.com").build();

        HouseTopic houseTopic = HouseTopic.getBuilder("All jobs").house(house).url("Https://odesk.com/all-jobs")
                .build();

        StoredMonItem job = new StoredMonItem();
        job.setTitle("Groovy developer needed");
        job.setUrl("https://www.odesk.com/groovy-dev-123");
        job.setTextBody("A powerful startup team needs a strong Groovy developer.\n Full time.\n Apply only if you are capable");
        job.setHouseTopic(houseTopic);

        User user = User.getBuilder(propertiesHolder.getConfig().getString("email.recipient"))
                .anyKeywords(Arrays.asList("java", "groovy")).build();

        notification.setJob(job);
        notification.setUser(user);

        emailNotifier.notify(notification);
    }

}
