package com.github.snowindy.mon.integration;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import com.github.snowindy.mon.parse.LoginException;
import com.github.snowindy.mon.parse.ODeskComApiLoginHelper;
import com.github.snowindy.util.net.PooledHttpGateway;
import com.github.snowindy.util.net.ex.IncorrectStatusCodeException;

public class ODeskComApiLoginHelperIT extends IntegrationTestConfigHolder {

    @Test
    public void test_logIn() throws LoginException, IncorrectStatusCodeException, UnsupportedEncodingException {
        ODeskComApiLoginHelper helper = new ODeskComApiLoginHelper();

        PooledHttpGateway gw = new PooledHttpGateway();
        gw.init();

        helper.setPropertiesHolder(propertiesHolder);

        helper.setLogin(config.getString("parser.house.odesk.com.api.consumer.key"));
        helper.setPassword(config.getString("parser.house.odesk.com.api.consumer.secret"));
        helper.setAccessToken(config.getString("parser.house.odesk.com.api.token.key"));
        helper.setAccessTokenSecret(config.getString("parser.house.odesk.com.api.token.secret"));

        String queryUrl = "https://www.upwork.com/api/profiles/v2/search/jobs.json?q=*&sort=create_time%20desc&paging=0%3B50";
        //String queryUrl = "https://www.odesk.com/api/profiles/v1/jobs/~01f40d13298fae0c7c.json";

        
        Pair<String, String> header = helper.getHeader(queryUrl);

        String text = gw.readHtmlPageAsString(queryUrl, header);
        System.out.println(text);

        queryUrl = "https://www.upwork.com/api/profiles/v1/jobs/~015350da140adece81.json";
        header = helper.getHeader(queryUrl);
        text = gw.readHtmlPageAsString(queryUrl, header);
        System.out.println(text);

        
        assertTrue(helper.checkLoggedIn(text));
    }

    @Test
    public void test_logInWrongLogin() throws LoginException {
        ODeskComApiLoginHelper helper = new ODeskComApiLoginHelper();

        PooledHttpGateway gw = new PooledHttpGateway();
        gw.init();

        helper.setPropertiesHolder(propertiesHolder);

        helper.setLogin(config.getString("parser.house.odesk.com.api.consumer.key"));
        helper.setPassword(config.getString("parser.house.odesk.com.api.consumer.secret"));
        helper.setAccessToken(config.getString("parser.house.odesk.com.api.token.key"));
        helper.setAccessTokenSecret(config.getString("parser.house.odesk.com.api.token.secret") + 1);

        Pair<String, String> header = helper.getHeader("https://www.odesk.com/api/profiles/v2/search/jobs.json?q=php");

        try {
            gw.readHtmlPageAsString("https://www.odesk.com/api/profiles/v2/search/jobs.json?q=php", header);
            fail();
        } catch (IncorrectStatusCodeException e) {
            assertFalse(helper.checkLoggedIn(e.getResponseText()));
        }
    }

}
