package com.github.snowindy.mon.integration;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.parse.ELanceComApiLoginHelper;
import com.github.snowindy.mon.parse.LoginException;
import com.github.snowindy.util.net.PooledHttpGateway;

public class ElanceComApiLoginHelperIT extends IntegrationTestConfigHolder {

    @Test
    public void test_logIn() throws LoginException {
        ELanceComApiLoginHelper helper = new ELanceComApiLoginHelper();

        PooledHttpGateway gw = new PooledHttpGateway();
        gw.init();

        helper.setHttpGateway(gw);
        helper.setPropertiesHolder(propertiesHolder);

        helper.setLogin(config.getString("parser.house.elance.com.api.user"));
        helper.setPassword(config.getString("parser.house.elance.com.api.password"));

        Assert.assertFalse(helper.isLoggedIn());

        helper.logIn();

        Assert.assertTrue(helper.isLoggedIn());

        Assert.assertTrue(StringUtils.startsWith(
                helper.updateUrlWithLoginParam("https://api.elance.com/api2/jobs?keywords=php"),
                "https://api.elance.com/api2/jobs?keywords=php&access_token="));
        
        String res = gw.readHtmlPageAsString(helper.updateUrlWithLoginParam("https://api.elance.com/api2/jobs?rpp=50"));
        System.out.println(res);
    }

    @Test(expected = LoginException.class)
    public void test_logInWrongLogin() throws LoginException {
        ELanceComApiLoginHelper helper = new ELanceComApiLoginHelper();
        PooledHttpGateway gw = new PooledHttpGateway();
        gw.init();

        helper.setHttpGateway(gw);
        helper.setPropertiesHolder(propertiesHolder);

        helper.setLogin("123");
        helper.setPassword("123");
        helper.logIn();
    }

}
