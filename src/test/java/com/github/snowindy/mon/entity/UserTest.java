package com.github.snowindy.mon.entity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UserTest {

    @Test
    public void test_getMigratedQueryContent() {
        assertEquals(
                "{\"items\":[{\"items\":[\"drupal\",\"сайт\"],\"type\":\"containsAnyPhrase\"},{\"items\":[{\"items\":[\"1с\"],\"type\":\"containsAnyPhrase\"}],\"type\":\"not\"},{\"type\":\"or\",\"items\":[{\"type\":\"lineStartsWithSubstringFirstNumberGTE\",\"items\":[\"Min rate:\",\"10\"]},{\"type\":\"not\",\"items\":[{\"type\":\"lineStartsWithSubstring\",\"items\":[\"Min rate:\"]}]}]}],\"type\":\"and\"}",
                User.getMigratedQueryContent("{\"minRatePerHour\":10,\"minProjectBudget\":null,\"groups\":[{\"any\":[\"drupal\",\"сайт\"],\"stop\":[\"1с\"],\"all\":[]}]}"));

        assertEquals(
                "{\"items\":[{\"items\":[\"drupal\",\"сайт\"],\"type\":\"containsAllPhrase\"},{\"items\":[{\"items\":[\"1с\"],\"type\":\"containsAnyPhrase\"}],\"type\":\"not\"},{\"type\":\"or\",\"items\":[{\"type\":\"lineStartsWithSubstringFirstNumberGTE\",\"items\":[\"Min rate:\",\"10\"]},{\"type\":\"not\",\"items\":[{\"type\":\"lineStartsWithSubstring\",\"items\":[\"Min rate:\"]}]}]},{\"type\":\"or\",\"items\":[{\"type\":\"lineStartsWithSubstringFirstNumberGTE\",\"items\":[\"Min budget:\",\"11.2\"]},{\"type\":\"not\",\"items\":[{\"type\":\"lineStartsWithSubstring\",\"items\":[\"Min budget:\"]}]}]}],\"type\":\"and\"}",
                User.getMigratedQueryContent("{\"minRatePerHour\":10,\"minProjectBudget\":11.2,\"groups\":[{\"all\":[\"drupal\",\"сайт\"],\"stop\":[\"1с\"],\"any\":[]}]}"));
        
        assertEquals(
                "{\"items\":[{\"items\":[{\"items\":[{\"items\":[\"drupal\",\"сайт\"],\"type\":\"containsAllPhrase\"},{\"items\":[{\"items\":[\"1с\"],\"type\":\"containsAnyPhrase\"}],\"type\":\"not\"}],\"type\":\"and\"},{\"items\":[{\"items\":[\"php\",\"mysql\"],\"type\":\"containsAnyPhrase\"},{\"items\":[{\"items\":[\"wordpress\"],\"type\":\"containsAnyPhrase\"}],\"type\":\"not\"}],\"type\":\"and\"}],\"type\":\"or\"}],\"type\":\"and\"}",
                User.getMigratedQueryContent("{\"minRatePerHour\":null,\"minProjectBudget\":null,\"groups\":[{\"all\":[\"drupal\",\"сайт\"],\"stop\":[\"1с\"],\"any\":[]}, {\"any\":[\"php\",\"mysql\"],\"stop\":[\"wordpress\"],\"all\":[]}]}"));

        // Test sanitize
        assertEquals(
                "{\"items\":[{\"items\":[\"drupal\",\"сайт\"],\"type\":\"containsAllPhrase\"}],\"type\":\"and\"}",
                User.getMigratedQueryContent("{\"minRatePerHour\":null,\"minProjectBudget\":null,\"groups\":[{\"all\":[\"\\\"drupal\\\"\",\"'сайт'\"],\"stop\":[],\"any\":[]}]}"));
    }
}
