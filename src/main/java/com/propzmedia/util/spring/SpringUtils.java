package com.propzmedia.util.spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;

public class SpringUtils {
    public static void checkRequiredBeans(ApplicationContext applicationContext, String[] requiredBeans) {
        if (applicationContext != null) {
            List<String> missingBeans = new ArrayList<String>();
            for (String beanName : requiredBeans) {
                try {
                    if (applicationContext.getBean(beanName) == null) {
                        missingBeans.add(beanName);
                    }
                } catch (NoSuchBeanDefinitionException e) {
                    missingBeans.add(beanName);
                }
            }
            if (!missingBeans.isEmpty()) {
                throw new NoSuchBeanDefinitionException(missingBeans.toString(), "Unable to locate required beans: "
                        + missingBeans);
            }
        }
    }
}
