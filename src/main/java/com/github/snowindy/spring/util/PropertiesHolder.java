package com.github.snowindy.spring.util;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Properties;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

public class PropertiesHolder extends PropertyPlaceholderConfigurer implements IPropertiesHolder {
    protected PropertiesConfiguration config;

    private final static Logger logger = LoggerFactory.getLogger(PropertiesHolder.class);

    @Override
    public PropertiesConfiguration getConfig() {
        return config;
    }

    @Override
    protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props)
            throws BeansException {
        logger.debug("Loading application properties...");
        super.processProperties(beanFactory, props);

        StringWriter writer = null;
        StringReader reader = null;
        try {
            writer = new StringWriter();

            props.store(writer, null);

            reader = new StringReader(writer.toString());

            config = new PropertiesConfiguration();
            config.load(reader);
        } catch (Exception e) {
            logger.error("Unable to load properties.");
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(writer);
            IOUtils.closeQuietly(reader);
        }
    }

    public PropertiesHolder(PropertiesConfiguration propertiesConfiguration) {
        this();
        this.config = propertiesConfiguration;
    }

    public PropertiesHolder() {
        super();
    }

}