package com.github.snowindy.spring.util;

import org.apache.commons.configuration.PropertiesConfiguration;

public interface IPropertiesHolder {
    PropertiesConfiguration getConfig();
}
