package com.github.snowindy.spring.util;

import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

public class EmailUtils {

    public static MimeMessagePreparator createPreparator(final String email, final String title, final String from,
                                                         final String replyTo, final String htmlBody,
                                                         final String unsubscribeURL) {
        return new MimeMessagePreparator() {

            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");

                message.setSubject(title);
                message.setFrom(from);
                message.setReplyTo(replyTo);
                message.setTo(email);

                message.setText(htmlBody, true);

                if (StringUtils.isNotBlank(unsubscribeURL)) {
                    mimeMessage.setHeader("List-Unsubscribe", unsubscribeURL);
                }
            }
        };
    }

    public static MimeMessagePreparator createPreparator(final String email, final String title, final String from,
                                                         final String replyTo, final String htmlBody) {
        return createPreparator(email, title, from, replyTo, htmlBody, null);
    }
}
