package com.github.snowindy.util.spring.liquibase;

import java.sql.Connection;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.SpringLiquibase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExtendedSpringLiquibase extends SpringLiquibase {

    @Override
    protected Database createDatabase(Connection c) throws DatabaseException {
        Database db = super.createDatabase(c);

        db.setDatabaseChangeLogLockTableName(tablePrefix + db.getDatabaseChangeLogLockTableName());
        db.setDatabaseChangeLogTableName(tablePrefix + db.getDatabaseChangeLogTableName());

        return db;
    }

    protected boolean performUpdate = true;
    protected String tablePrefix = "";

    private final static Logger logger = LoggerFactory.getLogger(ExtendedSpringLiquibase.class);

    @Override
    protected void performUpdate(Liquibase liquibase) throws LiquibaseException {
        logger.debug("Performing liquibase update.");
        if (performUpdate) {
            super.performUpdate(liquibase);
        } else {
            logger.info("Liquibase update has been omitted because of property value.");
        }

    }

    public void setPerformUpdate(boolean performUpdate) {
        this.performUpdate = performUpdate;
    }

    public void setTablePrefix(String tablePrefix) {
        this.tablePrefix = tablePrefix;
    }

}
