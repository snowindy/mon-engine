package com.github.snowindy.util.text;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.ObjectMapper;

public class JsonUtils {

    public static String removeFields(String json, String... omitFields) {

        for (String field : omitFields) {
            String fieldPattern = "\"" + field + "\"\\s*:\\s*((\"[\\w\\s]*\")|(null)|(\\d+)|(\\{([^\\}]+)\\}))\\s*,?";
            Matcher m = Pattern.compile(fieldPattern).matcher(json);
            while (m.find()) {
                json = m.replaceAll("");
            }

        }
        return removeLastComma(json);
    }

    public static String removeFieldsWithStringValues(String json, String... omitValues) {

        for (String val : omitValues) {
            val = prepareValForRegex(val);
            String fieldPattern = "\"[\\w-]+\"\\s*:\\s*\"" + val + "\"\\s*,?";
            Matcher m = Pattern.compile(fieldPattern).matcher(json);
            while (m.find()) {
                json = m.replaceAll("");
            }

        }
        return removeLastComma(json);
    }

    public static String removeFieldsWithRawRepresentationValues(String json, String... omitValues) {

        for (String val : omitValues) {
            val = prepareValForRegex(val);
            String fieldPattern = "\"[\\w-]+\"\\s*:\\s*" + val + "\\s*,?";
            Matcher m = Pattern.compile(fieldPattern).matcher(json);
            while (m.find()) {
                json = m.replaceAll("");
            }

        }
        return removeLastComma(json);
    }

    private static String prepareValForRegex(String val) {
        val = val.replace("{", "\\{");
        val = val.replace("}", "\\}");
        val = val.replace(".", "\\.");
        return val;
    }

    public static String removeNullFields(String json, String... omitFields) {

        for (String field : omitFields) {
            String fieldPattern = "\"" + field + "\"\\s*:(null)\\s*,?";
            Matcher m = Pattern.compile(fieldPattern).matcher(json);
            while (m.find()) {
                json = m.replaceAll("");
            }

        }
        return removeLastComma(json);

    }

    private static String removeLastComma(String json) {
        String commaBracesPattern = "\\,\\s*\\}";
        Matcher commaBracesMatcher = Pattern.compile(commaBracesPattern).matcher(json);
        while (commaBracesMatcher.find()) {
            json = commaBracesMatcher.replaceAll("}");
        }
        return json;
    }

    public static String toJson(Object target) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        ObjectMapper mapper = new ObjectMapper();

        /*
         * ISO8601 reference: http://www.w3.org/TR/NOTE-datetime
         */
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(timeZone);
        mapper.setDateFormat(dateFormat);
        mapper.setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);

        String res = null;
        try {
            mapper.writeValue(baos, target);

            res = baos.toString("UTF-8");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return res;

    }

    public static String toJsonWithObjectNotation(Object target) {
        String objectToString = target.getClass().getName() + '@' + Integer.toHexString(target.hashCode());
        String json = toJson(target);
        json = StringUtils.replaceOnce(json, "{", "{\"__this\":\"" + objectToString + "\",");
        // We don't check ",}" case cause Jackson does not allow serializing
        // empty objects.
        return json;

    }

    public static <T> T fromJson(String json, Class<T> returnType) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(json, returnType);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    

    @SuppressWarnings("unchecked")
    public static Map<String, String> fromJsonToMapOfString(String json) {
        return fromJson(json, Map.class);
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> fromJsonToMapOfObject(String json) {
        return fromJson(json, Map.class);
    }

    @SuppressWarnings("unchecked")
    public static List<String> fromJsonToListOfString(String json) {
        return fromJson(json, List.class);
    }

    public static <T> List<T> fromJsonToListOfObject(String json, Class<?> clazz) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(json, mapper.getTypeFactory().constructCollectionType(List.class, clazz));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
