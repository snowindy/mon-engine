package com.github.snowindy.util.text;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.github.snowindy.util.text.StringUtil;

public class WordMatchUtils {

    public static boolean containsAllKeywords(Collection<String> keywords, String text) {
        String[] wordsSplit = getWordsSplit(text);
        Set<String> wordsUnique = getWords(wordsSplit);

        if (keywords.isEmpty()) {
            return false;
        }
        String wordsWithSpacesBetween = null;

        Boolean foundAll = true;
        for (String keyword : keywords) {
            keyword = StringUtils.lowerCase(keyword);
            if (keyword.contains(" ")) {
                if (wordsWithSpacesBetween == null) {
                    // Lazy init for perfomance
                    wordsWithSpacesBetween = StringUtils.join(wordsSplit, " ");
                }
                if (!wordsWithSpacesBetween.contains(keyword)) {
                    foundAll = false;
                    break;
                }
            } else {
                if (!wordsUnique.contains(keyword)) {
                    foundAll = false;
                    break;
                }
            }
        }

        return foundAll;
    }

    private static Set<String> getWords(String[] words) {
        Set<String> wordsSet = new HashSet<String>();
        for (String word : words) {
            wordsSet.add(word);
        }

        return wordsSet;
    }

    private static String[] getWordsSplit(String text) {
        String textCleaned = StringUtil.clenup(text);
        textCleaned = StringUtils.lowerCase(textCleaned);

        String[] words = StringUtils.split(textCleaned, SPLIT_CHARS);
        return words;
    }

    // TODO more chars
    private static String SPLIT_CHARS = ", ()[]{}\"':.\n\t";

    public static boolean containsAnyKeywords(Collection<String> keywords, String text) {
        String[] wordsSplit = getWordsSplit(text);
        Set<String> wordsUnique = getWords(wordsSplit);
        String wordsWithSpacesBetween = null;

        boolean found = false;
        for (String keyword : keywords) {
            keyword = StringUtils.lowerCase(keyword);
            if (keyword.contains(" ")) {
                if (wordsWithSpacesBetween == null) {
                    // Lazy init for perfomance
                    wordsWithSpacesBetween = StringUtils.join(wordsSplit, " ");
                }
                if (wordsWithSpacesBetween.contains(keyword)) {
                    found = true;
                    break;
                }
            } else {
                if (wordsUnique.contains(keyword)) {
                    found = true;
                    break;
                }
            }
        }

        return found;
    }
}
