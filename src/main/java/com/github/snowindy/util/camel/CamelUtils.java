package com.github.snowindy.util.camel;

public class CamelUtils {
    /**
     * Hack dealing with beanRef strange behavior
     * http://stackoverflow.com/questions
     * /16797827/apache-camel-how-to-get-a-correct-mock-endpoint-for-beanref
     */
    public static String toBean(String beanName, String methodName) {
        return "bean:" + beanName + (methodName != null ? "?method=" + methodName : "");
    }

    public static String toBean(String beanName) {
        return toBean(beanName, null);
    }
}
