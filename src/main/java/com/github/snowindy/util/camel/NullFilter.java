package com.github.snowindy.util.camel;

public class NullFilter {
    public boolean isNotNull(Object o) {
        return o != null;
    }
}
