package com.github.snowindy.util.http;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;

public class HttpResponseWrapper {
    String responseContent;
    StatusLine statusLine;
    HttpResponse response;

    public String getResponseContent() {
        return responseContent;
    }

    public StatusLine getStatusLine() {
        return statusLine;
    }

    public HttpResponse getResponse() {
        return response;
    }

}