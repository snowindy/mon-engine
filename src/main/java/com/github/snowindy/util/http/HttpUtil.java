package com.github.snowindy.util.http;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.entity.ContentType;

public class HttpUtil {

    public static String getResponseContent(HttpResponse response) {
        return getResponseContent(response, null);
    }

    public static String getResponseContent(HttpResponse response, String encoding) {
        if (encoding == null) {
            encoding = "UTF-8";
        }
        HttpEntity entity = response.getEntity();
        String entityContent = null;
        if (entity != null) {
            InputStream inputStream = null;
            try {
                ContentType contentType = ContentType.getOrDefault(entity);
                Charset charSet = contentType.getCharset();
                String charserName;
                if (charSet == null) {
                    charserName = encoding;
                } else {
                    charserName = charSet.name();
                }

                inputStream = entity.getContent();
                entityContent = IOUtils.toString(inputStream, charserName);
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e) {
                throw new RuntimeException(e);
            } finally {
                IOUtils.closeQuietly(inputStream);
            }
        } else {
            throw new RuntimeException("No http entity returned.");
        }
        return entityContent;
    }

    /**
     * Utility that will not allow HttpClient hung up when only status line gets
     * read, and response body does not get consumed.
     * 
     * @throws IOException
     * @throws IllegalStateException
     */
    public static HttpResponseWrapper consumeResponseFully(HttpResponse response, String encoding) {
        HttpResponseWrapper wrapper = new HttpResponseWrapper();
        wrapper.statusLine = response.getStatusLine();
        wrapper.responseContent = getResponseContent(response, encoding);
        wrapper.response = response;

        return wrapper;
    }

    public static HttpResponseWrapper consumeResponseFully(HttpResponse response) {
        return consumeResponseFully(response, null);
    }

}
