package com.github.snowindy.util;

import java.lang.reflect.Field;

import org.springframework.util.ReflectionUtils;

public class BeanUtil {
    public static void copyProperties(Object source, Object target, String... propNames) {
        copyPropertiesInner(true, source, target, propNames);
    }

    public static void copyPropertiesIfNotNull(Object source, Object target, String... propNames) {
        copyPropertiesInner(false, source, target, propNames);
    }

    private static void copyPropertiesInner(boolean copyIfNull, Object source, Object target, String... propNames) {
        for (String prop : propNames) {
            try {
                Field fSource = ReflectionUtils.findField(source.getClass(), prop);
                Field fTarget = ReflectionUtils.findField(target.getClass(), prop);
                fSource.setAccessible(true);
                fTarget.setAccessible(true);
                Object val = ReflectionUtils.getField(fSource, source);
                if (val != null) {
                    ReflectionUtils.setField(fTarget, target, val);
                } else if (copyIfNull) {
                    ReflectionUtils.setField(fTarget, target, val);
                }
            } catch (Exception e) {
                throw new RuntimeException("Unable to copy property '" + prop + "'", e);
            }
        }
    }
}
