package com.github.snowindy.util;

import java.util.List;
import java.util.Random;

public class RandomUtils {
    public static <T> List<T> randomSublist(List<T> items, int m) {
        Random rnd = new Random();
        for (int i = 0; i < m; i++) {
            int pos = i + rnd.nextInt(items.size() - i);
            T tmp = items.get(pos);
            items.set(pos, items.get(i));
            items.set(i, tmp);
        }
        return items.subList(0, m);
    }
    
    public static <T> T randomItem(List<T> items) {
        List<T> lst = randomSublist(items, 1);
        
        return lst.get(0);
    }
}
