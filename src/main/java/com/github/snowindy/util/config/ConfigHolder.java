package com.github.snowindy.util.config;

import java.io.InputStream;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.github.snowindy.util.servlet.config.ConfigHolderInitListener;

public class ConfigHolder {
    private static PropertiesConfiguration config;

    public static PropertiesConfiguration getConfig() {
        if (config == null) {
            throw new IllegalStateException(String.format(
                    "Config holder has not been properly  initialized. Add %s to web.xml.",
                    ConfigHolderInitListener.class.getCanonicalName()));
        }
        return config;
    }

    public static void init(InputStream propsStream) throws ConfigurationException {
        config = new PropertiesConfiguration();
        config.setEncoding("UTF-8");
        config.load(propsStream);
    }

}
