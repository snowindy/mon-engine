package com.github.snowindy.util.collections;

import com.google.common.base.Function;

public class ListUtils {
    public static Function<Object, String> objectToString = new Function<Object, String>() {
        @Override
        public String apply(Object arg0) {
            return String.valueOf(arg0);
        }
    };
}
