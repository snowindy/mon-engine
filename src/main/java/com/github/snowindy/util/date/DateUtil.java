package com.github.snowindy.util.date;

import java.util.Date;

public class DateUtil {
    /**
     * Fix for http://stackoverflow.com/questions/15629222/java-sql-timestamp-comparison-bug
     */
    public static boolean after(Date target, Date compareTo) {
        return target.getTime() > compareTo.getTime();
    }

    /**
     * Fix for http://stackoverflow.com/questions/15629222/java-sql-timestamp-comparison-bug
     */
    public static boolean before(Date target, Date compareTo) {
        return target.getTime() < compareTo.getTime();
    }
}
