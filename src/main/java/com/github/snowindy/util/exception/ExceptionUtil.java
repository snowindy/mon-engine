package com.github.snowindy.util.exception;

public class ExceptionUtil {

    public static String getStackTraceAsString(Throwable thr) {
        StringBuffer buffer = new StringBuffer(256);
        StackTraceElement[] stackElements = thr.getStackTrace();
        for (int i = 0; i < stackElements.length; i++) {
            if (buffer.length() > 0) {
                buffer.append('\n');
            }
            buffer.append(stackElements[i].toString());
        }
        return buffer.toString();
    }
}
