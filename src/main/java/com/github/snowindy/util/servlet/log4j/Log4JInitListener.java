package com.github.snowindy.util.servlet.log4j;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.util.ClasspathUtils;

public class Log4JInitListener implements ServletContextListener {

    private final static Logger logger = LoggerFactory.getLogger(Log4JInitListener.class);

    private static String CONFIG_ATTR_NAME = "log4jConfigLocation";

    public void contextInitialized(ServletContextEvent evt) {
        try {
            Object con = evt.getServletContext().getInitParameter(CONFIG_ATTR_NAME);
            String configPath = con != null ? con.toString() : "classpath:log4j.properties";
            if (con == null) {
                logger.warn("Log4J config location context param '{}' not found, using default location '{}'.",
                        CONFIG_ATTR_NAME, configPath);
            }

            InputStream propsStream = null;

            try {
                if (StringUtils.startsWith(configPath, "classpath:")) {
                    String path = StringUtils.replaceOnce(configPath, "classpath:", "");
                    propsStream = ClasspathUtils.getResourceAsStream(path);
                } else {
                    URL url = new URL(configPath);
                    URLConnection connection = url.openConnection();
                    propsStream = connection.getInputStream();

                }
                if (propsStream == null) {
                    throw new IOException(String.format("Unable to open properties stream '%s'.", configPath));
                }

                BasicConfigurator.resetConfiguration();
                PropertyConfigurator.configure(propsStream);
                logger.info("Loaded log configuration from '{}'.", configPath);

            } finally {
                IOUtils.closeQuietly(propsStream);
            }
        } catch (Exception e) {
            logger.error("Unable to configure logger.", e);
            throw new RuntimeException(e);
        }

    }

    public void contextDestroyed(ServletContextEvent evt) {

    }
}