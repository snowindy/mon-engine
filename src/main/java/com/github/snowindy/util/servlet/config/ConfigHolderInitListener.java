package com.github.snowindy.util.servlet.config;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.util.ClasspathUtils;
import com.github.snowindy.util.config.ConfigHolder;

public class ConfigHolderInitListener implements ServletContextListener {

    private final static Logger logger = LoggerFactory.getLogger(ConfigHolderInitListener.class);

    private static String CONFIG_ATTR_NAME = "configLocation";

    public void contextInitialized(ServletContextEvent evt) {
        try {
            Object con = evt.getServletContext().getInitParameter(CONFIG_ATTR_NAME);

            if (con == null) {
                logger.error("Config location context param '{}' not found.", CONFIG_ATTR_NAME);
                throw new RuntimeException(String.format("Config location context param '%s' not found.",
                        CONFIG_ATTR_NAME));
            }

            String configPath = con.toString();

            InputStream propsStream = null;

            try {
                if (StringUtils.startsWith(configPath, "classpath:")) {
                    String path = StringUtils.replaceOnce(configPath, "classpath:", "");
                    propsStream = ClasspathUtils.getResourceAsStream(path);
                } else {
                    URL url = new URL(configPath);
                    URLConnection connection = url.openConnection();
                    propsStream = connection.getInputStream();

                }

                if (propsStream == null) {
                    throw new IOException(String.format("Unable to open properties stream '%s'.", configPath));
                }

                ConfigHolder.init(propsStream);

                logger.info("Loaded configuration from '{}'.", configPath);

            } finally {
                IOUtils.closeQuietly(propsStream);
            }
        } catch (Exception e) {
            logger.error("Unable to load configuration.", e);
            throw new RuntimeException(e);
        }

    }

    public void contextDestroyed(ServletContextEvent evt) {

    }
}