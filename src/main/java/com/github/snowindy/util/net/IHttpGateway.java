package com.github.snowindy.util.net;

import java.io.File;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.DefaultHttpClient;

public interface IHttpGateway {
    void downloadFile(String sourceUrl, File destFile);

    String readHtmlPageAsString(String sourceUrl, CookieStore cookieStore);

    String readHtmlPageAsString(String sourceUrl, Pair<String, String> header);

    String readHtmlPageAsString(String sourceUrl);

    String readHtmlPageAsString(String sourceUrl, String encoding);

    DefaultHttpClient getSharedHttpClient();
}
