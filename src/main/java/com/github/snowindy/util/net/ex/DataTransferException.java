package com.github.snowindy.util.net.ex;

public class DataTransferException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public DataTransferException(Throwable e) {
        super(e);
    }

    public DataTransferException(String string) {
        super(string);
    }
}
