package com.github.snowindy.util.net;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.DecompressingHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.util.http.HttpResponseWrapper;
import com.github.snowindy.util.http.HttpUtil;
import com.github.snowindy.util.net.ex.DataTransferException;
import com.github.snowindy.util.net.ex.IncorrectStatusCodeException;

public class PooledHttpGateway implements IHttpGateway {

    @Override
    public void downloadFile(String sourceUrl, File destFile) {
        try {
            HttpGet httpget = new HttpGet(sourceUrl);
            HttpResponse response = httpClient.execute(httpget);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = null;
                OutputStream outputStream = null;
                try {
                    inputStream = entity.getContent();
                    outputStream = new FileOutputStream(destFile);
                    IOUtils.copy(inputStream, outputStream);
                } finally {
                    IOUtils.closeQuietly(inputStream);
                    IOUtils.closeQuietly(outputStream);
                }
            }
        } catch (Exception e) {
            throw new DataTransferException(e);
        }
    }

    private int maxTotalConnections = 200;
    private int maxPerRoute = 30;
    private int connectionTimeoutMillis = 30000;
    private int socketTimeoutMillis = 30000;
    private boolean trustAllSSLCerts = false;

    public boolean isTrustAllSSLCerts() {
        return trustAllSSLCerts;
    }

    public void setTrustAllSSLCerts(boolean trustAllSSLCerts) {
        this.trustAllSSLCerts = trustAllSSLCerts;
    }

    public int getMaxTotalConnections() {
        return maxTotalConnections;
    }

    public void setMaxTotalConnections(int maxTotalConnections) {
        this.maxTotalConnections = maxTotalConnections;
    }

    public int getMaxPerRoute() {
        return maxPerRoute;
    }

    public void setMaxPerRoute(int maxPerRoute) {
        this.maxPerRoute = maxPerRoute;
    }

    public int getConnectionTimeoutMillis() {
        return connectionTimeoutMillis;
    }

    public void setConnectionTimeoutMillis(int connectionTimeoutMillis) {
        this.connectionTimeoutMillis = connectionTimeoutMillis;
    }

    public int getSocketTimeoutMillis() {
        return socketTimeoutMillis;
    }

    public void setSocketTimeoutMillis(int socketTimeoutMillis) {
        this.socketTimeoutMillis = socketTimeoutMillis;
    }

    @PostConstruct
    public synchronized void init() {
        PoolingClientConnectionManager cm = new PoolingClientConnectionManager();
        cm.setDefaultMaxPerRoute(maxPerRoute);
        cm.setMaxTotal(maxTotalConnections);

        HttpParams httpParams = new BasicHttpParams();

        HttpConnectionParams.setConnectionTimeout(httpParams, connectionTimeoutMillis);
        HttpConnectionParams.setStaleCheckingEnabled(httpParams, true);
        HttpConnectionParams.setSoTimeout(httpParams, socketTimeoutMillis);

        httpClient = new DefaultHttpClient(cm);
        decompressingHttpClient = new DecompressingHttpClient(httpClient);

        if (trustAllSSLCerts) {
            logger.warn("Http client is set to trust all SSL certificates. This is a security hole that should be fixed ASAP.");
            httpClient = TrustAllSSLCertificatesHttpClientWrapper.wrapClient(httpClient);
        }
    }

    private volatile DefaultHttpClient httpClient;
    private volatile DecompressingHttpClient decompressingHttpClient;

    private final static Logger logger = LoggerFactory.getLogger(PooledHttpGateway.class);

    @Override
    public String readHtmlPageAsString(String sourceUrl) throws IncorrectStatusCodeException {
        return readHtmlPageAsString(sourceUrl, null, null, null);
    }

    @Override
    public String readHtmlPageAsString(String sourceUrl, String encoding) throws IncorrectStatusCodeException {
        return readHtmlPageAsString(sourceUrl, encoding, null, null);
    }

    @Override
    public String readHtmlPageAsString(String sourceUrl, CookieStore cookieStore) throws IncorrectStatusCodeException {
        return readHtmlPageAsString(sourceUrl, null, cookieStore, null);
    }

    private String readHtmlPageAsString(String sourceUrl, String encoding, CookieStore cookieStore,
                                        Pair<String, String> header) throws IncorrectStatusCodeException {
        try {
            boolean cookiesExist = cookieStore != null;
            logger.debug("Requesting url '{}'" + (cookiesExist ? " with cookies" : " no cookies"), sourceUrl);

            HttpContext localContext = null;
            if (cookieStore != null) {
                localContext = new BasicHttpContext();
                localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
            }

            HttpGet httpget = new HttpGet(sourceUrl);
            if (header != null) {
                httpget.setHeader(header.getKey(), header.getValue());
            }

            HttpResponseWrapper wrapper = HttpUtil.consumeResponseFully(
                    decompressingHttpClient.execute(httpget, localContext), encoding);

            if (wrapper.getStatusLine().getStatusCode() == 302 || wrapper.getStatusLine().getStatusCode() == 301) {
                Header[] lh = wrapper.getResponse().getHeaders("Location");
                if (lh != null && lh.length > 0) {
                    String url = lh[0].getValue();
                    logger.debug("Detected redirect with code {} and url '{}'.", wrapper.getStatusLine()
                            .getStatusCode(), url);
                    return readHtmlPageAsString(url, encoding, cookieStore, header);
                } else {
                    logger.debug("Detected redirect with code {} but Location header is not specified.", wrapper
                            .getStatusLine().getStatusCode());
                }
            }

            if (wrapper.getStatusLine().getStatusCode() != 200) {
                String msg = String.format("Server returned incorrect http status code: %s. Page text: %s.", wrapper
                        .getStatusLine().getStatusCode(), wrapper.getResponseContent());
                logger.info(msg);
                throw new IncorrectStatusCodeException(wrapper.getStatusLine().getStatusCode(),
                        wrapper.getResponseContent(), sourceUrl);
            }

            return wrapper.getResponseContent();
        } catch (IOException e) {
            throw new DataTransferException(e);
        }
    }

    @Override
    public DefaultHttpClient getSharedHttpClient() {
        return httpClient;
    }

    @Override
    public String readHtmlPageAsString(String sourceUrl, Pair<String, String> header)
            throws IncorrectStatusCodeException {
        return readHtmlPageAsString(sourceUrl, null, null, header);
    }
}
