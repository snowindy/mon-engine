package com.github.snowindy.util.net.ex;

public class IncorrectStatusCodeException extends DataTransferException {

    private static final long serialVersionUID = 1L;

    String responseText;
    int statusCode;
    String sourceDescriptor;

    public IncorrectStatusCodeException(int statusCode, String responseText, String sourceDescriptor) {
        super(String.format("Incorrect status code has been detected: %s. Source descriptor: %s. Response text: %s.",
                statusCode, sourceDescriptor, responseText));
        this.responseText = responseText;
        this.statusCode = statusCode;
        this.sourceDescriptor = sourceDescriptor;
    }

    public String getResponseText() {
        return responseText;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getSourceDescriptor() {
        return sourceDescriptor;
    }
}
