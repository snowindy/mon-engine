package com.github.snowindy.util.template;

import groovy.text.GStringTemplateEngine;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.github.snowindy.util.ClasspathUtils;

public class GroovyTemplateUtils {
    public static String processTemplate(String template, Map<String, ? extends Object> params) {
        try {
            GStringTemplateEngine engine1 = new GStringTemplateEngine();
            return String.valueOf(engine1.createTemplate(template).make(params));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String processTemplateFromClasspath(String tmplPath, Map<String, ? extends Object> params) {
        String template = ClasspathUtils.readResourceUTF8(tmplPath);
        if (StringUtils.isBlank(template)) {
            throw new IllegalArgumentException("Unable to find template at path: ${tmplPath}.");
        }

        return processTemplate(template, params);
    }
}
