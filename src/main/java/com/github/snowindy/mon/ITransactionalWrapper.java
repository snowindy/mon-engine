package com.github.snowindy.mon;

import java.util.concurrent.Callable;

public interface ITransactionalWrapper {
    <T> T withTransaction(Callable<T> callable);
    void withTransaction(Runnable runnable);
}
