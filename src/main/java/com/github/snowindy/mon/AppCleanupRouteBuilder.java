package com.github.snowindy.mon;

import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.lang3.text.WordUtils;

import com.github.snowindy.mon.beans.PeriodicCleaner;
import com.github.snowindy.util.camel.CamelUtils;

public class AppCleanupRouteBuilder extends RouteBuilder {

    public static final String BEAN_PERIODIC_CLEANER = WordUtils.uncapitalize(PeriodicCleaner.class.getSimpleName());

    private static final String ROUTE_ID_PREFIX = "AppCleanup: ";

    @Override
    public void configure() {
        from(SchedulerRouteBuilder.TOPIC_CLEANUP_SHORT + "?multipleConsumers=true").to(
                CamelUtils.toBean(BEAN_PERIODIC_CLEANER, "cleanupShortPeriod")).routeId(
                ROUTE_ID_PREFIX + "cleanupShortPeriod");
        from(SchedulerRouteBuilder.TOPIC_CLEANUP_MEDIUM + "?multipleConsumers=true").to(
                CamelUtils.toBean(BEAN_PERIODIC_CLEANER, "cleanupMediumPeriod")).routeId(
                ROUTE_ID_PREFIX + "cleanupMediumPeriod");
        from(SchedulerRouteBuilder.TOPIC_CLEANUP_LONG + "?multipleConsumers=true").to(
                CamelUtils.toBean(BEAN_PERIODIC_CLEANER, "cleanupLongPeriod")).routeId(
                ROUTE_ID_PREFIX + "cleanupLongPeriod");
    }

}
