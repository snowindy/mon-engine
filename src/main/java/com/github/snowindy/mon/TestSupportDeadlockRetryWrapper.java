package com.github.snowindy.mon;

import java.util.concurrent.Callable;

/**
 * Passthrough implementation for tests.
 * 
 */
public class TestSupportDeadlockRetryWrapper implements IDeadlockRetryWrapper {

    @Override
    public <T> T withTransaction(Callable<T> callable) {
        return withTransaction(callable, 5);
    }

    @Override
    public void withTransaction(final Runnable runnable) {
        withTransaction(runnable, 5);
    }

    @Override
    public void withTransaction(final Runnable runnable, int maxTries) {
        withTransaction(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                runnable.run();
                return null;
            }
        }, maxTries);
    }

    @Override
    public <T> T withTransaction(Callable<T> callable, int maxTries) {
        try {
            return callable.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}