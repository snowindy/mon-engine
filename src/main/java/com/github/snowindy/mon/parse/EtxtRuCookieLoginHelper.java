package com.github.snowindy.mon.parse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.github.snowindy.mon.i.ILoginCookieHelper;
import com.github.snowindy.util.http.HttpResponseWrapper;
import com.github.snowindy.util.http.HttpUtil;

public class EtxtRuCookieLoginHelper extends AbstractLoginHelper implements ILoginCookieHelper {

    private static final String SESSION_COOKIE_NAME = "EtxtCookieData";

    private volatile CookieStore cookieStore;

    @Override
    public boolean checkLoggedIn(String source) {
        Document doc = Jsoup.parse(source);

        return doc.select("#loginUser").size() > 0 || doc.select(".exit").size() > 0;
    }

    @Override
    public boolean isLoggedIn() {
        return cookieStore != null;
    }

    @Override
    public void logIn() throws LoginException {
        DefaultHttpClient httpClient = httpGateway.getSharedHttpClient();
        try {
            HttpPost httpost = new HttpPost("https://www.etxt.ru?enter=1");

            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair("login", login));
            nvps.add(new BasicNameValuePair("password", password));

            httpost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            httpost.setHeader(
                    "User-Agent",
                    propertiesHolder
                            .getConfig()
                            .getString("http.userAgent",
                                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36"));

            HttpResponse response = httpClient.execute(httpost);
            HttpResponseWrapper wrapper = HttpUtil.consumeResponseFully(response);

            if (wrapper.getResponse().getStatusLine().getStatusCode() != 302) {
                throw new LoginException("Unexpected status line in response:" + response.getStatusLine() + "\n "
                        + wrapper.getResponseContent());
            }
            
            Header[] locHeader = response.getHeaders("Location");
            if (locHeader == null || locHeader.length == 0) {
                throw new LoginException("Cannot find expected location redirect header");
            }

            if (!"".equals(wrapper.getResponseContent())) {
                throw new LoginException("Login request returned unexpected response: " + wrapper.getResponseContent());
            }

            List<Cookie> loggedInCookies = httpClient.getCookieStore().getCookies();
            if (loggedInCookies.isEmpty()) {
                throw new LoginException("Response cookies are empty");
            } else {
                boolean foundSessionCookie = false;
                for (Cookie cookie : loggedInCookies) {
                    if (SESSION_COOKIE_NAME.equals(cookie.getName())) {
                        foundSessionCookie = true;
                        break;
                    }
                }

                if (!foundSessionCookie) {
                    throw new LoginException("Unable to find session cookie: " + SESSION_COOKIE_NAME + ". Cookies: "
                            + loggedInCookies);
                }
            }

        } catch (LoginException e) {
            throw e;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        cookieStore = httpClient.getCookieStore();
    }

    @Override
    public CookieStore getLoggedInCookieStore() {
        return cookieStore;
    }

}
