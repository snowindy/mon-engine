package com.github.snowindy.mon.parse;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.github.snowindy.mon.i.ILoginHelper;
import com.github.snowindy.spring.util.IPropertiesHolder;
import com.github.snowindy.util.net.IHttpGateway;

public abstract class AbstractLoginHelper implements ILoginHelper {
    @Autowired
    protected IPropertiesHolder propertiesHolder;

    @Autowired
    IHttpGateway httpGateway;
    
    @PostConstruct
    private void init() {
        if (login == null || password == null) {
            throw new IllegalStateException("login or password cannot be null");
        }
    }

    public void setHttpGateway(IHttpGateway httpGateway) {
        this.httpGateway = httpGateway;
    }

    public void setPropertiesHolder(IPropertiesHolder propertiesHolder) {
        this.propertiesHolder = propertiesHolder;
    }


    protected String login;
    protected String password;

    @Override
    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

}
