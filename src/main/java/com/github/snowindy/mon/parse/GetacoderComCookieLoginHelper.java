package com.github.snowindy.mon.parse;


public class GetacoderComCookieLoginHelper extends AbstractCookieLoginHelper {

    @Override
    public boolean checkLoggedIn(String source) {
        // Contains logout button
        return source.contains("http://www.getacoder.com/users/onsignout.php") || source.contains("<body>Loading...");
    }

    @Override
    protected String getPostLoginUrl() {
        return "http://www.getacoder.com/users/onlogin.php";
    }

    @Override
    protected String getLoginParamName() {
        return "username";
    }

    @Override
    protected String getPasswordParamName() {
        return "passwd";
    }

    @Override
    protected boolean expectRedirect() {
        return false;
    }

}
