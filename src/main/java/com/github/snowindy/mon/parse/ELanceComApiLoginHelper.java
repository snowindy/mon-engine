package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.i.ILoginRequestParamHelper;
import com.github.snowindy.util.http.HttpResponseWrapper;
import com.github.snowindy.util.http.HttpUtil;

public class ELanceComApiLoginHelper extends AbstractLoginHelper implements ILoginRequestParamHelper {

    private final static Logger logger = LoggerFactory.getLogger(ELanceComApiLoginHelper.class);

    @Override
    public boolean checkLoggedIn(String source) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode errNode = mapper.readTree(source).get("errors");
            return errNode == null;
        } catch (Exception e) {
            logger.error("Unable to parse JSON. Source: '{}'.", source, e);
            throw new RuntimeException(e);
        }
    }

    private volatile String accessToken;

    @Override
    public boolean isLoggedIn() {
        return accessToken != null;
    }
    
    @Override
    public void logIn() throws LoginException {
        try {
            DefaultHttpClient httpClient = httpGateway.getSharedHttpClient();

            HttpPost httpost = new HttpPost("https://api.elance.com/api2/oauth/token");

            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair("client_id", login));
            nvps.add(new BasicNameValuePair("client_secret", password));
            nvps.add(new BasicNameValuePair("grant_type", "client_credentials"));

            httpost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));

            HttpResponseWrapper wrapper = HttpUtil.consumeResponseFully(httpClient.execute(httpost));
            int statusCode = wrapper.getStatusLine().getStatusCode();
            if (statusCode != 200) {
                throw new LoginException("Unexpected status line in response: " + wrapper.getStatusLine());
            }

            accessToken = getAccessToken(wrapper.getResponseContent());

        } catch (LoginException e) {
            accessToken = null;
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public String getAccessToken(String source) throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        accessToken = mapper.readTree(source).path("data").path("access_token").asText();

        if (StringUtils.isEmpty(accessToken)) {
            throw new LoginException("Unexpected response body returned: " + source);
        }

        accessToken = StringUtils.replace(accessToken, "|", "%7C");

        return accessToken;
    }

    @Override
    public String updateUrlWithLoginParam(String url) {
        if (StringUtils.contains(url, "access_token=")) {
            throw new IllegalArgumentException(String.format("Url already contains access_token param: '%s'", url));
        }
        if (accessToken == null) {
            throw new IllegalStateException("login operation should be called before this operation.");
        }
        url = url + "&access_token=" + accessToken;
        return url;
    }
}
