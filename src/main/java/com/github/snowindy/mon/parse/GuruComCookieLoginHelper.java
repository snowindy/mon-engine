package com.github.snowindy.mon.parse;

public class GuruComCookieLoginHelper extends AbstractCookieLoginHelper {

    @Override
    public boolean checkLoggedIn(String source) {
        // Contains logout button
        return source.contains("http://www.guru.com/LogOut.aspx");
    }

    @Override
    protected String getPostLoginUrl() {
        return "https://www.guru.com/login.aspx";
    }

    @Override
    protected String getLoginParamName() {
        return "ctl00$ContentPlaceHolder1$ucLogin$txtUserName$txtUserName_TextBox";
    }

    @Override
    protected String getPasswordParamName() {
        return "ctl00$ContentPlaceHolder1$ucLogin$txtPassword$txtPassword_TextBox";
    }

    @Override
    protected boolean expectRedirect() {
        return true;
    }

}
