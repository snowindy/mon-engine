package com.github.snowindy.mon.parse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.github.snowindy.mon.i.ILoginCookieHelper;
import com.github.snowindy.util.http.HttpResponseWrapper;
import com.github.snowindy.util.http.HttpUtil;

public abstract class AbstractCookieLoginHelper extends AbstractLoginHelper implements ILoginCookieHelper {

    private volatile CookieStore cookieStore;

    protected abstract String getPostLoginUrl();

    protected abstract String getLoginParamName();

    protected abstract String getPasswordParamName();

    protected abstract boolean expectRedirect();

    @Override
    public boolean isLoggedIn() {
        return cookieStore != null;
    }

    @Override
    public void logIn() throws LoginException {
        DefaultHttpClient httpClient = httpGateway.getSharedHttpClient();
        try {
            HttpPost httpost = new HttpPost(getPostLoginUrl());

            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair(getLoginParamName(), login));
            nvps.add(new BasicNameValuePair(getPasswordParamName(), password));

            httpost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            httpost.setHeader(
                    "User-Agent",
                    propertiesHolder
                            .getConfig()
                            .getString("http.userAgent",
                                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36"));

            HttpResponse response = httpClient.execute(httpost);
            HttpResponseWrapper wrapper = HttpUtil.consumeResponseFully(response);

            if (wrapper.getResponse().getStatusLine().getStatusCode() != (expectRedirect() ? 302 : 200)) {
                throw new LoginException("Unexpected status line in response:" + response.getStatusLine() + "\n "
                        + wrapper.getResponseContent());
            }

            if (expectRedirect()) {
                Header[] locHeader = response.getHeaders("Location");
                if (locHeader == null || locHeader.length == 0) {
                    throw new LoginException("Cannot find expected location redirect header");
                }

                if (!"".equals(wrapper.getResponseContent())) {
                    throw new LoginException("Login request returned unexpected response: "
                            + wrapper.getResponseContent());
                }
            } else {
                if (!checkLoggedIn(wrapper.getResponseContent())) {
                    throw new LoginException("Not logged in after not redirected:" + response.getStatusLine() + "\n "
                            + wrapper.getResponseContent());
                }
            }
        } catch (LoginException e) {
            throw e;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        cookieStore = httpClient.getCookieStore();
    }

    @Override
    public CookieStore getLoggedInCookieStore() {
        return cookieStore;
    }

}
