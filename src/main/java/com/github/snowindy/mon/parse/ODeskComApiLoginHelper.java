package com.github.snowindy.mon.parse;

import java.util.Date;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.i.ILoginHeaderHelper;
import com.google.gdata.client.authn.oauth.OAuthException;
import com.google.gdata.client.authn.oauth.OAuthHelper;
import com.google.gdata.client.authn.oauth.OAuthHmacSha1Signer;
import com.google.gdata.client.authn.oauth.OAuthParameters;
import com.google.gdata.client.authn.oauth.OAuthParameters.OAuthType;

public class ODeskComApiLoginHelper extends AbstractLoginHelper implements ILoginHeaderHelper {

    private final static Logger logger = LoggerFactory.getLogger(ODeskComApiLoginHelper.class);

    @Override
    public boolean checkLoggedIn(String source) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode errNode = mapper.readTree(source).get("error");
            return errNode == null;
        } catch (Exception e) {
            logger.error("Unable to parse JSON. Source: '{}'.", source, e);
            throw new RuntimeException(e);
        }
    }

    private String accessToken;
    private String accessTokenSecret;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessTokenSecret() {
        return accessTokenSecret;
    }

    public void setAccessTokenSecret(String accessTokenSecret) {
        this.accessTokenSecret = accessTokenSecret;
    }

    @Override
    public boolean isLoggedIn() {
        // Suppose we're always logged in, as it's stateless
        return true;
    }

    @Override
    public void logIn() throws LoginException {
        if (login == null || password == null || accessToken == null || accessTokenSecret == null) {
            throw new IllegalStateException("login, password, accessToken, accessTokenSecret cannot be null");
        }
    }

    @Override
    public Pair<String, String> getHeader(String requestUrl) {

        OAuthHelper helper = new OAuthHelper("", "", "", "", new OAuthHmacSha1Signer());

        OAuthParameters oauthParameters = new OAuthParameters();

        oauthParameters.setOAuthConsumerKey(login);
        oauthParameters.setOAuthConsumerSecret(password);
        oauthParameters.setOAuthToken(accessToken);
        oauthParameters.setOAuthTokenSecret(accessTokenSecret);
        oauthParameters.setOAuthTimestamp(String.valueOf(new Date().getTime()));
        oauthParameters.setOAuthType(OAuthType.THREE_LEGGED_OAUTH);
        oauthParameters.setOAuthNonce(String.valueOf(Math.round(Math.random() * 100000000)));
        oauthParameters.setRealm("http://upwork.com");

        try {
            String header = helper.getAuthorizationHeader(requestUrl, "GET", oauthParameters);
            return new ImmutablePair<String, String>("Authorization", header);
        } catch (OAuthException e) {
            throw new RuntimeException(e);
        }
    }
}
