package com.github.snowindy.mon;

import java.util.concurrent.Callable;

public interface IDeadlockRetryWrapper {

    public abstract <T> T withTransaction(Callable<T> callable);

    public abstract void withTransaction(Runnable runnable);

    public abstract void withTransaction(Runnable runnable, int maxTries);

    public abstract <T> T withTransaction(Callable<T> callable, int maxTries) throws Exception;

}