package com.github.snowindy.mon.beans;

import com.github.snowindy.mon.entity.StoredMonItem;
import com.github.snowindy.mon.i.IJobEnricher;

/**
 * Doing-nothing implementation for flows that don't require enrichment.
 *
 */
public class NoEnrichmentMonItemEnricher implements IJobEnricher {

    @Override
    public StoredMonItem enrich(StoredMonItem job) {
        return job;
    }

}
