package com.github.snowindy.mon.beans;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.snowindy.mon.entity.MonItemNotification;
import com.github.snowindy.mon.entity.StoredMonItem;
import com.github.snowindy.mon.entity.User;
import com.github.snowindy.mon.i.IUserMonItemMatcher;
import com.github.snowindy.mon.i.IUserProvider;

@Service
public class UserMonItemMatcher implements IUserMonItemMatcher {

    private final static Logger logger = LoggerFactory.getLogger(UserMonItemMatcher.class);

    @Autowired
    private IUserProvider userProvider;

    public void setUserProvider(IUserProvider userProvider) {
        this.userProvider = userProvider;
    }

    @Autowired
    private WordMatcher wordMatcher;

    @Override
    public List<MonItemNotification> matchJobForUsers(StoredMonItem job) {
        String plainTextForm = job.getPlainTextForm();
        List<MonItemNotification> notifications = new ArrayList<MonItemNotification>();
        for (User user : userProvider.getUsersInterestedInHouse(job.getHouseTopic().getHouse())) {
            logger.debug("Testing MonItem {} (metadata '{}') for user {}.", job, job.getMetadata(), user);

            if (user.getMatcher().matches(plainTextForm)) {
                MonItemNotification notification = new MonItemNotification();
                notification.setJob(job);
                notification.setUser(user);

                notifications.add(notification);

                logger.debug("MonItem {} matched user {}, created notification {}.", job, user, notification);
            }
        }

        return notifications;
    }

}
