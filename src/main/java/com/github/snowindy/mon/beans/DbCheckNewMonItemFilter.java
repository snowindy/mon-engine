package com.github.snowindy.mon.beans;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.snowindy.mon.entity.StoredMonItem;
import com.github.snowindy.mon.entity.repository.MonItemRepository;
import com.github.snowindy.mon.i.INewMonItemFilter;

@Service
@Transactional
public class DbCheckNewMonItemFilter implements INewMonItemFilter {

    private final static Logger logger = LoggerFactory.getLogger(DbCheckNewMonItemFilter.class);

    @Autowired
    private MonItemRepository monItemRepository;

    @Value("#{cacheManager.getCache('checkNewMonItem')}")
    private Cache cache;

    @PostConstruct
    private void check() {
        if (cache == null) {
            throw new IllegalStateException(String.format("Cache should be initialized for bean '%s'.", this.getClass()
                    .getCanonicalName()));
        }
    }

    @Override
    public boolean isNew(StoredMonItem monItem) {
        boolean isNew = monItemRepository.countByUrl(monItem.getUrl()) == 0;
        if (isNew) {
            ValueWrapper val = cache.get(monItem.getTitle());
            if (val != null) {
                logger.debug("Filtered item as check-cache already have item with such title: '{}'.", monItem);
                isNew = false;
            }
        }

        if (isNew) {
            logger.debug("Is-new-job result: {}, {}.", isNew, monItem);
        } else {
            logger.trace("Is-new-job result: {}, {}.", isNew, monItem);
        }
        if (isNew) {
            cache.put(monItem.getTitle(), "1");
            monItemRepository.save(monItem);
        }
        return isNew;
    }
}
