package com.github.snowindy.mon.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.camel.Handler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import com.github.snowindy.mon.entity.MonItemNotification;
import com.github.snowindy.mon.entity.MultiMonItemNotification;
import com.github.snowindy.mon.entity.repository.ISentMonItemStorage;
import com.github.snowindy.mon.i.IMultiMonItemNotifier;
import com.github.snowindy.spring.util.EmailUtils;
import com.github.snowindy.spring.util.PropertiesHolder;
import com.github.snowindy.util.template.GroovyTemplateUtils;
import com.github.snowindy.util.text.StringUtil;
import com.google.common.collect.ImmutableMap;

/**
 * TODO [Minor] Add word search words highlighting.
 * 
 */
@Service
public class MultiMonItemEmailNotifier implements IMultiMonItemNotifier {

    private final static Logger logger = LoggerFactory.getLogger(MultiMonItemEmailNotifier.class);

    public MultiMonItemEmailNotifier() {
        super();
    }

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private PropertiesHolder propertiesHolder;

    @Value("${notify.email.title.prefix:LanceMonitor}")
    private String emailTitlePrefix;

    @Value("${notify.email.prefix.file.location:}")
    private String emailPrefixFileLocation;

    @Autowired
    private ISentMonItemStorage sentMonItemStorage;

    @Handler
    @Override
    public void notify(final MultiMonItemNotification notification) {
        logger.trace("Sending email aggregated notification {}.", notification);
        List<MonItemNotification> sendNotifications = new ArrayList<MonItemNotification>();
        for (MonItemNotification ntf : notification.getNotifications()) {
            if (sentMonItemStorage.isSent(ntf.getJob().getId().toString(), ntf.getUser().getId().toString())) {
                logger.debug("Notification '{}' is already sent, current send operation was omitted.", notification);
            } else {
                sendNotifications.add(ntf);
            }
        }

        if (sendNotifications.size() > 0) {
            long start = System.currentTimeMillis();

            try {
                String subject = emailTitlePrefix
                        + ": Aggregated search results ("
                        + FastDateFormat.getInstance("yyyy-MM-dd HH:mm", TimeZone.getTimeZone("UTC"))
                                .format(new Date()) + " UTC)";
                String from = propertiesHolder.getConfig().getString("notify.email.from");
                String replyTo = propertiesHolder.getConfig().getString("notify.email.replyTo",
                        propertiesHolder.getConfig().getString("notify.email.from"));
                String to = notification.getUser().getEmail();

          
                HashMap<String, Object> params = new HashMap<String, Object>();
                params.put("user", notification.getUser());
                params.put("notification", notification);
                List<Map<String, String>> monItemHtmlList = new ArrayList<Map<String, String>>();
                for (MonItemNotification ntf : sendNotifications) {
                    monItemHtmlList.add(ImmutableMap.of("body", StringUtil.convertTextToHtml(ntf.getJob().getTextBody()),
                        "jobInfo", StringUtil.convertTextToHtml(ntf.getJob().getJobInfo()),
                        "clientInfo", StringUtil.convertTextToHtml(ntf.getJob().getClientInfo()) ));
                }
                params.put("monItemHtmlList", monItemHtmlList);

                params.put("emailTop",
                        GroovyTemplateUtils.processTemplateFromClasspath("/templates/email/email-top.tmpl", params));
                params.put("emailBottom", GroovyTemplateUtils.processTemplateFromClasspath(
                        "/templates/email/email-bottom.tmpl", params));
                
                String htmlBody = GroovyTemplateUtils.processTemplateFromClasspath(
                        "/templates/email/aggregated-items-body.tmpl", params);

                if (StringUtils.isNoneBlank(emailPrefixFileLocation)) {
                    File emailPrefixFile = new File(emailPrefixFileLocation);
                    if (emailPrefixFile.exists()) {
                        String contents = FileUtils.readFileToString(emailPrefixFile, "UTF-8");
                        htmlBody = contents + htmlBody;
                    }
                }

                MimeMessagePreparator preparator = EmailUtils.createPreparator(to, subject, from, replyTo, htmlBody);

                mailSender.send(preparator);
            } catch (Exception e) {
                logger.warn("Cannot send email notification {}.", notification, e);
                // TODO throw and retry sending after.
            }
            long end = System.currentTimeMillis();
            logger.trace("Email send operation took {} s.", ((end - start) / 1000.0));

            for (MonItemNotification ntf : sendNotifications) {
                sentMonItemStorage.markSent(ntf.getJob().getId().toString(), ntf.getUser().getId().toString());
            }
        } else {
            logger.debug("No messages to be sent for notification due to already sent status '{}'.", notification);
        }
    }
}
