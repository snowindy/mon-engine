package com.github.snowindy.mon.beans;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.snowindy.mon.entity.MonItemNotification;
import com.github.snowindy.spring.util.IPropertiesHolder;

@Service
public class NotificationDestinationFilter {

    @Autowired
    private IPropertiesHolder propertiesHolder;

    private final static Logger logger = LoggerFactory.getLogger(NotificationDestinationFilter.class);

    public boolean check(MonItemNotification monItemNotification) {
        boolean allowed = true;
        String[] filterList = propertiesHolder.getConfig().getStringArray("notify.filter.suffix.blacklist");
        if (filterList != null) {
            for (String listItem : filterList) {
                if (StringUtils.endsWithIgnoreCase(monItemNotification.getUser().getEmail(), listItem)) {
                    logger.debug("Filtered destination {}", monItemNotification.getUser().getEmail());
                    allowed = false;
                    break;
                }
            }
        }
        return allowed;
    }

    public void setPropertiesHolder(IPropertiesHolder propertiesHolder) {
        this.propertiesHolder = propertiesHolder;
    }

}
