package com.github.snowindy.mon.beans;

import java.io.File;
import java.util.HashMap;

import org.apache.camel.Handler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import com.github.snowindy.mon.entity.MonItemNotification;
import com.github.snowindy.mon.entity.repository.ISentMonItemStorage;
import com.github.snowindy.mon.i.IMonItemNotifier;
import com.github.snowindy.spring.util.EmailUtils;
import com.github.snowindy.spring.util.PropertiesHolder;
import com.github.snowindy.util.template.GroovyTemplateUtils;
import com.github.snowindy.util.text.StringUtil;
import com.google.common.collect.ImmutableMap;

/**
 * TODO [Minor] Add word search words highlighting.
 * 
 */
@Service
public class SingleMonItemEmailNotifier implements IMonItemNotifier {

    private final static Logger logger = LoggerFactory.getLogger(SingleMonItemEmailNotifier.class);

    public SingleMonItemEmailNotifier() {
        super();
    }

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private PropertiesHolder propertiesHolder;

    @Value("${notify.email.title.prefix:LanceMonitor}")
    private String emailTitlePrefix;

    @Value("${notify.email.prefix.file.location:}")
    private String emailPrefixFileLocation;

    @Autowired
    private ISentMonItemStorage sentMonItemStorage;

    @Handler
    @Override
    public void notify(final MonItemNotification notification) {
        logger.trace("Sending email notification {}.", notification);

        if (!sentMonItemStorage.isSent(notification.getJob().getId().toString(), notification.getUser().getId()
                .toString())) {

            long start = System.currentTimeMillis();

            try {
                String subject = emailTitlePrefix + ": " + notification.getJob().getTitle();
                String from = propertiesHolder.getConfig().getString("notify.email.from");
                String replyTo = propertiesHolder.getConfig().getString("notify.email.replyTo",
                        propertiesHolder.getConfig().getString("notify.email.from"));
                String to = notification.getUser().getEmail();

                String unsubscribeUrlTmpl = propertiesHolder.getConfig().getString("notify.email.unsubscribe.urlTmpl");

                String unsubscribeUrl = StringUtils.isNotBlank(unsubscribeUrlTmpl) ? String.format(unsubscribeUrlTmpl,
                        notification.getUser().getUuid()) : null;

                HashMap<String, Object> params = new HashMap<String, Object>();
                params.put("user", notification.getUser());
                params.put("notification", notification);
                params.put("html", ImmutableMap.of("body",
                        StringUtil.convertTextToHtml(notification.getJob().getTextBody()), "jobInfo",
                        StringUtil.convertTextToHtml(notification.getJob().getJobInfo()), "clientInfo",
                        StringUtil.convertTextToHtml(notification.getJob().getClientInfo())));

                params.put("emailTop",
                        GroovyTemplateUtils.processTemplateFromClasspath("/templates/email/email-top.tmpl", params));
                params.put("emailBottom",
                        GroovyTemplateUtils.processTemplateFromClasspath("/templates/email/email-bottom.tmpl", params));

                String htmlBody = GroovyTemplateUtils.processTemplateFromClasspath(
                        "/templates/email/new-job-body.tmpl", params);

                if (StringUtils.isNoneBlank(emailPrefixFileLocation)) {
                    File emailPrefixFile = new File(emailPrefixFileLocation);
                    if (emailPrefixFile.exists()) {
                        String contents = FileUtils.readFileToString(emailPrefixFile, "UTF-8");
                        htmlBody = contents + htmlBody;
                    }
                }

                MimeMessagePreparator preparator = EmailUtils.createPreparator(to, subject, from, replyTo, htmlBody,
                        unsubscribeUrl);

                mailSender.send(preparator);
            } catch (Exception e) {
                logger.warn("Cannot send email notification {}.", notification, e);
                // TODO throw and retry sending after.
            }
            long end = System.currentTimeMillis();
            logger.trace("Email send operation took {} s.", ((end - start) / 1000.0));

            sentMonItemStorage.markSent(notification.getJob().getId().toString(), notification.getUser().getId()
                    .toString());
        } else {
            logger.debug("Send operation was omitted due to notification is already sent: '{}'.", notification);
        }
    }
}
