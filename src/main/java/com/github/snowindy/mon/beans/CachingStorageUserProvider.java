package com.github.snowindy.mon.beans;

import java.util.Collection;
import java.util.Date;

import javax.annotation.PostConstruct;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.snowindy.mon.ITransactionalWrapper;
import com.github.snowindy.mon.entity.House;
import com.github.snowindy.mon.entity.User;
import com.github.snowindy.mon.entity.repository.UserRepository;
import com.github.snowindy.mon.i.IUserProvider;
import com.github.snowindy.spring.util.IPropertiesHolder;

@Transactional(readOnly = true)
@Service
public class CachingStorageUserProvider implements IUserProvider {

    private final static Logger logger = LoggerFactory.getLogger(CachingStorageUserProvider.class);

    @Autowired
    private IPropertiesHolder propertiesHolder;

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private UserRepository userRepository;

    private volatile Cache cache;

    @Autowired
    private ITransactionalWrapper transactionalWrapper;

    @PostConstruct
    private void init() {
        int defaultTimeToLive = 1 * 60;
        int maxCacheElements = 1000;
        cache = new Cache(
                new CacheConfiguration(CachingStorageUserProvider.class.getName(), maxCacheElements)
                        .timeToLiveSeconds(propertiesHolder.getConfig().getLong(
                                "engine.cache.storage.houseUsers.timeToLive.sec", defaultTimeToLive)));
        cache.getCacheConfiguration().freezeConfiguration();
        cacheManager.addCache(cache);

        logger.info("Performing data migration.");

        transactionalWrapper.withTransaction(new Runnable() {

            @Override
            public void run() {
                Iterable<User> users = userRepository.findAll();
                for (User user : users) {
                    // data migration
                    if (user.migrateIfNeeded()) {
                        userRepository.save(user);
                    }
                }
            }
        });
    }

    @SuppressWarnings({ "unchecked" })
    @Override
    public Collection<User> getUsersInterestedInHouse(House house) {
        try {
            Element element;
            if ((element = cache.get(house.getDomainName())) != null) {
                Collection<User> users = (Collection<User>) element.getObjectValue();
                logger.trace("Users-interested-in-house cache hit: {}. Size: {}", house.getDomainName(), users.size());
                return users;
            }
            Collection<User> users = userRepository.findAllByHouseWithNoSearchDateBefore(house, new Date());

            logger.trace("Users-interested-in-house starage data retrieval: {}. Size: {}.", house.getDomainName(),
                    users.size());
            cache.put(new Element(house.getDomainName(), users));

            return users;
        } catch (Exception e) {
            logger.error("", e);
            throw new RuntimeException(e);
        }
    }

}
