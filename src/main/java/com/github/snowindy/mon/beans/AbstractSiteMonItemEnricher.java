package com.github.snowindy.mon.beans;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.snowindy.mon.beans.DataRetrievalLogic.DataRetrievalLogicException;
import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.entity.StoredMonItem;
import com.github.snowindy.mon.i.IHouseDataSource;
import com.github.snowindy.mon.i.IJobEnricher;
import com.github.snowindy.mon.i.parse.IMonItemPageParser;
import com.github.snowindy.mon.i.parse.ex.InsufficientViewRightsItemException;
import com.github.snowindy.mon.i.parse.ex.ItemBlockedException;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;
import com.github.snowindy.util.net.ex.DataTransferException;

public abstract class AbstractSiteMonItemEnricher implements IJobEnricher {

    @Autowired
    IHouseDataSource houseDataSource;

    @Autowired
    private DataRetrievalLogic dataRetrievalLogic;

    protected String getJobPageText(StoredMonItem jobPreview) {
        return houseDataSource.getContentFromUrl(jobPreview.getUrl());
    }

    private final static Logger logger = LoggerFactory.getLogger(AbstractSiteMonItemEnricher.class);

    protected abstract IMonItemPageParser createMonItemPageParser();

    protected abstract Logger getLogger();

    @Override
    public StoredMonItem enrich(StoredMonItem monItem) {

        getLogger().debug("Enriching item {}", monItem);

        String sourceText = getJobPageText(monItem);
        IMonItemPageParser parser = createMonItemPageParser();
        parser.setRelativeUrlPrefix(monItem.getHouseTopic().getHouse().getUrlPrefix());
        parser.parse(sourceText, monItem.getUrl());
        try {
            MonItem enriched = parser.getJob();

            monItem.setTextBody(enriched.getTextBody());
            monItem.setMetadata(enriched.getMetadata());

            try {
                dataRetrievalLogic.checkEnrichmentResult(monItem, sourceText, monItem.getUrl());
            } catch (DataRetrievalLogicException e) {
                getLogger().error("Data retrieval enrichment logic error detected", e);
            }
        } catch (ItemBlockedException e) {
            logger.info(e.getMessage());
        } catch (InsufficientViewRightsItemException e) {
            // we have preview only, ok: processing it as a normal item.
        } catch (NotAvailableException e) {
            // we should never hit this point: all non-availability causes
            // should be handled as separate exception classes.
            logger.error("Programming logic bug.", e);
        } catch (DataTransferException e) {
            logger.warn("Could not transfer data, omitting processing: {}", ExceptionUtils.getRootCauseMessage(e));
            return null;
        }

        return monItem;
    }

    public IHouseDataSource getHouseDataSource() {
        return houseDataSource;
    }

    public void setHouseDataSource(IHouseDataSource houseDataSource) {
        this.houseDataSource = houseDataSource;
    }

}
