package com.github.snowindy.mon.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.github.snowindy.mon.i.parse.ITopicPageParser;

public class DefaultMonItemSource extends AbstractSiteMonItemSource {

    private final static Logger logger = LoggerFactory.getLogger(DefaultMonItemSource.class);

    public DefaultMonItemSource() {
        super();
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    protected ITopicPageParser createTopicPageParser() {
        return (ITopicPageParser) applicationContext.getBean(BeanNameUtils.getParserByName(parserBeanName));
    }

    private String parserBeanName;

    public void setParserBeanName(String parserClassName) {
        this.parserBeanName = parserClassName;
        createTopicPageParser();
    }

}
