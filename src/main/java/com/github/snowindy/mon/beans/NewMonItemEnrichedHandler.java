package com.github.snowindy.mon.beans;

import io.keen.client.java.KeenClient;
import io.keen.client.java.exceptions.KeenException;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.github.snowindy.mon.ITransactionalWrapper;
import com.github.snowindy.mon.entity.StoredMonItem;
import com.github.snowindy.mon.entity.repository.MonItemRepository;
import com.github.snowindy.mon.i.IMonItemHandler;

@Service
public class NewMonItemEnrichedHandler implements IMonItemHandler {

    @Autowired
    private MonItemRepository monItemRepository;

    @Autowired
    private ITransactionalWrapper transactionalWrapper;

    @Autowired
    private AnalyticsManager analyticsManager;

    private final static Logger logger = LoggerFactory.getLogger(NewMonItemEnrichedHandler.class);

    @Override
    public void handle(final StoredMonItem monItem) {

        storeAnalyticsData(monItem);

        try {
            transactionalWrapper.withTransaction(new Runnable() {
                @Override
                public void run() {
                    StoredMonItem fromRepo = monItemRepository.findOneByUuid(monItem.getUuid());
                    fromRepo.setTextBody(monItem.getTextBody());
                    monItemRepository.save(fromRepo);
                }
            });
        } catch (DataIntegrityViolationException e) {
            logger.error(
                    "Mon item already exits. This should not happend as it filter is responsible for checks. {}, {}",
                    monItem, e.getMessage());
        } catch (Exception e) {
            logger.error("", e);
            throw new RuntimeException(e);
        }
    }

    private void storeAnalyticsData(StoredMonItem monItem) {
        if (analyticsManager.isAnalyticsEnabled()) {
            Map<String, Object> event = new HashMap<String, Object>();
            event.put("uuid", monItem.getUuid());
            event.put("url", monItem.getUrl());
            event.put("date", monItem.getDateDetected());
            event.put("title", monItem.getTitle());
            event.put("house", monItem.getHouseTopic().getHouse().getDomainName());

            try {
                KeenClient.client().addEvent("monItems", event);
            } catch (KeenException e) {
                logger.warn("Cannot write event to analytics.", e);
            }
        }
    }
}
