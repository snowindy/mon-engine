package com.github.snowindy.mon.beans;

import io.keen.client.java.KeenClient;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class AnalyticsManager {
    @Value("${keen.projectId}")
    private String keenProjectId;

    @Value("${keen.writeKey}")
    private String keenWriteKey;

    @Value("${keen.readKey}")
    private String keenReadKey;

    private boolean analyticsEnabled = true;

    @PostConstruct
    private void initKeenAnalytics() {
        if (StringUtils.isNotBlank(keenProjectId)) {
            KeenClient.initialize(keenProjectId, keenWriteKey, keenReadKey);
        } else {
            analyticsEnabled = false;
        }
    }

    public boolean isAnalyticsEnabled() {
        return analyticsEnabled;
    }
}
