package com.github.snowindy.mon.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.github.snowindy.mon.i.IHouseDataSource;
import com.github.snowindy.util.net.IHttpGateway;

@Primary
@Service
public class HttpHouseDataSource implements IHouseDataSource {

    @Autowired
    private IHttpGateway httpGateway;

    @Override
    public String getContentFromUrl(String url) {
        return httpGateway.readHtmlPageAsString(url, encoding);
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    private String encoding;

}
