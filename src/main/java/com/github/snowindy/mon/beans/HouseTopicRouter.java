package com.github.snowindy.mon.beans;

import com.github.snowindy.mon.entity.House;
import com.github.snowindy.mon.entity.HouseTopic;

public class HouseTopicRouter {
    private static final String SEDA_QUEUE_PREFIX = "seda:houseTopicTasks:";

    public String resolveHouseChannel(HouseTopic topic) {
        return resolveHouseTopicChannel(topic.getHouse());
    }

    public static String resolveHouseTopicChannel(House house) {
        return SEDA_QUEUE_PREFIX + house.getDomainName();
    }
}
