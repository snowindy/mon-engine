package com.github.snowindy.mon.beans;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.github.snowindy.mon.entity.StoredMonItem;

@Service
public class DataRetrievalLogic {

    private final static Logger logger = LoggerFactory.getLogger(DataRetrievalLogic.class);

    public void checkTopicResult(List<StoredMonItem> items, String sourceText, String sourceDescriptor,
                                 boolean topicMonItemsMustHaveBody) throws DataRetrievalLogicException {
        if (items.isEmpty()) {
            throw new NoTopicElementsException(sourceDescriptor, sourceText);
        }

        Iterator<StoredMonItem> it = items.iterator();
        int idx = -1;
        while (it.hasNext()) {
            idx++;
            StoredMonItem monItem = it.next();

            try {
                if (StringUtils.isBlank(monItem.getUrl())) {
                    throw new IncorrectTopicItemContent(sourceDescriptor, sourceText, monItem, "url");
                }
                if (StringUtils.isBlank(monItem.getTitle())) {
                    throw new IncorrectTopicItemContent(sourceDescriptor, sourceText, monItem, "title");
                }
                if (topicMonItemsMustHaveBody && StringUtils.isBlank(monItem.getTextBody())) {
                    throw new IncorrectTopicItemContent(sourceDescriptor, sourceText, monItem, "textBody");
                }
            } catch (Exception e) {
                logger.warn(e.getMessage() + " idx: " + idx);
                it.remove();
            }
        }
    }

    public void checkEnrichmentResult(StoredMonItem monItem, String sourceText, String sourceDescriptor)
            throws DataRetrievalLogicException {
        if (StringUtils.isBlank(monItem.getUrl())) {
            throw new IncorrectEnrichmentItemContent(sourceDescriptor, sourceText, monItem, "url");
        }
        if (StringUtils.isBlank(monItem.getTitle())) {
            throw new IncorrectEnrichmentItemContent(sourceDescriptor, sourceText, monItem, "title");
        }
        if (StringUtils.isBlank(monItem.getTextBody())) {
            throw new IncorrectEnrichmentItemContent(sourceDescriptor, sourceText, monItem, "textBody");
        }
    }

    public abstract static class DataRetrievalLogicException extends Exception {
        private String sourceDescriptor;
        private String sourceText;
        private static final long serialVersionUID = -4495783954885925419L;

        public DataRetrievalLogicException(String sourceDescriptor, String sourceText, String msg) {
            super(msg + ". Source descriptor: '" + sourceDescriptor + "'. Source text: '" + sourceText + "'");
            this.sourceDescriptor = sourceDescriptor;
        }

        public String getSourceDescriptor() {
            return sourceDescriptor;
        }

        public String getSourceText() {
            return sourceText;
        }

    }

    public static class NoTopicElementsException extends DataRetrievalLogicException {
        public NoTopicElementsException(String sourceDescriptor, String sourceText) {
            super(sourceDescriptor, sourceText, "Topic returned no elements");
        }

        private static final long serialVersionUID = 3452631562826970790L;
    }

    public static class IncorrectTopicItemContent extends DataRetrievalLogicException {

        public IncorrectTopicItemContent(String sourceDescriptor, String sourceText, StoredMonItem item, String cause) {
            super(sourceDescriptor, sourceText, "Topic item is invalid: " + item + " Cause: '" + cause + "'");
        }

        private static final long serialVersionUID = -158269553233820427L;
    }

    public static class IncorrectEnrichmentItemContent extends DataRetrievalLogicException {

        public IncorrectEnrichmentItemContent(String sourceDescriptor, String sourceText, StoredMonItem item,
                String cause) {
            super(sourceDescriptor, sourceText, "Enrichment item is invalid: " + item + " Cause: '" + cause + "'");
        }

        private static final long serialVersionUID = -158269553233820427L;
    }
}
