package com.github.snowindy.mon.beans;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.snowindy.mon.entity.House;
import com.github.snowindy.mon.entity.HouseTopic;
import com.github.snowindy.mon.entity.repository.HouseTopicRepository;
import com.github.snowindy.mon.i.IHouseTopicsSource;
import com.google.common.collect.Lists;

@Transactional
@Service
public class HouseTopicsSource implements IHouseTopicsSource {

    @Autowired
    private HouseTopicRepository houseTopicRepository;

    @Override
    public Collection<HouseTopic> getTopics(House house) {
        return Lists.newArrayList(houseTopicRepository.findAllByHouseAndActive(house, true));
    }
}
