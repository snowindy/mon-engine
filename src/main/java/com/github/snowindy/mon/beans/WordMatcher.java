package com.github.snowindy.mon.beans;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.github.snowindy.util.text.WordMatchUtils;

@Service
public class WordMatcher {

    private final static Logger logger = LoggerFactory.getLogger(WordMatcher.class);

    public boolean matches(Collection<String> allWords, Collection<String> anyWords, Collection<String> stopWords,
                           String text) {
        if (!WordMatchUtils.containsAnyKeywords(stopWords, text)) {
            if (!allWords.isEmpty()) {
                logger.trace("Checking all words: " + allWords);
                return WordMatchUtils.containsAllKeywords(allWords, text);
            } else {
                logger.trace("Checking any words: " + anyWords);
                return WordMatchUtils.containsAnyKeywords(anyWords, text);
            }
        }
        logger.trace("No match: contains stops: " + stopWords);
        return false;
    }
}
