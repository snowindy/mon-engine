package com.github.snowindy.mon.beans;

import org.apache.commons.lang3.StringUtils;

import com.github.snowindy.mon.parse.OdeskApiTopicPageParser;

public class BeanNameUtils {
    public static String getBeanNamePrefixFromDomainName(String domainName) {
        domainName = domainName.toLowerCase();
        int idx;
        while ((idx = domainName.indexOf(".")) != -1) {
            char nextChar = domainName.charAt(idx + 1);
            String pref = StringUtils.substring(domainName, 0, idx);
            String suff = StringUtils.substring(domainName, idx + 2, domainName.length());
            domainName = pref + Character.toUpperCase(nextChar) + suff;
        }

        domainName = StringUtils.replace(domainName, "_", "__");
        domainName = StringUtils.replace(domainName, "-", "_");
        if (domainName.matches("\\d.*")) {
            domainName = "_" + domainName;
        }

        return domainName;
    }

    public static String getBeanNameFromDomainName(String domainName, String suffix) {
        String pref = getBeanNamePrefixFromDomainName(domainName);
        return pref + suffix;
    }

    public static Class<?> getParserByName(String name) {
        try {
            return Class.forName(OdeskApiTopicPageParser.class.getPackage().getName() + "."
                    + StringUtils.capitalize(name));
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
