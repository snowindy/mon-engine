package com.github.snowindy.mon.beans;

import org.springframework.stereotype.Service;

import com.github.snowindy.mon.i.IPeriodicCleaner;

@Service
public class PeriodicCleaner implements IPeriodicCleaner {

    @Override
    public void cleanupShortPeriod(Object body) {
    }

    @Override
    public void cleanupMediumPeriod(Object body) {
    }

    @Override
    public void cleanupLongPeriod(Object body) {
    }

}
