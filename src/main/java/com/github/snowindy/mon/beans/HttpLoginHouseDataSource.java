package com.github.snowindy.mon.beans;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.snowindy.mon.i.IHouseDataSource;
import com.github.snowindy.mon.i.ILoginCookieHelper;
import com.github.snowindy.mon.i.ILoginHeaderHelper;
import com.github.snowindy.mon.i.ILoginHelper;
import com.github.snowindy.mon.i.ILoginRequestParamHelper;
import com.github.snowindy.mon.parse.LoginException;
import com.github.snowindy.util.net.IHttpGateway;
import com.github.snowindy.util.net.ex.IncorrectStatusCodeException;

public class HttpLoginHouseDataSource implements IHouseDataSource {

    volatile ILoginHelper loginHelper;

    public ILoginHelper getLoginHelper() {
        return loginHelper;
    }

    public void setLoginHelper(ILoginHelper loginHelper) {
        this.loginHelper = loginHelper;
    }

    @Autowired
    private IHttpGateway httpGateway;

    public void setHttpGateway(IHttpGateway httpGateway) {
        this.httpGateway = httpGateway;
    }

    private final static Logger logger = LoggerFactory.getLogger(HttpLoginHouseDataSource.class);

    private String getTextWithLoginHelper(String url) {
        String pageText = null;
        try {
            if (loginHelper instanceof ILoginCookieHelper) {
                pageText = httpGateway.readHtmlPageAsString(url,
                        ((ILoginCookieHelper) loginHelper).getLoggedInCookieStore());
            } else if (loginHelper instanceof ILoginRequestParamHelper) {
                url = ((ILoginRequestParamHelper) loginHelper).updateUrlWithLoginParam(url);
                pageText = httpGateway.readHtmlPageAsString(url);
            } else if (loginHelper instanceof ILoginHeaderHelper) {
                Pair<String, String> header = ((ILoginHeaderHelper) loginHelper).getHeader(url);
                pageText = httpGateway.readHtmlPageAsString(url, header);
            } else {
                throw new IllegalStateException("loginHelper type is unknown: " + loginHelper.getClass());
            }
        } catch (IncorrectStatusCodeException e) {
            // Some sources return 401 response code with incorrect login
            // description. We look at description to detect if we're logged in,
            // not aware of response code.
            pageText = e.getResponseText();
        }
        return pageText;
    }

    private void loginIfNotLoggedIn() throws LoginException {
        if (!loginHelper.isLoggedIn()) {
            logger.trace("Not logged in - logging in.");
            synchronized (loginHelper) {
                if (!loginHelper.isLoggedIn()) {
                    loginHelper.logIn();
                }
            }
        }
    }

    private void loginAnyway() throws LoginException {
        synchronized (loginHelper) {
            loginHelper.logIn();
        }
    }

    @Override
    public String getContentFromUrl(String url) {
        try {
            logger.trace("Geting content with login");
            loginIfNotLoggedIn();

            String pageText = getTextWithLoginHelper(url);

            if (!loginHelper.checkLoggedIn(pageText)) {
                // try to relogin. In case of flushed server session etc.
                logger.trace("Try to relogin.");
                loginAnyway();

                pageText = getTextWithLoginHelper(url);

                // This is a double check: ensure that login algorythm still
                // works as expected (external site login algorythm update, for
                // example)
                if (!loginHelper.checkLoggedIn(pageText)) {
                    throw new LoginException("Login double check failure detected.\n\n\n" + pageText);
                }
            }

            return pageText;
        } catch (LoginException e) {
            logger.error("Cannot login to a house site.", e);
            throw new RuntimeException(e);
        }

    }

}
