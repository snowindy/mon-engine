package com.github.snowindy.mon.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.camel.Handler;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.github.snowindy.mon.MonitoringRouteBuilder;
import com.github.snowindy.mon.entity.MonItemNotification;
import com.github.snowindy.mon.entity.MultiMonItemNotification;
import com.github.snowindy.mon.entity.User;
import com.github.snowindy.mon.entity.enums.EAggregationPolicy;
import com.github.snowindy.mon.i.INotificationsThresholdPeriodHandler;
import com.github.snowindy.spring.util.IPropertiesHolder;
import com.google.common.util.concurrent.AtomicLongMap;

@Service
public class NotificationsThresholdPeriodHandler implements INotificationsThresholdPeriodHandler {

    private volatile AtomicLongMap<String> stats = AtomicLongMap.create();
    private volatile ConcurrentHashMap<String, List<MonItemNotification>> aggregatedMap = new ConcurrentHashMap<String, List<MonItemNotification>>();

    @Autowired
    private IPropertiesHolder propertiesHolder;

    @Autowired
    private ApplicationContext context;

    ProducerTemplate getProducerTemplate() {
        // We use lazy fetching for this bean cause @Autowired does not work
        // with Camel and @Component.
        ProducerTemplate producerTemplate = context.getBean(ProducerTemplate.class);
        return producerTemplate;
    }

    @Handler
    @Override
    public void process(MonItemNotification notification) {
        String key = notification.getUser().getEmail();

        long value = stats.addAndGet(key, 1);

        int maxNotificationsInPeriod = propertiesHolder.getConfig().getInt("notify.immediate.maxInPeriod.number",
                DEFAULT_MAX_NUMBER_MIXED);

        int maxNotificationsInPeriodAggregationNotDesired = propertiesHolder.getConfig().getInt(
                "notify.immediate.aggregationAvoided.maxInPeriod.number", DEFAULT_MAX_NUMBER_AVOIDED);

        boolean allowed;
        if (notification.getUser().getAggregationPolicy() == EAggregationPolicy.FULL) {
            allowed = false;
        } else if (notification.getUser().getAggregationPolicy() == EAggregationPolicy.MIXED) {
            allowed = value <= maxNotificationsInPeriod;
        } else if (notification.getUser().getAggregationPolicy() == EAggregationPolicy.AVOIDED) {
            allowed = value <= maxNotificationsInPeriodAggregationNotDesired;
        } else {
            throw new IllegalStateException("Unknown aggregation policy: "
                    + notification.getUser().getAggregationPolicy());
        }

        if (allowed) {
            logger.trace("Sending immediate {}.", notification);
            getProducerTemplate().sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_SINGLE, notification);
        } else {
            delayNotificationForMulti(key, notification);
        }
    }

    void delayNotificationForMulti(String key, MonItemNotification notification) {
        logger.trace("Delaying to aggregate {}.", notification);
        List<MonItemNotification> notifications = new ArrayList<MonItemNotification>();
        List<MonItemNotification> ntfPrev = aggregatedMap.putIfAbsent(key, notifications);
        notifications = ntfPrev == null ? notifications : ntfPrev;
        notifications.add(notification);
    }

    private final static Logger logger = LoggerFactory.getLogger(NotificationsThresholdPeriodHandler.class);

    @Override
    public void cleanup(Object body) {
        logger.debug("Performing counting map cleanup. Map contents: {}", stats);
        stats.clear();
    }

    @Override
    public void processAggregated(Object body) {
        logger.trace("Processing aggregate {}.", aggregatedMap);

        for (Entry<String, List<MonItemNotification>> e : aggregatedMap.entrySet()) {

            if (!e.getValue().isEmpty()) {
                if (e.getValue().size() == 1) {
                    logger.trace("Sending single notification from bundle mode for user {}.", e.getKey());

                    getProducerTemplate().sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_SINGLE,
                            e.getValue().get(0));
                } else {
                    logger.trace("Sending notifications bundle for user {}. Notifications size: {}.", e.getKey(), e
                            .getValue().size());
                    User user = e.getValue().get(0).getUser();

                    MultiMonItemNotification multiMonItemNotification = new MultiMonItemNotification(user, e.getValue());

                    getProducerTemplate().sendBody(MonitoringRouteBuilder.QUEUE_NOTIFICATIONS_MULTI,
                            multiMonItemNotification);
                }
            }
        }

        aggregatedMap.clear();
    }
}
