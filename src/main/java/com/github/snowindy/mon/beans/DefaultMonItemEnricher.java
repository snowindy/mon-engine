package com.github.snowindy.mon.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.github.snowindy.mon.i.parse.IMonItemPageParser;

public class DefaultMonItemEnricher extends AbstractSiteMonItemEnricher {

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    protected IMonItemPageParser createMonItemPageParser() {
        return (IMonItemPageParser) applicationContext.getBean(BeanNameUtils.getParserByName(parserBeanName));
    }

    private String parserBeanName;

    public void setParserBeanName(String parserBeanName) {
        this.parserBeanName = parserBeanName;
        createMonItemPageParser();
    }

    private final static Logger logger = LoggerFactory.getLogger(DefaultMonItemEnricher.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

}
