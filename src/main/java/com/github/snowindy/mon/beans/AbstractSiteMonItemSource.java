package com.github.snowindy.mon.beans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.camel.Handler;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.snowindy.mon.beans.DataRetrievalLogic.DataRetrievalLogicException;
import com.github.snowindy.mon.entity.HouseTopic;
import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.entity.StoredMonItem;
import com.github.snowindy.mon.i.IHouseDataSource;
import com.github.snowindy.mon.i.IMonItemSource;
import com.github.snowindy.mon.i.parse.ITopicPageParser;
import com.github.snowindy.mon.i.parse.ex.ItemBlockedException;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;
import com.github.snowindy.mon.parse.LoginException;
import com.github.snowindy.util.net.ex.DataTransferException;

public abstract class AbstractSiteMonItemSource implements IMonItemSource {

    public AbstractSiteMonItemSource() {
        super();
    }

    @Handler
    @Override
    public List<StoredMonItem> getMonItems(HouseTopic topic) {

        try {
            List<StoredMonItem> res = getJobsInner(topic);

            for (StoredMonItem jobPreview : res) {
                jobPreview.setHouseTopic(topic);
            }

            return res;
        } catch (Exception e) {
            getLogger().error("", e);
            throw new RuntimeException(e);
        }
    }

    protected abstract Logger getLogger();

    @Autowired
    IHouseDataSource houseDataSource;

    public void setHouseDataSource(IHouseDataSource houseDataSource) {
        this.houseDataSource = houseDataSource;
    }

    protected String getTopicSourceData(HouseTopic topic) {
        getLogger().trace("Getting data from '{}' with '{}'.", topic.getUrl(), houseDataSource);
        String text = houseDataSource.getContentFromUrl(topic.getUrl());
        getLogger().trace("Response '{}'", text);
        return text;
    }

    protected abstract ITopicPageParser createTopicPageParser();

    private DataRetrievalLogic dataRetrievalLogic = new DataRetrievalLogic();

    protected List<StoredMonItem> getJobsInner(HouseTopic topic) throws LoginException {
        ITopicPageParser parser = createTopicPageParser();
        parser.setRelativeUrlPrefix(topic.getHouse().getUrlPrefix());
        try {
            String sourceText = getTopicSourceData(topic);
            parser.parse(sourceText, topic.getUrl());

            List<StoredMonItem> previews = new ArrayList<StoredMonItem>();
            try {
                List<MonItem> previews1 = parser.getMonItems();
                for (MonItem monItem : previews1) {
                    previews.add(new StoredMonItem(monItem));
                }
            } catch (ItemBlockedException e) {
                getLogger().info(e.getMessage());
            } catch (NotAvailableException e) {
                // we should never hit this point: all non-availability causes
                // should be handled as separate exception classes.
                getLogger().error("Programming logic bug.", e);
            }

            try {
                dataRetrievalLogic.checkTopicResult(previews, sourceText, topic.getUrl(),
                        parser.isTopicMonItemsMustHaveBody());
            } catch (DataRetrievalLogicException e) {
                getLogger().error("Data retrieval topic logic error detected", e);
            }
            return previews;
        } catch (DataTransferException e) {
            getLogger().warn("Could not transfer data: {}", ExceptionUtils.getRootCauseMessage(e));
            return Collections.emptyList();
        }
    }
}
