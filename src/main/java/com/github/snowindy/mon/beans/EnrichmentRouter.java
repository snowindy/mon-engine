package com.github.snowindy.mon.beans;

import com.github.snowindy.mon.entity.House;
import com.github.snowindy.mon.entity.StoredMonItem;

public class EnrichmentRouter {
    private static final String SEDA_QUEUE_PREFIX = "seda:enrichment:";
    
    public String resolveEnrichmentChannel(StoredMonItem jp) {
        return resolveEnrichmentChannel(jp.getHouseTopic().getHouse());
    }

    public static String resolveEnrichmentChannel(House house) {
        return SEDA_QUEUE_PREFIX + house.getDomainName();
    }
}
