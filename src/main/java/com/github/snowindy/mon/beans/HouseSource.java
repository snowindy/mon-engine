package com.github.snowindy.mon.beans;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.snowindy.mon.entity.House;
import com.github.snowindy.mon.entity.repository.HouseRepository;
import com.github.snowindy.mon.i.IHouseSource;
import com.google.common.collect.Lists;

@Transactional
@Service
public class HouseSource implements IHouseSource {

    @Autowired
    private HouseRepository houseRepository;

    @Override
    public Collection<House> getActiveHouses(Object body) {
        return Lists.newArrayList(houseRepository.findAllByActive(true));
    }  
}
