package com.github.snowindy.mon.i;

import org.apache.commons.lang3.tuple.Pair;

public interface ILoginHeaderHelper extends ILoginHelper {

    Pair<String, String> getHeader(String requestUrl);

}
