package com.github.snowindy.mon.i.parse.ex;

public abstract class NotAvailableException extends Exception {

    private static final long serialVersionUID = 1L;

    protected NotAvailableException(String message, String sourceDescriptor) {
        super(String.format(message + " Source: '%s'.", sourceDescriptor));
    }
}
