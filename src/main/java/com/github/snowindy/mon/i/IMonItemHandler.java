package com.github.snowindy.mon.i;

import com.github.snowindy.mon.entity.StoredMonItem;

public interface IMonItemHandler {
    void handle(StoredMonItem monItem);
}
