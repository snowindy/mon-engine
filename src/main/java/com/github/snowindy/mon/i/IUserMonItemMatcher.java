package com.github.snowindy.mon.i;

import java.util.List;

import com.github.snowindy.mon.entity.StoredMonItem;
import com.github.snowindy.mon.entity.MonItemNotification;

public interface IUserMonItemMatcher {
    List<MonItemNotification> matchJobForUsers(StoredMonItem job);
}
