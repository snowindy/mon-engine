package com.github.snowindy.mon.i;

import java.util.List;

import com.github.snowindy.mon.entity.HouseTopic;
import com.github.snowindy.mon.entity.StoredMonItem;

public interface IMonItemSource {
	List<StoredMonItem> getMonItems(HouseTopic topic);
}
