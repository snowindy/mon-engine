package com.github.snowindy.mon.i;

import com.github.snowindy.mon.entity.StoredMonItem;

public interface IMonItemNotificationHandler {
    void process(StoredMonItem monItem);
}
