package com.github.snowindy.mon.i;

import com.github.snowindy.mon.entity.MonItemNotification;

public interface INotificationsThresholdPeriodHandler {

    int DEFAULT_MAX_NUMBER_MIXED = 10;
    int DEFAULT_MAX_NUMBER_AVOIDED = 20;
    public static final String METHOD_NAME_PROCESS = "process";
    public static final String METHOD_NAME_CLEANUP = "cleanup";
    public static final String METHOD_NAME_PROCESS_AGGREGATED = "processAggregated";

    void process(MonItemNotification notification);

    void cleanup(Object body);

    void processAggregated(Object body);
}
