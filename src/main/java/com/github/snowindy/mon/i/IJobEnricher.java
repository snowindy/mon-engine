package com.github.snowindy.mon.i;

import com.github.snowindy.mon.entity.StoredMonItem;

public interface IJobEnricher {
	StoredMonItem enrich(StoredMonItem job);
}
