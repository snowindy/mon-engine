package com.github.snowindy.mon.i;

public interface IHouseDataSource {
	String getContentFromUrl(String url);
}
