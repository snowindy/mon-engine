package com.github.snowindy.mon.i;

import java.util.Collection;

import com.github.snowindy.mon.entity.House;
import com.github.snowindy.mon.entity.HouseTopic;

public interface IHouseTopicsSource {
	Collection<HouseTopic> getTopics(House house);
}
