package com.github.snowindy.mon.i;

import java.util.Collection;

import com.github.snowindy.mon.entity.House;
import com.github.snowindy.mon.entity.User;

public interface IUserProvider {
	Collection<User> getUsersInterestedInHouse(House house);
}
