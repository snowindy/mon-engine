package com.github.snowindy.mon.i;

import com.github.snowindy.mon.entity.MultiMonItemNotification;

public interface IMultiMonItemNotifier {
    void notify(MultiMonItemNotification notification);
}
