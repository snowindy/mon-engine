package com.github.snowindy.mon.i;

import com.github.snowindy.mon.entity.StoredMonItem;

public interface INewMonItemFilter {
	boolean isNew(StoredMonItem job);
}
