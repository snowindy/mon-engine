package com.github.snowindy.mon.i;

public interface IPeriodicCleaner {
    void cleanupShortPeriod(Object body);

    void cleanupMediumPeriod(Object body);

    void cleanupLongPeriod(Object body);
}
