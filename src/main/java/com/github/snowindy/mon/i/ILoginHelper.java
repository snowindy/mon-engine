package com.github.snowindy.mon.i;

import com.github.snowindy.mon.parse.LoginException;

public interface ILoginHelper {

    void setLogin(String login);

    void setPassword(String password);

    boolean checkLoggedIn(String source);

    boolean isLoggedIn();

    void logIn() throws LoginException;
}
