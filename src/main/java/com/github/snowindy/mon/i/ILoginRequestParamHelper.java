package com.github.snowindy.mon.i;


public interface ILoginRequestParamHelper extends ILoginHelper {

    String updateUrlWithLoginParam(String url);

}
