package com.github.snowindy.mon.i;

import org.apache.http.client.CookieStore;

public interface ILoginCookieHelper extends ILoginHelper {

    /**
     * Variable must be volatile for concurrent access.
     */
    CookieStore getLoggedInCookieStore();

}
