package com.github.snowindy.mon.i;

import java.util.Collection;

import com.github.snowindy.mon.entity.House;

public interface IHouseSource {
	Collection<House> getActiveHouses(Object body);
}
