package com.github.snowindy.mon.i;

import com.github.snowindy.mon.entity.MonItemNotification;

public interface IMonItemNotifier {
    void notify(MonItemNotification notification);
}
