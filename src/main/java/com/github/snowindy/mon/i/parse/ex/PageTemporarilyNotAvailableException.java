package com.github.snowindy.mon.i.parse.ex;

public class PageTemporarilyNotAvailableException extends NotAvailableException {

    private static final long serialVersionUID = 1L;

    public PageTemporarilyNotAvailableException(String sourceDescriptor) {
        super("Page is temporarily not available.", sourceDescriptor);
    }
}
