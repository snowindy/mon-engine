package com.github.snowindy.mon;

import java.util.UUID;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DeadlockLoserDataAccessException;
import org.springframework.stereotype.Service;

/**
 * Retries transactional work in case of deadlock exception.
 * 
 */
@Service
public class DeadlockRetryWrapper implements IDeadlockRetryWrapper {

    @Autowired
    private ITransactionalWrapper transactionalWrapper;

    private final static Logger logger = LoggerFactory.getLogger(DeadlockRetryWrapper.class);


    @Override
    public <T> T withTransaction(Callable<T> callable) {
        return withTransaction(callable, 5);
    }

    
    @Override
    public void withTransaction(final Runnable runnable) {
        withTransaction(runnable, 5);
    }

   
    @Override
    public void withTransaction(final Runnable runnable, int maxTries) {
        withTransaction(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                runnable.run();
                return null;
            }
        }, maxTries);
    }

    
    @Override
    public <T> T withTransaction(Callable<T> callable, int maxTries) {
        String transactionGenId = UUID.randomUUID().toString();
        int tryNum = 0;
        T res = null;
        while (true) {
            tryNum++;
            try {
                res = transactionalWrapper.withTransaction(callable);
                break;
            } catch (DeadlockLoserDataAccessException e) {
                if (tryNum > maxTries) {
                    String msg = String.format(
                            "Unable to perform transactional job '%s' within '%' tries due to deadlock exception.",
                            transactionGenId, tryNum);
                    logger.error(msg, e);
                    throw e;
                } else {
                    String msg = String
                            .format("Deadlock exception occured, try # '%s', retrying transactional job '%s' after sleep. Message: %s.",
                                    tryNum, transactionGenId, e.getMessage());
                    logger.info(msg);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e1) {
                    }
                }
            }
        }
        return res;
    }
}