package com.github.snowindy.mon;

import java.util.concurrent.Callable;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Utility class that enables execution of arbitrary code within a transaction.
 * 
 */
@Transactional
@Service
public class TransactionalWrapper implements ITransactionalWrapper {

    @Override
    public <T> T withTransaction(Callable<T> callable) {
        try {
            return callable.call();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void withTransaction(Runnable runnable) {
        runnable.run();
    }
}
