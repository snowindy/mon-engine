package com.github.snowindy.mon;

import org.apache.camel.builder.RouteBuilder;

import com.github.snowindy.mon.i.INotificationsThresholdPeriodHandler;
import com.github.snowindy.util.camel.CamelUtils;

public class MonItemNotificationThresholdRouteBuilder extends RouteBuilder {

    private static final String ROUTE_ID_PREFIX = "NotificationThreshold: ";

    @Override
    public void configure() {
        from(SchedulerRouteBuilder.TOPIC_NOTIFICATION_MAX_IN_PERIOD).to(
                CamelUtils.toBean(MonitoringRouteBuilder.BEAN_NOTIFICATION_THRESHOLD_PERIOD_HANDLER,
                        INotificationsThresholdPeriodHandler.METHOD_NAME_CLEANUP)).routeId(
                ROUTE_ID_PREFIX + "cleanup timer");

        from(SchedulerRouteBuilder.TOPIC_NOTIFICATION_AGGREGATION_FLUSH).to(
                CamelUtils.toBean(MonitoringRouteBuilder.BEAN_NOTIFICATION_THRESHOLD_PERIOD_HANDLER,
                        INotificationsThresholdPeriodHandler.METHOD_NAME_PROCESS_AGGREGATED)).routeId(
                ROUTE_ID_PREFIX + "aggr flush timer");

    }
}
