package com.github.snowindy.mon.entity;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

import com.github.snowindy.mon.entity.enums.EEntityType;

@Entity
@Table(name = "TOKENS")
public class Token extends AbstractPersistable<Long> {

    private static final long serialVersionUID = -288343472341509347L;

    public Token() {
        this.dateCreated = new Date();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 5);
        this.validUntil = cal.getTime();
    }

    /**
     * Unique entity identifier: For a user it's an email, etc
     */
    @Column(nullable = false)
    private String entityId;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private EEntityType entityType;

    @Column(nullable = false)
    private String uuid;

    @Column(nullable = false)
    private Date dateCreated;

    @Column
    private Date validUntil;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EEntityType entityType) {
        this.entityType = entityType;
    }

}
