package com.github.snowindy.mon.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.github.snowindy.util.text.JsonUtils;

public class Query {
    private BigDecimal minRatePerHour;
    private BigDecimal minProjectBudget;

    private List<QueryGroup> groups = new ArrayList<Query.QueryGroup>();

    public static class QueryGroup {
        private List<String> any = new ArrayList<String>();
        private List<String> stop = new ArrayList<String>();
        private List<String> all = new ArrayList<String>();

        public List<String> getAny() {
            return any;
        }

        public void setAny(List<String> any) {
            this.any = any;
        }

        public List<String> getStop() {
            return stop;
        }

        public void setStop(List<String> stop) {
            this.stop = stop;
        }

        public List<String> getAll() {
            return all;
        }

        public void setAll(List<String> all) {
            this.all = all;
        }

        public QueryGroup(Collection<String> any, Collection<String> stop, Collection<String> all) {
            super();
            this.any = new ArrayList<String>(any);
            this.stop = new ArrayList<String>(stop);
            this.all = new ArrayList<String>(all);
        }

        public QueryGroup() {

        }
    }

    public BigDecimal getMinRatePerHour() {
        return minRatePerHour;
    }

    public void setMinRatePerHour(BigDecimal minRatePerHour) {
        this.minRatePerHour = minRatePerHour;
    }

    public BigDecimal getMinProjectBudget() {
        return minProjectBudget;
    }

    public void setMinProjectBudget(BigDecimal minProjectBudget) {
        this.minProjectBudget = minProjectBudget;
    }

    public List<QueryGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<QueryGroup> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }

}