package com.github.snowindy.mon.entity.enums;

public enum ENotificationStatus {
    READY, IN_PROCESS, FAIL_THIS_TIME, // temp statuses
    SUCCESS, FAIL_MAX_TRIES // end statuses
}
