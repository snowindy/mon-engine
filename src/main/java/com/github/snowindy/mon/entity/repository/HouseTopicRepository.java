package com.github.snowindy.mon.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.github.snowindy.mon.entity.House;
import com.github.snowindy.mon.entity.HouseTopic;

/**
 * {@link PagingAndSortingRepository} to access {@link HouseTopic} instances.
 * 
 * @author Anton Novopashin
 */
public interface HouseTopicRepository extends
		PagingAndSortingRepository<HouseTopic, Long> {

	HouseTopic findOneByUrl(String url);

	List<HouseTopic> findAllByHouseAndActive(House house, boolean active);

	@Modifying
	@Query("delete from HouseTopic u where u.url = ?1")
	public void deleteByUrl(String url);
}
