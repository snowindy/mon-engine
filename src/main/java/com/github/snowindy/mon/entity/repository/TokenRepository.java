package com.github.snowindy.mon.entity.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.github.snowindy.mon.entity.Token;

public interface TokenRepository extends PagingAndSortingRepository<Token, Long> {
    Token findOneByUuid(String uuid);
}