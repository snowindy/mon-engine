package com.github.snowindy.mon.entity.repository;


public interface ISentMonItemStorage {

    void markSent(String monItemId, String userId);

    boolean isSent(String monItemId, String userId);
}