package com.github.snowindy.mon.entity.enums;

public enum EAggregationPolicy {
    FULL, MIXED, AVOIDED
}
