package com.github.snowindy.mon.entity.repository;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
@Service
public class SentMonItemStorage implements ISentMonItemStorage {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final static Logger logger = LoggerFactory.getLogger(SentMonItemStorage.class);

    @Transactional
    @Override
    public void markSent(String monItemId, String userId) {
        // TODO this gives more bad than good so far.
//        try {
//            jdbcTemplate.update("insert into MON_SENT_ITEMS(item,user,date_created) values(?,?,?)", monItemId, userId,
//                    new Date());
//        } catch (DataIntegrityViolationException e) {
//            logger.info("Duplicate record ('{}','{}'): '{}'.", monItemId, userId, e.getMessage());
//        }

    }

    @Override
    public boolean isSent(String monItemId, String userId) {
        return false;
//        return jdbcTemplate.queryForObject("select count(*) from MON_SENT_ITEMS where item = ? and user = ?",
//                Integer.class, monItemId, userId) > 0;
    }

}
