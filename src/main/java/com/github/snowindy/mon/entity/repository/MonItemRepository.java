/**
 * 
 */
package com.github.snowindy.mon.entity.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.github.snowindy.mon.entity.House;
import com.github.snowindy.mon.entity.StoredMonItem;

public interface MonItemRepository extends PagingAndSortingRepository<StoredMonItem, Long> {

    @Modifying
    @Query("delete from StoredMonItem m where m.dateDetected < ?1")
    void deleteAllBefore(Date barrierDate);

    @Query("select count(*) from StoredMonItem m where m.dateDetected > ?2 and m.houseTopic.house = ?1")
    long countForHouseAfterDate(House house, Date barrier);

    @Query("select count(*) from StoredMonItem m where m.url = ?1")
    long countByUrl(String url);

    StoredMonItem findOneByUuid(String uuid);
}