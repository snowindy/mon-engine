package com.github.snowindy.mon.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.jpa.domain.AbstractPersistable;

/**
 * A specific job category, the source of new jobs.
 * 
 */
@Entity
@Table(name = "HOUSE_TOPICS")
public class HouseTopic extends AbstractPersistable<Long> {
    private static final long serialVersionUID = 6187433326659764969L;

    @Column(nullable = false, unique = true)
    private String uuid;
    
    // 'Web Development', 'Web Design' etc.
    @Column(nullable = false)
    private String title;

    @ManyToOne
    private House house;
    @Column(nullable = false)
    private String url;

    @Column(nullable = false)
    private boolean active = true;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String topicUrl) {
        this.url = topicUrl;
    }

    @Deprecated
    public static Builder getBuilder(String title) {
        return new Builder(title);
    }

    public boolean isActive() {
        return active;
    }

    public final static class Builder {
        HouseTopic built;

        private String title;
        private House house;
        private String url;
        private boolean active = true;

        public Builder(String title) {
            this.title = title;
        }

        public final Builder title(String title) {
            this.title = title;
            return this;
        }

        public final Builder url(String url) {
            this.url = url;
            return this;
        }

        public final Builder house(House house) {
            this.house = house;
            return this;
        }

        public final Builder active(boolean active) {
            this.active = active;
            return this;
        }

        public HouseTopic build() {
            built = new HouseTopic();
            built.title = title;
            built.house = house;
            built.url = url;
            built.active = active;
            return built;

        }

    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
