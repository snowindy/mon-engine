package com.github.snowindy.mon.entity.enums;

public enum ENotificationType {
    USER_CREATED_EMAIL, USER_RESET_PASSWORD
}
