package com.github.snowindy.mon.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.springframework.data.jpa.domain.AbstractPersistable;

import com.github.snowindy.util.text.JsonUtils;

@Entity
@Table(name = "MON_ITEMS")
public class StoredMonItem extends AbstractPersistable<Long> {
    private static final long serialVersionUID = 6660071164890215163L;

    @Column(nullable = false, unique = true)
    private String uuid = UUID.randomUUID().toString();

    @Column(nullable = false)
    private String url;
    @Column(nullable = false)
    private String title;

    @Transient
    private Map<String, String> metadata;

    @Column(
    // TODO rename to 'metadata'
            name = "ATTACHMENTS_TEXT")
    private String metadataJson;

    @ManyToOne
    private HouseTopic houseTopic;
    @Column
    private String textBody;
    @Column(nullable = false)
    Date dateDetected = new Date();

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public HouseTopic getHouseTopic() {
        return houseTopic;
    }

    public void setHouseTopic(HouseTopic houseTopic) {
        this.houseTopic = houseTopic;
    }

    public StoredMonItem(MonItem monItem) {
        this();
        this.setTitle(monItem.getTitle());
        this.setUrl(monItem.getUrl());
        this.setMetadata(monItem.getMetadata());
        this.setTextBody(monItem.getTextBody());
    }

    public StoredMonItem() {
    }

    public String getTextBody() {
        return textBody;
    }

    public void setTextBody(String textBody) {
        this.textBody = textBody;
    }

    public String getFullText() {
        return title + " " + textBody;
    }

    public Date getDateDetected() {
        return dateDetected;
    }

    public void setDateDetected(Date date) {
        this.dateDetected = date;
    }

    @SuppressWarnings("unchecked")
    public Map<String, String> getMetadata() {
        if (metadataJson != null) {
            metadata = JsonUtils.fromJson(metadataJson, Map.class);
        }
        return metadata;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
        metadataJson = JsonUtils.toJson(metadata != null ? metadata : Collections.emptyMap());
    }

    @Override
    public String toString() {
        return String.format("MonItem ['%s', '%s', detected '%s'", title, url,
                FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss,SSS").format(dateDetected));
    }

    public String getClientInfo() {
        return getByPrefix("CLIENT");
    }

    public String getJobInfo() {
        return getByPrefix("JOB");
    }

    private String getByPrefix(String prefix) {
        List<String> items = new ArrayList<String>();
        for (Map.Entry<String, String> entry : metadata.entrySet()) {
            MetadataKeys k = MetadataKeys.valueOf(entry.getKey());
            if (k.name().startsWith(prefix)) {
                if (StringUtils.isNoneBlank(entry.getValue())) {
                    items.add(String.format("%s: %s", MetadataKeys.valueOf(entry.getKey()).getText(), entry.getValue()));
                }
            }
        }
        Collections.sort(items);
        return StringUtils.join(items, "\n");
    }

    public String getPlainTextForm() {

        return "Title: " + title + "\n\n-------------------------------------\n\n" + textBody
                + "\n\n-------------------------------------\n\n" + getJobInfo()
                + "\n\n-------------------------------------\n\n" + getClientInfo();
    }

}
