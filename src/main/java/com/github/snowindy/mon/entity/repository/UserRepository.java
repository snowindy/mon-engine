package com.github.snowindy.mon.entity.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.github.snowindy.mon.entity.House;
import com.github.snowindy.mon.entity.User;

/**
 * {@link PagingAndSortingRepository} to access {@link User} instances.
 * 
 * @author eg
 */
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    @Query("from User u where ?1 in elements(u.housesOfInterest) and u.active = true and (u.noSearchTill = null or u.noSearchTill <= ?2)")
    public List<User> findAllByHouseWithNoSearchDateBefore(House house, Date inactiveBarrier);

    public User findOneByEmail(String email);
}