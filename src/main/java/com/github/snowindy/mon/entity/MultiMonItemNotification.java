package com.github.snowindy.mon.entity;

import java.util.List;

/**
 * Short-living abstraction for monitored items notifications bundled together.
 * 
 */
public class MultiMonItemNotification {
    private List<MonItemNotification> notifications;
    private User user;

    public MultiMonItemNotification(User user, List<MonItemNotification> notifications) {
        this.user = user;
        this.notifications = notifications;
    }

    public List<MonItemNotification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<MonItemNotification> notifications) {
        this.notifications = notifications;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " [user=" + user + ", size="
                + (notifications != null ? notifications.size() : "0") + "]";
    }

}
