package com.github.snowindy.mon.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.jpa.domain.AbstractPersistable;

/**
 * 
 * odesk.com, freelance.ru etc.
 * 
 */
@Entity
@Table(name = "HOUSES")
public class House extends AbstractPersistable<Long> {
    private static final long serialVersionUID = -4348692138840907785L;

    @Column(nullable = false, unique = true)
    private String uuid;
    
    @Column(nullable = false, unique = true)
    private String domainName;
    @Column(nullable = false)
    private String urlPrefix;
    @Column(nullable = false)
    private boolean active = true;

    public String getDomainName() {
        return domainName;
    }

    public String getUrlPrefix() {
        return urlPrefix;
    }

    public boolean isActive() {
        return active;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public void setUrlPrefix(String urlPrefix) {
        this.urlPrefix = urlPrefix;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Deprecated
    public static Builder getBuilder(String domainName) {
        return new Builder(domainName);
    }

    public final static class Builder {
        private House built;

        private String domainName;
        private String urlPrefix;
        private boolean active = true;

        public Builder(String domainName) {
            this.domainName = domainName;
        }

        public final Builder urlPrefix(String urlPrefix) {
            this.urlPrefix = urlPrefix;
            return this;
        }

        public final Builder domainName(String domainName) {
            this.domainName = domainName;
            return this;
        }

        public final Builder active(boolean active) {
            this.active = active;
            return this;
        }

        public House build() {
            built = new House();
            built.domainName = domainName;
            built.urlPrefix = urlPrefix;
            built.active = active;
            return built;
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
