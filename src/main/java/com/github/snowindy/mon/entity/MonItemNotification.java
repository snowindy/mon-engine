package com.github.snowindy.mon.entity;

/**
 * Short-living abstraction for monitored items notifications. Currenly should
 * not be persistent.
 * 
 */
public class MonItemNotification {
    private User user;
    private StoredMonItem job;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public StoredMonItem getJob() {
        return job;
    }

    public void setJob(StoredMonItem job) {
        this.job = job;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " [user=" + user + ", job=" + job + "]";
    }

}
