package com.github.snowindy.mon.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.AbstractPersistable;

import com.github.snowindy.mon.entity.Query.QueryGroup;
import com.github.snowindy.mon.entity.enums.EAggregationPolicy;
import com.github.snowindy.mon.matcher.AndMatcher;
import com.github.snowindy.mon.matcher.ContainsAllPhraseMatcher;
import com.github.snowindy.mon.matcher.ContainsAnyPhraseMatcher;
import com.github.snowindy.mon.matcher.IMatcher;
import com.github.snowindy.mon.matcher.MatcherSerializer;
import com.github.snowindy.mon.matcher.NotMatcher;
import com.github.snowindy.mon.matcher.OrMatcher;
import com.github.snowindy.util.text.JsonUtils;

@Entity
@Table(name = "USERS")
public class User extends AbstractPersistable<Long> {
    private static final long serialVersionUID = -3254889221988115046L;

    @Column(nullable = false, unique = true)
    private String uuid;

    @Column(nullable = false)
    private String password;

    @Transient
    @Deprecated
    private Query query;

    @Transient
    private IMatcher matcher;

    @Column(nullable = false, name = "query", columnDefinition = "text")
    private String queryAsString;

    public String getQueryAsString() {
        return queryAsString;
    }

    private static MatcherSerializer serializer = new MatcherSerializer();

    public IMatcher getMatcher() {
        if (matcher == null) {
            matcher = serializer.deserializeJson(queryAsString);
        }
        return matcher;
    }

    private final static Logger logger = LoggerFactory.getLogger(User.class);

    public boolean migrateIfNeeded() {
        String queryInit = queryAsString;
        String output = getMigratedQueryContent(queryInit);
        queryAsString = output != null ? output : queryAsString;
        boolean migrate = output != null;
        if (migrate) {
            logger.info(String.format("Migrated user '%s': old content: '%s', new content: '%s'.", email, queryInit,
                    queryAsString));
        }
        return migrate;
    }

    protected static String getMigratedQueryContent(String input) {
        Map<String, Object> res = JsonUtils.fromJsonToMapOfObject(input);
        boolean oldVersion = res.get("groups") != null;

        String output = null;
        if (oldVersion) {
            Map<String, Object> map = new HashMap<>();

            map.put("type", "and");
            List<Object> rootAndItems = new ArrayList<>();
            map.put("items", rootAndItems);

            Query query = JsonUtils.fromJson(input, Query.class);

            if (query.getGroups().size() == 1) {
                convertGroup(rootAndItems, query.getGroups().get(0));
            } else {
                List<Object> orItems = new ArrayList<>();
                for (QueryGroup group : query.getGroups()) {
                    List<Object> items = new ArrayList<Object>();
                    convertGroup(items, group);
                    orItems.add(new AndMatcher(items.toArray()));
                }
                rootAndItems.add(new OrMatcher(orItems.toArray()));
            }

            BigDecimal val = query.getMinRatePerHour();
            if (val != null) {
                String tmpl = "{\"type\":\"or\",\"items\":[{\"type\":\"lineStartsWithSubstringFirstNumberGTE\","
                        + "\"items\":[\"%s:\",\"%s\"]},{\"type\":\"not\",\"items\":"
                        + "[{\"type\":\"lineStartsWithSubstring\",\"items\":[\"%s:\"]}]}]}";
                Map<String, Object> m = JsonUtils.fromJsonToMapOfObject(String.format(tmpl,
                        MetadataKeys.JOB_FORMATTED_MIN_RATE.getText(), val.toString(),
                        MetadataKeys.JOB_FORMATTED_MIN_RATE.getText()));
                rootAndItems.add(m);
            }

            val = query.getMinProjectBudget();
            if (val != null) {
                String tmpl = "{\"type\":\"or\",\"items\":[{\"type\":\"lineStartsWithSubstringFirstNumberGTE\","
                        + "\"items\":[\"%s:\",\"%s\"]},{\"type\":\"not\",\"items\":"
                        + "[{\"type\":\"lineStartsWithSubstring\",\"items\":[\"%s:\"]}]}]}";
                Map<String, Object> m = JsonUtils.fromJsonToMapOfObject(String.format(tmpl,
                        MetadataKeys.JOB_FORMATTED_MIN_BUDGET.getText(), val.toString(),
                        MetadataKeys.JOB_FORMATTED_MIN_BUDGET.getText()));
                rootAndItems.add(m);
            }

            output = JsonUtils.toJson(map);
        }
        return output;
    }

    private static void convertGroup(List<Object> addingTo, QueryGroup group) {
        if (group.getAll().size() > 0) {
            addingTo.add(new ContainsAllPhraseMatcher(sanitizeWordList(group.getAll())));
        }
        if (group.getAny().size() > 0) {
            addingTo.add(new ContainsAnyPhraseMatcher(sanitizeWordList(group.getAny())));
        }
        if (group.getStop().size() > 0) {
            addingTo.add(new NotMatcher(new ContainsAnyPhraseMatcher(sanitizeWordList(group.getStop()))));
        }
    }

    private static Object[] sanitizeWordList(List<String> list) {
        List<String> res = new ArrayList<String>();
        for (String val : list) {
            val = StringUtils.removeStart(val, "\"");
            val = StringUtils.removeStart(val, "'");
            val = StringUtils.removeEnd(val, "\"");
            val = StringUtils.removeEnd(val, "'");
            res.add(val);
        }
        return res.toArray();
    }

    public void setQueryAsString(String json) {
        this.queryAsString = json;
    }

    private Date noSearchTill;

    private boolean active = true;

    @ElementCollection
    private List<String> anyKeywords = Collections.emptyList();
    @ElementCollection
    private List<String> allKeywords = Collections.emptyList();
    @ElementCollection
    private List<String> stopKeywords = Collections.emptyList();

    @ManyToMany
    private List<House> housesOfInterest;

    @Column(nullable = false, unique = true)
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "NTF_AGGREGATION", nullable = false)
    private EAggregationPolicy aggregationPolicy;

    public EAggregationPolicy getAggregationPolicy() {
        return aggregationPolicy;
    }

    public void setAggregationPolicy(EAggregationPolicy aggregationPolicy) {
        this.aggregationPolicy = aggregationPolicy;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public List<String> getAnyKeywords() {
        return anyKeywords;
    }

    public List<String> getAllKeywords() {
        return allKeywords;
    }

    public List<String> getStopKeywords() {
        return stopKeywords;
    }

    public String getEmail() {
        return email;
    }

    public boolean isActive() {
        return active;
    }

    public List<House> getHousesOfInterest() {
        if (housesOfInterest == null) {
            housesOfInterest = Collections.emptyList();
        }
        return housesOfInterest;
    }

    public Date getNoSearchTill() {
        return noSearchTill;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Gets a builder which is used to create {@link User} domain object
     * 
     * @param name
     *            - user name
     * @param password
     *            - entered password. This field should be encrypted
     * @return A new builder instance
     * @deprecated
     */
    public static Builder getBuilder(String email) {
        return new Builder(email);
    }

    // Builder pattern has implemented
    public static final class Builder {
        User built;

        private Long id;
        private String name;
        private String password;
        private String email;
        private Date noSearchTill;
        private boolean active;
        private List<String> anyKeywords = Collections.emptyList();;
        private List<String> allKeywords = Collections.emptyList();;
        private List<String> stopKeywords = Collections.emptyList();;
        private List<House> housesOfInterest;

        public Builder(String email) {
            this.email = email;
            this.active = Boolean.TRUE;
            this.noSearchTill = new Date(0);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder noSearchTill(Date noSearchTill) {
            this.noSearchTill = noSearchTill;
            return this;
        }

        public Builder active(boolean active) {
            this.active = active;
            return this;
        }

        public Builder anyKeywords(List<String> anyKeywords) {
            // Defencive copy
            this.anyKeywords = new ArrayList<String>(anyKeywords);
            return this;
        }

        public Builder allKeywords(List<String> allKeywords) {
            // Defencive copy
            this.allKeywords = new ArrayList<String>(allKeywords);
            return this;
        }

        public Builder stopKeywords(List<String> stopKeywords) {
            // Defencive copy
            this.stopKeywords = new ArrayList<String>(stopKeywords);
            return this;
        }

        public Builder housesOfInterest(List<House> housesOfInterest) {
            // Defencive copy
            this.housesOfInterest = new ArrayList<House>(housesOfInterest);
            return this;
        }

        public User build() {
            built = new User();
            built.setId(id == null ? null : id);
            built.email = email;
            built.password = password;
            built.noSearchTill = noSearchTill;
            built.active = active;
            built.allKeywords = allKeywords;
            built.anyKeywords = anyKeywords;
            built.stopKeywords = stopKeywords;
            built.housesOfInterest = housesOfInterest;
            return built;
        }
    }

    @Transient
    public final static Builder getBuilderFromUser(User user) {
        User.Builder userBuilder = User.getBuilder(user.getEmail());
        userBuilder.password(user.getPassword()).active(user.isActive()).noSearchTill(user.getNoSearchTill())
                .allKeywords(user.getAllKeywords()).anyKeywords(user.getAnyKeywords())
                .stopKeywords(user.getStopKeywords()).housesOfInterest(user.getHousesOfInterest()).id(user.getId());
        return userBuilder;
    }

    /**
     * @return {@link String} representation of {@link User} object
     */
    @Transient
    public String getFullUserDescription() {
        return ToStringBuilder.reflectionToString(this);
    }

    public void setPassword(String password2) {
        this.password = password2;
    }

    public void setNoSearchTill(Date noSearchTill) {
        this.noSearchTill = noSearchTill;
    }

    public void setAnyKeywords(List<String> anyKeywords) {
        this.anyKeywords = anyKeywords;
    }

    public void setAllKeywords(List<String> allKeywords) {
        this.allKeywords = allKeywords;
    }

    public void setStopKeywords(List<String> stopKeywords) {
        this.stopKeywords = stopKeywords;
    }

    public void setHousesOfInterest(List<House> housesOfInterest) {
        this.housesOfInterest = housesOfInterest;
    }

    @Override
    public String toString() {
        return String.format("User ('%s', '%s', '%s')", email, uuid, query);
    }
}