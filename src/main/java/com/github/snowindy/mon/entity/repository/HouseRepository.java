/**
 * 
 */
package com.github.snowindy.mon.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.github.snowindy.mon.entity.House;

/**
 * {@link PagingAndSortingRepository} to access {@link House} instances.
 * 
 * @author Anton Novopashin
 */
public interface HouseRepository extends PagingAndSortingRepository<House, Long> {

    List<House> findAllByActive(boolean active);
    
	House findOneByDomainName(String domainName);

	@Modifying
	@Query("delete from House u where u.domainName = ?1")
	void deleteByDomainName(String domainName);
}