package com.github.snowindy.mon;

import java.util.Collection;

import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.lang3.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.github.snowindy.mon.beans.BeanNameUtils;
import com.github.snowindy.mon.beans.EnrichmentRouter;
import com.github.snowindy.mon.beans.HouseSource;
import com.github.snowindy.mon.beans.HouseTopicRouter;
import com.github.snowindy.mon.beans.HouseTopicsSource;
import com.github.snowindy.mon.beans.MultiMonItemEmailNotifier;
import com.github.snowindy.mon.beans.NewMonItemEnrichedHandler;
import com.github.snowindy.mon.beans.NotificationDestinationFilter;
import com.github.snowindy.mon.beans.NotificationsThresholdPeriodHandler;
import com.github.snowindy.mon.beans.SingleMonItemEmailNotifier;
import com.github.snowindy.mon.beans.UserMonItemMatcher;
import com.github.snowindy.mon.entity.House;
import com.github.snowindy.mon.entity.repository.HouseRepository;
import com.github.snowindy.mon.entity.repository.HouseTopicRepository;
import com.github.snowindy.mon.i.INotificationsThresholdPeriodHandler;
import com.github.snowindy.util.camel.CamelUtils;
import com.github.snowindy.util.camel.ListSplitter;
import com.propzmedia.util.spring.SpringUtils;

public class MonitoringRouteBuilder extends RouteBuilder {

    public MonitoringRouteBuilder() {
        log.info("\n\n---------------------------------------------------------------------------\n"
                + "Starting monitoring engine core module."
                + "\n---------------------------------------------------------------------------\n");
    }

    public static final String BEAN_NEW_MON_ITEM_FILTER = "newMonItemsFilter";
    public static final String BEAN_HOUSE_SOURCE = WordUtils.uncapitalize(HouseSource.class.getSimpleName());
    public static final String BEAN_NOTIFIER_SINGLE = WordUtils.uncapitalize(SingleMonItemEmailNotifier.class
            .getSimpleName());
    public static final String BEAN_NOTIFIER_MULTI = WordUtils.uncapitalize(MultiMonItemEmailNotifier.class
            .getSimpleName());
    public static final String BEAN_MON_ITEM_USER_MATCHER = WordUtils.uncapitalize(UserMonItemMatcher.class
            .getSimpleName());
    public static final String BEAN_HOUSE_TOPICS_SOURCE = WordUtils.uncapitalize(HouseTopicsSource.class
            .getSimpleName());
    public static final String BEAN_NEW_MON_ITEM_ENRICHED_HANDLER = WordUtils
            .uncapitalize(NewMonItemEnrichedHandler.class.getSimpleName());
    public static final String BEAN_NOTIFICATION_THRESHOLD_PERIOD_HANDLER = WordUtils
            .uncapitalize(NotificationsThresholdPeriodHandler.class.getSimpleName());
    public static final String BEAN_NOTIFICATION_DESTINATION_FILTER = WordUtils
            .uncapitalize(NotificationDestinationFilter.class.getSimpleName());

    public static final String[] REQUIRED_BEANS = { BEAN_HOUSE_SOURCE, BEAN_NEW_MON_ITEM_FILTER, BEAN_NOTIFIER_SINGLE,
            BEAN_NOTIFIER_MULTI, BEAN_MON_ITEM_USER_MATCHER, BEAN_HOUSE_TOPICS_SOURCE,
            BEAN_NEW_MON_ITEM_ENRICHED_HANDLER, BEAN_NOTIFICATION_THRESHOLD_PERIOD_HANDLER,
            BEAN_NOTIFICATION_DESTINATION_FILTER };

    @Autowired
    private HouseRepository houseRepository;

    @Autowired
    private HouseTopicRepository houseTopicRepository;

    public static final String QUEUE_HOUSES = "seda:queue:houses";
    public static final String QUEUE_ITEMS_UNFILTERED = "seda:queue:monItemsUnfiltered";
    public static final String QUEUE_NEW_ITEMS_NOT_ENRICHED = "seda:queue:newMonItemsNotEnriched";
    public static final String QUEUE_NEW_ITEMS_ENRICHED = "seda:queue:newMonItemsEnriched";
    public static final String QUEUE_NOTIFICATIONS = "seda:queue:notifications";
    public static final String QUEUE_NOTIFICATIONS_SINGLE = "seda:queue:notificationsSingle";
    public static final String QUEUE_NOTIFICATIONS_MULTI = "seda:queue:notificationsMulti";

    public static final String BEAN_NAME_SUFFIX_MON_ITEM_SOURCE = "_monItemSource";
    public static final String BEAN_NAME_SUFFIX_MON_ITEM_ENRICHER = "_monItemEnricher";

    public static final String DIRECT_ALL_NEW_ITEMS = "direct:allNewItems";
    public static final String DIRECT_AFTER_ENRICHMENT = "direct:afterEnrichment";

    private static final String ROUTE_ID_PREFIX = "EngineCore: ";

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ITransactionalWrapper transactionalWrapper;

    @Override
    public void configure() {
        /**
         * This is because camel is lazy in binding beans. We want to be sure
         * that when started, app contains all beans defined coorectly.
         */
        SpringUtils.checkRequiredBeans(applicationContext, REQUIRED_BEANS);

        transactionalWrapper.withTransaction(new Runnable() {

            @Override
            public void run() {
                // Getting configuration-time houses
                Collection<House> confHouses = houseRepository.findAllByActive(true);

                // Removing nulls passed to routes
               // intercept().when(body().isNull()).stop();
                
                // Getting runtime houses
                from(SchedulerRouteBuilder.TOPIC_MAIN_TIMER_EVENT)
                        .to(CamelUtils.toBean(BEAN_HOUSE_SOURCE, "getActiveHouses")).split().method(ListSplitter.class)
                        .to(QUEUE_HOUSES).routeId(ROUTE_ID_PREFIX + "timer");

                // Getting active topics for selected houses
                from(QUEUE_HOUSES + "?multipleConsumers=true")
                        .to(CamelUtils.toBean(BEAN_HOUSE_TOPICS_SOURCE, "getTopics")).split()
                        .method(ListSplitter.class).recipientList().method(HouseTopicRouter.class)
                        .routeId(ROUTE_ID_PREFIX + "house topics");

                // For each topic getting all listed monItems
                for (House house : confHouses) {
                    from(HouseTopicRouter.resolveHouseTopicChannel(house) + "?concurrentConsumers=5")
                            .to("bean:"
                                    + BeanNameUtils.getBeanNameFromDomainName(house.getDomainName(),
                                            BEAN_NAME_SUFFIX_MON_ITEM_SOURCE)).split().method(ListSplitter.class)
                            .to(QUEUE_ITEMS_UNFILTERED)
                            .routeId(ROUTE_ID_PREFIX + "items-unfiltered: " + house.getDomainName());
                }

                // Filtering only new items (the ones engine has not seen yet)
                from(QUEUE_ITEMS_UNFILTERED).filter().method(BEAN_NEW_MON_ITEM_FILTER, "isNew")
                        .to(QUEUE_NEW_ITEMS_NOT_ENRICHED).routeId(ROUTE_ID_PREFIX + "items-filter");

                // Sending new items to enrichment step
                from(QUEUE_NEW_ITEMS_NOT_ENRICHED).recipientList().method(EnrichmentRouter.class)
                        .routeId(ROUTE_ID_PREFIX + "enrichment router");

                // Enriching items
                for (House house : confHouses) {
                    from(EnrichmentRouter.resolveEnrichmentChannel(house) + "?concurrentConsumers=5")
                            .to("bean:"
                                    + BeanNameUtils.getBeanNameFromDomainName(house.getDomainName(),
                                            BEAN_NAME_SUFFIX_MON_ITEM_ENRICHER)).to(QUEUE_NEW_ITEMS_ENRICHED)
                            .routeId(ROUTE_ID_PREFIX + "items-enriched: " + house.getDomainName());
                }

                // Matching items to users, if a match found - notification
                // object will
                // be issued
                from(QUEUE_NEW_ITEMS_ENRICHED).multicast().parallelProcessing()
                        .to(DIRECT_AFTER_ENRICHMENT, DIRECT_ALL_NEW_ITEMS)
                        .routeId(ROUTE_ID_PREFIX + "items-enriched multicast");

                {
                    from(DIRECT_AFTER_ENRICHMENT).to(CamelUtils.toBean(BEAN_MON_ITEM_USER_MATCHER)).split()
                            .method(ListSplitter.class).filter().method(BEAN_NOTIFICATION_DESTINATION_FILTER)
                            .to(QUEUE_NOTIFICATIONS).routeId(ROUTE_ID_PREFIX + "item matcher");
                    from(DIRECT_ALL_NEW_ITEMS).to(CamelUtils.toBean(BEAN_NEW_MON_ITEM_ENRICHED_HANDLER)).routeId(
                            ROUTE_ID_PREFIX + "new items handler");
                }

                // Sending notification to notification processing steps
                from(QUEUE_NOTIFICATIONS).to(
                        CamelUtils.toBean(BEAN_NOTIFICATION_THRESHOLD_PERIOD_HANDLER,
                                INotificationsThresholdPeriodHandler.METHOD_NAME_PROCESS)).routeId(
                        ROUTE_ID_PREFIX + "Notifications limits hanler");

                // Notifications processing
                {
                    from(QUEUE_NOTIFICATIONS_SINGLE).to(CamelUtils.toBean(BEAN_NOTIFIER_SINGLE)).routeId(
                            ROUTE_ID_PREFIX + "sigle notifier");
                    from(QUEUE_NOTIFICATIONS_MULTI).to(CamelUtils.toBean(BEAN_NOTIFIER_MULTI)).routeId(
                            ROUTE_ID_PREFIX + "multi notifier");
                }
            }
        });

    }

}
