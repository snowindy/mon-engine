package com.github.snowindy.mon;

import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.snowindy.spring.util.IPropertiesHolder;

public class SchedulerRouteBuilder extends RouteBuilder {

    @Autowired
    private IPropertiesHolder propertiesHolder;

    public static final String TOPIC_MAIN_TIMER_EVENT = "seda:topic:mainTimer";
    public static final String TOPIC_NOTIFICATION_TIMER_EVENT = "seda:topic:notificationTimer";
    public static final String TOPIC_CLEANUP_SHORT = "seda:topic:cleanupShortTimer";
    public static final String TOPIC_CLEANUP_MEDIUM = "seda:topic:cleanupMediumTimer";
    public static final String TOPIC_CLEANUP_LONG = "seda:topic:cleanupLongTimer";
    public static final String TOPIC_NOTIFICATION_MAX_IN_PERIOD = "seda:topic:notificationMaxInPeriod";
    public static final String TOPIC_NOTIFICATION_AGGREGATION_FLUSH = "seda:topic:notificationAggregationFlush";

    private static final String ROUTE_ID_PREFIX = "Timers: ";

    @Override
    public void configure() {
        try {
            PeriodConfig pc = getPeriodConfig("engine.timer.main", 60000);
            from("timer:mon-engine-main-timer?period=" + pc.period + "&fixedRate=true&delay=" + pc.delay).to(
                    TOPIC_MAIN_TIMER_EVENT).routeId(ROUTE_ID_PREFIX + TOPIC_MAIN_TIMER_EVENT);

            pc = getPeriodConfig("engine.timer.notificationDurable", 60000);
            from("timer:mon-engine-durable-notify-timer?period=" + pc.period + "&fixedRate=true&delay=" + pc.delay).to(
                    TOPIC_NOTIFICATION_TIMER_EVENT).routeId(ROUTE_ID_PREFIX + TOPIC_NOTIFICATION_TIMER_EVENT);

            int period = 60000;
            pc = getPeriodConfig("engine.timer.cleanupShortTimer", period, period);
            from("timer:mon-engine-cleanup-short-timer?period=" + pc.period + "&fixedRate=true&delay=" + pc.delay).to(
                    TOPIC_CLEANUP_SHORT).routeId(ROUTE_ID_PREFIX + TOPIC_CLEANUP_SHORT);

            period = 60000 * 60;
            pc = getPeriodConfig("engine.timer.cleanupMediumTimer", period, period);
            from("timer:mon-engine-cleanup-medium-timer?period=" + pc.period + "&fixedRate=true&delay=" + pc.delay).to(
                    TOPIC_CLEANUP_MEDIUM).routeId(ROUTE_ID_PREFIX + TOPIC_CLEANUP_MEDIUM);

            period = 60000 * 60 * 24;
            pc = getPeriodConfig("engine.timer.cleanupLongTimer", period, period);
            from("timer:mon-engine-cleanup-long-timer?period=" + pc.period + "&fixedRate=true&delay=" + pc.delay).to(
                    TOPIC_CLEANUP_LONG).routeId(ROUTE_ID_PREFIX + TOPIC_CLEANUP_LONG);

            pc = getPeriodConfig("notify.immediate.maxInPeriod.cleanup", 3600000);
            from("timer:mon-engine-max-in-period-clean-timer?period=" + pc.period + "&fixedRate=true&delay=" + pc.delay)
                    .to(TOPIC_NOTIFICATION_MAX_IN_PERIOD).routeId(ROUTE_ID_PREFIX + TOPIC_NOTIFICATION_MAX_IN_PERIOD);

            pc = getPeriodConfig("notify.aggregation.flush", 3600000);
            from("timer:mon-engine-notification-aggr-flush?period=" + pc.period + "&fixedRate=true&delay=" + pc.delay)
                    .to(TOPIC_NOTIFICATION_AGGREGATION_FLUSH).routeId(
                            ROUTE_ID_PREFIX + TOPIC_NOTIFICATION_AGGREGATION_FLUSH);
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    private final static Logger logger = LoggerFactory.getLogger(SchedulerRouteBuilder.class);

    public void setPropertiesHolder(IPropertiesHolder propertiesHolder) {
        this.propertiesHolder = propertiesHolder;
    }

    private PeriodConfig getPeriodConfig(String timerPrefix, int defaultPeriod, int defaultDelay) {
        int period = propertiesHolder.getConfig().getInt(timerPrefix + ".period.ms", defaultPeriod);
        int delay = propertiesHolder.getConfig().getInt(timerPrefix + ".initialDelay.ms", defaultDelay);
        PeriodConfig pc = new PeriodConfig();
        pc.period = period;
        pc.delay = delay;

        return pc;
    }

    private PeriodConfig getPeriodConfig(String timerPrefix, int defaultPeriod) {
        return getPeriodConfig(timerPrefix, defaultPeriod, 1000);
    }

    class PeriodConfig {
        int period, delay;
    }
}
