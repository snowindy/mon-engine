package com.github.snowindy.mon.matcher;

public class OrMatcher extends AbstractMatcher implements IContainerMatcher {

    public OrMatcher() {

    }

    public OrMatcher(Object... items) {
        super(items);
    }

    @Override
    public boolean matches(String content) {
        boolean res = false;
        for (Object matcher : items) {
            res = res || ((AbstractMatcher) matcher).matches(content);
            if (res) {
                break;
            }
        }
        return res;
    }
}
