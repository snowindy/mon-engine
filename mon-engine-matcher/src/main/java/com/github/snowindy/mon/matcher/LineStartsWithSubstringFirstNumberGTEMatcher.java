package com.github.snowindy.mon.matcher;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LineStartsWithSubstringFirstNumberGTEMatcher extends LineStartsWithSubstringMatcher {

    public LineStartsWithSubstringFirstNumberGTEMatcher() {

    }

    public LineStartsWithSubstringFirstNumberGTEMatcher(Object... items) {
        super(items);
    }

    static Pattern pattern = Pattern.compile("\\d+(\\.\\d+)?");

    BigDecimal compareTo;

    private void prepare() {
        if (compareTo == null) {
            List<String> kw = new ArrayList<>();
            for (Object object : items) {
                kw.add(object.toString());
            }
            compareTo = new BigDecimal(kw.get(1));
        }
    }

    @Override
    protected boolean additionalCheck(String line) {
        prepare();
        Matcher m = pattern.matcher(line);
        while (m.find()) {
            BigDecimal number = new BigDecimal(m.group(0));
            if (number.compareTo(compareTo) >= 0) {
                return true;
            }
        }
        return false;
    }

}
