package com.github.snowindy.mon.matcher;


public interface IMatcher {
    boolean matches(String content);
}
