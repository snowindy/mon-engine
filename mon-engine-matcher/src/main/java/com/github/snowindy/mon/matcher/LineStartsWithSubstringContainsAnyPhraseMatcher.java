package com.github.snowindy.mon.matcher;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.github.snowindy.util.WordMatchUtils;

public class LineStartsWithSubstringContainsAnyPhraseMatcher extends LineStartsWithSubstringMatcher {

    public LineStartsWithSubstringContainsAnyPhraseMatcher() {

    }

    public LineStartsWithSubstringContainsAnyPhraseMatcher(Object... items) {
        super(items);
    }

    @Override
    public boolean matches(String content) {
        String[] arr = StringUtils.split(content, "\n");
        boolean res = false;
        for (String line : arr) {
            res = super.matches(line);
            if (res) {
                break;
            }
        }
        return res;
    }

    private List<String> searchPhrases;

    private void prepare() {
        if (searchPhrases == null) {
            searchPhrases = new ArrayList<>();
            for (int i = 1; i < items.size(); i++) {
                searchPhrases.add(items.get(i).toString());
            }
        }
    }

    @Override
    protected boolean additionalCheck(String line) {
        prepare();
        return WordMatchUtils.containsAnyPhrases(searchPhrases, line);
    }

}
