package com.github.snowindy.mon.matcher;


public class NotMatcher extends AndMatcher {

    public NotMatcher() {

    }

    public NotMatcher(Object... items) {
        super(items);
    }

    @Override
    public boolean matches(String content) {
        return !super.matches(content);
    }

}
