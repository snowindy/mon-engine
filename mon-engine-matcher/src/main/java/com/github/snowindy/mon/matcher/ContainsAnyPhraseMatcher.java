package com.github.snowindy.mon.matcher;

import java.util.ArrayList;
import java.util.List;

import com.github.snowindy.util.WordMatchUtils;

public class ContainsAnyPhraseMatcher extends AbstractMatcher {

    public ContainsAnyPhraseMatcher() {

    }

    public ContainsAnyPhraseMatcher(Object... items) {
        super(items);
    }

    @Override
    public boolean matches(String content) {
        List<String> kw = new ArrayList<>();
        for (Object object : items) {
            kw.add(object.toString());
        }
        return WordMatchUtils.containsAnyPhrases(kw, content);
    }
}
