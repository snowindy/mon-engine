package com.github.snowindy.mon.matcher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractMatcher implements IMatcher {
    protected List<Object> items = new ArrayList<>();

    protected AbstractMatcher() {
    }

    protected AbstractMatcher(Object... items) {
        this.items = Arrays.asList(items);
    }

    
    public String getType() {
        return WordUtils.uncapitalize(StringUtils.removeEnd(this.getClass().getSimpleName(), "Matcher"));
    }

    @Override
    public abstract boolean matches(String content);

    public List<Object> getItems() {
        return items;
    }

    public void setItems(List<Object> items) {
        this.items = items;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((items == null) ? 0 : items.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractMatcher other = (AbstractMatcher) obj;
        if (items == null) {
            if (other.items != null)
                return false;
        } else if (!items.equals(other.items))
            return false;
        return true;
    }

}
