package com.github.snowindy.mon.matcher;

import java.util.ArrayList;
import java.util.List;

import com.github.snowindy.util.WordMatchUtils;

public class ContainsAllPhraseMatcher extends AbstractMatcher {

    public ContainsAllPhraseMatcher() {

    }

    public ContainsAllPhraseMatcher(Object... items) {
        super(items);
    }
    
    @Override
    public boolean matches(String content) {
        List<String> kw = new ArrayList<>();
        for (Object object : items) {
            kw.add(object.toString());
        }
        return WordMatchUtils.containsAllPhrases(kw, content);
    }
}
