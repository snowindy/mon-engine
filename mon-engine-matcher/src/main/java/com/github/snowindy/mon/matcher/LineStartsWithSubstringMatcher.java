package com.github.snowindy.mon.matcher;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class LineStartsWithSubstringMatcher extends AbstractMatcher {

    public LineStartsWithSubstringMatcher() {

    }

    public LineStartsWithSubstringMatcher(Object... items) {
        super(items);
    }

    protected boolean additionalCheck(String line) {
        return true;
    }

    @Override
    public boolean matches(String content) {
        List<String> kw = new ArrayList<>();
        for (Object object : items) {
            kw.add(object.toString());
        }
        String lineMatchQualifier = kw.get(0).toLowerCase();

        String[] arr = StringUtils.split(content, "\n");

        boolean res = false;
        for (String line : arr) {
            line = line.toLowerCase();
            if (StringUtils.startsWithIgnoreCase(line, lineMatchQualifier)) {
                if (additionalCheck(line)) {
                    res = true;
                    break;
                }
            }
            if (res) {
                break;
            }
        }
        return res;
    }

}
