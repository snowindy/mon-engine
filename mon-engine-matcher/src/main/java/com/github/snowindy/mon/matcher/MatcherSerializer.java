package com.github.snowindy.mon.matcher;

import org.apache.commons.lang3.text.WordUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.github.snowindy.util.JsonUtils;

public class MatcherSerializer {

    public IMatcher deserializeJson(String json) {
        try {
            return deserializeJsonInner(json);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private IMatcher deserializeJsonInner(String json) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(json);
        String type = node.get("type").asText();

        AbstractMatcher res;

        @SuppressWarnings("unchecked")
        Class<AbstractMatcher> mClass = (Class<AbstractMatcher>) Class.forName(getClass().getPackage().getName() + "."
                + WordUtils.capitalize(type) + "Matcher");
        if (IContainerMatcher.class.isAssignableFrom(mClass)) {
            res = mClass.newInstance();
            for (int i = 0; i < node.get("items").size(); i++) {
                String json1 = mapper.writeValueAsString(node.get("items").get(i));
                res.getItems().add(deserializeJsonInner(json1));
            }
        } else {
            res = JsonUtils.fromJson(json, mClass);
        }

        return res;
    }

    public String serializeJson(IMatcher matcher) {
        return JsonUtils.toJson(matcher);
    }
}
