package com.github.snowindy.mon.matcher;


public class AndMatcher extends AbstractMatcher implements IContainerMatcher {

    public AndMatcher() {

    }

    public AndMatcher(Object... items) {
        super(items);
    }

    @Override
    public boolean matches(String content) {
        boolean res = true;
        for (Object matcher : items) {
            res = ((AbstractMatcher) matcher).matches(content);
            if (!res) {
                break;
            }
        }
        return res;
    }

}
