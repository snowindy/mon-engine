package com.github.snowindy.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities.EscapeMode;
import org.jsoup.nodes.TextNode;
import org.jsoup.safety.Cleaner;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

public class StringUtil {

    /**
     * http://stackoverflow.com/questions/5134959/convert-plain-text-to-html-
     * text-in -java
     * 
     */
    public static String convertTextToHtml(String s) {
        if (s == null) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        boolean previousWasASpace = false;
        for (char c : s.toCharArray()) {
            if (c == ' ') {
                if (previousWasASpace) {
                    builder.append("&nbsp;");
                    previousWasASpace = false;
                    continue;
                }
                previousWasASpace = true;
            } else {
                previousWasASpace = false;
            }
            switch (c) {
                case '<':
                    builder.append("&lt;");
                    break;
                case '>':
                    builder.append("&gt;");
                    break;
                case '&':
                    builder.append("&amp;");
                    break;
                case '"':
                    builder.append("&quot;");
                    break;
                case '\n':
                    builder.append("<br/>");
                    break;
                // We need Tab support here, because we print StackTraces as
                // HTML
                case '\t':
                    builder.append("&nbsp; &nbsp; &nbsp;");
                    break;
                default:
                    if (c < 128) {
                        builder.append(c);
                    } else {
                        builder.append("&#").append((int) c).append(";");
                    }
            }
        }
        return builder.toString();
    }

    public static String clenup(String s) {
        s = whitespaceClenup(s);
        s = s.replaceAll(" [,\\.]", ",");
        return s;
    }

    public static String whitespaceClenup(String s) {
        s = s.replace("\u00a0", " "); // removes &nbsp;
        s = s.replaceAll("\\s+", " ");
        return s;
    }

    private static Whitelist brWhitelist = new Whitelist().addTags("br");

    public static String escapeHtmlWithNewLineSupport(String source) {
        Document doc = Jsoup.parseBodyFragment(source);

        // replacing links with href attr
        Elements elements = doc.select("a");
        for (Element element : elements) {
            element.replaceWith(new TextNode(element.attr("href"), ""));
        }

        // adding newlines to p elements
        elements = doc.select("p");
        for (Element element : elements) {
            element.append("<br/>");
        }

        // adding newlines to li elements
        elements = doc.select("li");
        for (Element element : elements) {
            element.append("<br/>");
        }

        doc = new Cleaner(brWhitelist).clean(doc);

        doc.outputSettings().escapeMode(EscapeMode.xhtml);

        String withBrs = doc.body().html();

        return withBrs.replaceAll("\\n?<br\\s*/>", "\n");
    }
}
