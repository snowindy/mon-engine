package com.github.snowindy.util;

import java.util.*;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class WordMatchUtils {

    public static boolean containsAllPhrases(Collection<String> keywords, String text) {
        String[] wordsSplit = getWordsSplit(text);
        Set<String> wordsUnique = getWords(wordsSplit);

        if (keywords.isEmpty()) {
            return false;
        }
        String wordsWithSpacesBetween = null;

        Boolean foundAll = true;
        for (String keyword : keywords) {
            keyword = StringUtils.lowerCase(keyword);
            if (keyword.contains(" ")) {
                if (wordsWithSpacesBetween == null) {
                    // Lazy init for perfomance
                    wordsWithSpacesBetween = StringUtils.join(wordsSplit, " ");
                }
                if (!wordsWithSpacesBetween.contains(keyword)) {
                    foundAll = false;
                    break;
                }
            } else {
                if (!wordsUnique.contains(keyword)) {
                    foundAll = false;
                    break;
                }
            }
        }

        return foundAll;
    }

    private static Set<String> getWords(String[] words) {
        Set<String> wordsSet = new HashSet<String>();
        for (String word : words) {
            wordsSet.add(word);
        }

        return wordsSet;
    }

    private static String[] getWordsSplit(String text) {
        String textCleaned = StringUtil.clenup(text);
        textCleaned = StringUtils.lowerCase(textCleaned);

        String[] words = StringUtils.split(textCleaned, SPLIT_CHARS);
        return words;
    }

    // TODO more chars
    private static String SPLIT_CHARS = ", ()[]{}\"':.\n\t";

    public static boolean containsAnyPhrases(Collection<String> keywords, String text) {
        String[] wordsSplit = getWordsSplit(text);
        Set<String> wordsUnique = getWords(wordsSplit);
        String wordsWithSpacesBetween = null;

        boolean found = false;
        for (String keyword : keywords) {
            keyword = StringUtils.lowerCase(keyword);
            if (keyword.contains(" ")) {
                if (wordsWithSpacesBetween == null) {
                    // Lazy init for perfomance
                    wordsWithSpacesBetween = StringUtils.join(wordsSplit, " ");
                }
                if (wordsWithSpacesBetween.contains(keyword) || contains(keyword, Arrays.asList(wordsWithSpacesBetween))){
                    found = true;
                    break;
                }
            } else {
                if (contains(keyword, wordsUnique)) {
                    found = true;
                    break;
                }
            }
        }

        return found;
    }

    static boolean contains(String keyword, Collection<String> words){
        if (keyword.contains("*")){
            String[] kwParts = StringUtils.split(keyword, "*");
            for(String word: words){
                boolean innerContain = true;
                for(String kwPart: kwParts){
                    innerContain = innerContain && word.contains(kwPart);
                }
                if (innerContain){
                    return true;
                }
            }
            return false;
        }else{
            return words.contains(keyword);
        }
    }
}
