package com.github.snowindy.mon.matcher;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LineStartsWithSubstringContainsAnyPhraseMatcherTest {

    @Test
    public void test_matches() {
        LineStartsWithSubstringContainsAnyPhraseMatcher m = new LineStartsWithSubstringContainsAnyPhraseMatcher();
        m.setItems(new ArrayList<Object>(Arrays.asList("Category:", "General translation")));

        assertTrue(m.matches("Category: Translation / General Translation"));
        assertFalse(m.matches("Category: foobar"));
        assertFalse(m.matches("Category: Translation"));
        assertFalse(m.matches("Category1: Translation / General Translation"));
    }

    @Test
    public void test_matches1() {
        LineStartsWithSubstringContainsAnyPhraseMatcher m = new LineStartsWithSubstringContainsAnyPhraseMatcher();
        m.setItems(new ArrayList<Object>(Arrays.asList("Category:", "translation", "writing")));

        assertTrue(m.matches("Category: Translation"));
        assertTrue(m.matches("Category: writing"));
        assertFalse(m.matches("Category: other"));
    }

}
