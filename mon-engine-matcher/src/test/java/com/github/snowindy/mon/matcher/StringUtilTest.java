package com.github.snowindy.mon.matcher;

import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.util.StringUtil;

public class StringUtilTest extends Assert {
    @Test
    public void test_convertTextToHtml() {
        assertEquals("new &nbsp;java dev needed<br/>, urgent.",
                StringUtil.convertTextToHtml("new  java dev needed\n, urgent."));
        assertEquals("", StringUtil.convertTextToHtml(null));
    }

    @Test
    public void test_whitespaceClenup() {
        assertEquals("new java dev needed, urgent.", StringUtil.clenup("new  java dev \t needed\n, urgent."));
    }

    @Test
    public void test_escapeHtmlWithNewLineSupport() {
        assertEquals(
                "hello \n http://test.com world",
                StringUtil
                        .escapeHtmlWithNewLineSupport("hello <br/> <a href=\"http://test.com\">http://test.com</a> world"));

        assertEquals(
                "hello \n http://test.com «world»",
                StringUtil
                        .escapeHtmlWithNewLineSupport("hello <br/> <a href=\"http://test.com\">http://test.com</a> <strong>«world»</strong>"));

        assertEquals(
                "hello \n http://test.com\n «world»",
                StringUtil
                        .escapeHtmlWithNewLineSupport("<p>hello <br/> <a href=\"http://test.com\">http://test.com</a></p> <strong>«world»</strong>"));

        assertEquals("line1\nline2\n", StringUtil.escapeHtmlWithNewLineSupport("<ul><li>line1</li><li>line2</li></ul>"));

        assertEquals("line1\nline2\n", StringUtil.escapeHtmlWithNewLineSupport("<ul><li>line1</li><li>line2</li></ul>"));

        assertEquals(
                "\n",
                StringUtil
                        .escapeHtmlWithNewLineSupport("<div class=\"plain_text\"><br _moz_editor_bogus_node=\"TRUE\" /></div>"));

    }

}
