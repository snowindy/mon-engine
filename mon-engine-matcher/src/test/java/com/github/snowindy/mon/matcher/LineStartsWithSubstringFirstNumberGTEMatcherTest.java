package com.github.snowindy.mon.matcher;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class LineStartsWithSubstringFirstNumberGTEMatcherTest {

    @Test
    public void test_matches() {
        LineStartsWithSubstringFirstNumberGTEMatcher m = new LineStartsWithSubstringFirstNumberGTEMatcher();
        m.setItems(new ArrayList<Object>(Arrays.asList("Budget", "100")));

        assertTrue(m.matches("Budget: 100 bucks"));
        assertTrue(m.matches("Budget: $100"));
        assertTrue(m.matches("Budget: 100.0 dinars"));
        assertFalse(m.matches("Budget: 99.0 dinars"));
        assertFalse(m.matches("My 110 dollars budget"));
        assertFalse(m.matches("My 90 dollars budget"));

        m.setItems(new ArrayList<Object>(Arrays.asList("Budget size", "100")));

        assertTrue(m.matches("budget size is 110 dollars"));
        assertFalse(m.matches("My 110 dollars budget amount"));
    }
}
