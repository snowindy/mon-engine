package com.github.snowindy.mon.matcher;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.util.WordMatchUtils;

public class WordMatchUtilsTest extends Assert {
    @Test
    public void test_containsAny() {        
        assertTrue(WordMatchUtils.containsAnyPhrases(Arrays.asList("jAvA"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertTrue(WordMatchUtils.containsAnyPhrases(Arrays.asList("java"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertTrue(WordMatchUtils.containsAnyPhrases(Arrays.asList("gwt"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAnyPhrases(Arrays.asList("php"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAnyPhrases(Arrays.asList("java"), "javascript developer needed.\n Need a php developer, urgent."));
        assertTrue(WordMatchUtils.containsAnyPhrases(Arrays.asList("javaScript"), "javascript developer needed.\n Need a php developer, urgent."));
        assertFalse(WordMatchUtils.containsAnyPhrases(Arrays.asList(new String[]{}), "javascript developer needed.\n Need a php developer, urgent."));
        assertFalse(WordMatchUtils.containsAnyPhrases(Arrays.asList("gwt"), ""));
        
        assertTrue(WordMatchUtils.containsAnyPhrases(Arrays.asList("java developer"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAnyPhrases(Arrays.asList("gwt needed"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAnyPhrases(Arrays.asList("gwt needed"), ""));
    }

    @Test
    public void test_containsWithWildcard() {
        assertTrue(WordMatchUtils.containsAnyPhrases(Arrays.asList("urg*"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertTrue(WordMatchUtils.containsAnyPhrases(Arrays.asList("developer need*"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertTrue(WordMatchUtils.containsAnyPhrases(Arrays.asList("нелат*"), "Тест нелатиницы."));
        assertFalse(WordMatchUtils.containsAnyPhrases(Arrays.asList("Тест* фигни*"), "Тест нелатиницы.")); // It's kind of bug but really not critical

        assertFalse(WordMatchUtils.containsAnyPhrases(Arrays.asList("urgar*"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAnyPhrases(Arrays.asList("developer needNot*"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAnyPhrases(Arrays.asList("нелатать*"), "Тест нелатиницы."));

    }

    @Test
    public void test_containsAll() {
        assertTrue(WordMatchUtils.containsAllPhrases(Arrays.asList("jAvA"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertTrue(WordMatchUtils.containsAllPhrases(Arrays.asList("jAvA","GWT"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAllPhrases(Arrays.asList("jAvA","php"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAllPhrases(Arrays.asList(new String[]{}), "javascript developer needed.\n Need a php developer, urgent."));
        
        assertTrue(WordMatchUtils.containsAllPhrases(Arrays.asList("gwt developer", "java"), "GWT developer needed.\n Need a Java developer, urgent."));
        assertFalse(WordMatchUtils.containsAllPhrases(Arrays.asList("gwt needed", "java"), "GWT developer needed.\n Need a Java developer, urgent."));
    }

}
