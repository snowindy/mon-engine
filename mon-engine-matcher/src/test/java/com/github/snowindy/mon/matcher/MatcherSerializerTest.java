package com.github.snowindy.mon.matcher;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MatcherSerializerTest {

    @Test
    public void test_serialization() {
        AbstractMatcher matcher = new AndMatcher(new OrMatcher(new ContainsAllPhraseMatcher("java", "mysql"),
                new ContainsAllPhraseMatcher("php", "postgres")), new OrMatcher(
                new LineStartsWithSubstringFirstNumberGTEMatcher("Budget:", "100"), new NotMatcher(
                        new LineStartsWithSubstringMatcher("Budget:"))));

        MatcherSerializer s = new MatcherSerializer();
        String json = s.serializeJson(matcher);

        assertEquals(
                "{\"items\":[{\"items\":[{\"items\":[\"java\",\"mysql\"],\"type\":\"containsAllPhrase\"},"
                        + "{\"items\":[\"php\",\"postgres\"],\"type\":\"containsAllPhrase\"}],\"type\":\"or\"},"
                        + "{\"items\":[{\"items\":[\"Budget:\",\"100\"],\"compareTo\":null,\"type\":\"lineStartsWithSubstringFirstNumberGTE\"}"
                        + ",{\"items\":[{\"items\":[\"Budget:\"],\"type\":\"lineStartsWithSubstring\"}],\"type\":\"not\"}],\"type\":\"or\"}],"
                        + "\"type\":\"and\"}", json);

        IMatcher md = s.deserializeJson(json);

        assertEquals(matcher, md);
    }
}
